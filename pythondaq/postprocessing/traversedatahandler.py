#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (C) 2022 Patrick Bachmann, Daniel Jaeger, Lukas Selmayr

This file is part of PythonDAQ.

PythonDAQ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PythonDAQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PythonDAQ.  If not, see <http://www.gnu.org/licenses/>.

Classes and methods to handle measurement data
aquired by traverse measuring procedures

@author: Patrick Bachmann
@author: Daniel Jaeger
@author: Lukas Selmayr
@date: 2021-2022
"""

import logging
import os
from datastorage.csvhandler import CSVDictReader
from datastorage.hdfhandler import HDFDictReader
from datastorage.csvhandler import CSVReader
from datastorage.hdfhandler import HDFReader
from postprocessing import DataReduction

try:
    from datastorage.cgnshandler import CGNSHandler
    cgnshandler_available = True
except ImportError:
    cgnshandler_available = False
import numpy as np
import matplotlib.pyplot as plt
logging.getLogger().setLevel(logging.WARNING)
import operator
import statistics
import math


class TraverseDataHandler():
    """

    This class handles measurement data aquired by traverse measuring procedures

    The measurment data are stored in files, one file for each measurement position.

    Therefore multiple files inside a folder have to be merged
    before going to postprocessing procedures

    The name of the files must have the following structure:
        "<filename>r or x<r in mm or x coordinate in mm>phi or y<angle phi as float or y coordinate in mm>.<extension csv or hdf5>"
    """
    def __init__(self, path):
        """
        Initialization method

        Parameters
        ----------
        path : string
            path to the folder where the measurement data files are stored.
        dimensions : list with length two
            length of dimensions of the two dimensional measurement grid.

        @author: Vojtech Vavra
        @author: Patrick Bachmann
        """
        # Path to the folder with measurement files
        self.path = path

        # Directory for postprocessed Data
        self.path_postprocessing = path + str("/postprocessing")

        # Create folder for postprocessing
        try:
            os.makedirs(self.path_postprocessing)
        except:
            logging.debug('folder already exists')

        # List of readers
        self.readers = []

        # List of filenames
        self.filelist = []

        # List of dataset names
        self.datasetnames = []

        # Different file extensions to differentiate
        self.extension_1 = ".csv"
        self.extension_2 = ".hdf5"

        # Predefine dictionary with all data in it
        self.data = {}

        # Predefine dictionary with all return data in it
        self.meas_dict = {}


    def createDict(self):
        """
        Creates a dictionary with all positions in cartesian coordinates and polar coordinates
        of the measurements and the mean values of the measured values
        """
        # Filter all files in folder that are .csv or .hdf5 measurement files
        filelist = []
        for i, filename in enumerate(os.listdir(self.path)):
            if filename[-4:]==self.extension_1 or filename[-5:]==self.extension_2:
                filelist.append(os.listdir(self.path)[i])

        r=[]
        phi=[]
        x_pos=[]
        y_pos=[]
        z_pos=[]

        # Get sensor positions from file names
        for i ,filename in enumerate(filelist):
            index_p = [pos for pos, char in enumerate(filename) if char == "."][-1]# index of extension point

            # if the file names include polar coordinates:
            if "r" and "phi" in filename:
                index_r = filename.index("r")
                index_phi = filename.index("phi")
                r.append(float(filename[index_r+1:index_phi]))# r value must be the same for one array row
                phi.append(float(filename[index_phi+3:index_p]))
                # cartesian coordinates are calculated as well
                x_pos.append(float(filename[index_r+1:index_phi])*math.cos(float(filename[index_phi+3:index_p])))
                y_pos.append(float(filename[index_r+1:index_phi])*math.sin(float(filename[index_phi+3:index_p])))

            # if the file names include cartesian coordinates:
            if "x" and "y" in filename:
                index_x = filename.index("x")# r and phi coordinates
                index_y = filename.index("y")
                # check if the coordinates are 3D or 2D:
                try:
                    index_z = filename.index("z")
                    z_available = True
                except:
                    z_available = False
                # polar coordinates are calculated as well
                r.append(math.sqrt(float(filename[index_x+1:index_y])**2+float(filename[index_y+1:index_p])**2))# r value must be the same for one array row
                phi.append(math.atan2(float(filename[index_y+1:index_p]),float(filename[index_x+1:index_y])))

                x_pos.append(float(filename[index_x+1:index_y]))
                if z_available:
                    y_pos.append(float(filename[index_y+1:index_z]))
                    z_pos.append(float(filename[index_z+1:index_p]))
                else:
                    y_pos.append(float(filename[index_y+1:index_p]))

        # Write coordinates in meas dictionary
        self.meas_dict['R'] = {}
        self.meas_dict['Phi'] = {}
        self.meas_dict['X'] = {}
        self.meas_dict['Y'] = {}
        self.meas_dict['Z'] = {}

        self.meas_dict['R']['unit'] = 'mm'
        self.meas_dict['Phi']['unit'] = 'rad'
        self.meas_dict['X']['unit'] = 'mm'
        self.meas_dict['Y']['unit'] = 'mm'
        self.meas_dict['Z']['unit'] = 'mm'

        self.meas_dict['R']['data'] = r
        self.meas_dict['Phi']['data'] = phi
        self.meas_dict['X']['data'] = x_pos
        self.meas_dict['Y']['data'] = y_pos
        self.meas_dict['Z']['data'] = z_pos

        # Select readers regarding file extension, readers read file data
        # only csv-files
        if all([x[-4:]==self.extension_1 for x in filelist])==True and all([x[-5:]==self.extension_2 for x in filelist])==False:
            for i, filename in enumerate(filelist):
                self.readers.append(CSVDictReader(self.path+str("/") + filename))

        # only hdf5-files
        elif all([x[-4:]==self.extension_1 for x in filelist])==False and all([x[-5:]==self.extension_2 for x in filelist])==True:
            for i, filename in enumerate(filelist):
                self.readers.append(HDFDictReader(self.path+str("/") + filename))
        # both file types
        else:
            print("Error: CSV-Files and HDF5-Files are in directory, Please select only one type of data!")

        # Write data in data dictionary
        for i, read_key in enumerate(self.readers):
            self.data["TraverseData_"+str(i)] = read_key.getDictionary()

        # Get the derived data of all measurements
        data_red = DataReduction()
        data_red.setDictionary(self.data)

        for i, keyname in enumerate(self.data):
            data_red.getAverageDict(keyname)
            data_red.getMedianDict(keyname)
            data_red.getStandDev(keyname)

        # Add the derived data to the return dictionary
        self.meas_dict['Metadata'] = self.data['TraverseData_0']['Metadata']
        for i, keyname in enumerate(self.data):
            for i, channelname in enumerate(self.data[keyname][self.readers[0].sensorline[1]]):
                self.meas_dict[channelname] = {'unit': self.data[keyname][self.readers[0].sensorline[1]][channelname]['unit'], 'data': []}

        for i, keyname in enumerate(self.data):
            for i, channelname in enumerate(self.data[keyname][self.readers[0].sensorline[1]]):
                self.meas_dict[channelname]['data'].append(self.data[keyname][self.readers[0].sensorline[1]][channelname]['mean'])

        # Sort list and position regarding sensor positions (from big to small radius and clockwise on constant radius)
        for keyname in self.meas_dict:
            if keyname != 'Metadata' and keyname != 'R':
                if len(self.meas_dict[keyname]['data']) != 0:
                    # sort every list by the radius:
                    self.meas_dict['R']['data'], self.meas_dict[keyname]['data'] = zip(*sorted(zip(r, self.meas_dict[keyname]['data']), reverse = True))
                    # convert tuples back to lists:
                    self.meas_dict['R']['data'] = list(self.meas_dict['R']['data'])
                    self.meas_dict[keyname]['data'] = list(self.meas_dict[keyname]['data'])

    def getDictionary(self):
        """
        Return the dictionary with the mean values of the traverse data
        """
        self.createDict()
        return self.meas_dict


    def getAllData(self):
        """
        Returns dict with all measured data from the traverse data folder
        -------
        """
        return self.data


    def writeCGNS(self, cgns_filename, data_dict, dimensions):
        """
        Write cgns file with data from readers

        Parameters
        ----------
        cgns_filename : string
            name of file with extention, cgns file is stored in postprocessing folder
        """

        # CGNS writer
        self.writer = CGNSHandler()

        if (cgnshandler_available == False):
            raise ImportError("Package cgnsHandler is not available")

        self.writer.writeCGNS(cgns_filename, self.path_postprocessing, dimensions, data_dict)


    '''
    OLD PART OF THE SCRIPT
    '''

#------------------------------------------------------------------------------
    def createList(self):
        """
        Create a sorted list of measurement file names

        @author: Vojtech Vavra
        @author: Patrick Bachmann
        """
        # Filter all files in folder that are .csv or .hdf5 measurement files
        filelist = []
        for i, filename in enumerate(os.listdir(self.path)):
            if filename[-4:]==self.extension_1 or filename[-5:]==self.extension_2:
                filelist.append(os.listdir(self.path)[i])

        r=[]
        phi=[]
        x_pos=[]
        y_pos=[]
        z_pos=[]
        # Get sensor positions from file names
        for i ,filename in enumerate(filelist):
            index_p = [pos for pos, char in enumerate(filename) if char == "."][-1]# index of extension point
            if "r" and "phi" in filename:
                index_r = filename.index("r")
                index_phi = filename.index("phi")
                r.append(float(filename[index_r+1:index_phi]))# r value must be the same for one array row
                phi.append(float(filename[index_phi+3:index_p]))

            if "x" and "y" in filename:
                index_x = filename.index("x")# r and phi coordinates
                index_y = filename.index("y")
                try:
                    index_z = filename.index("z")
                    z_available = True
                except:
                    z_available = False
                r.append(math.sqrt(float(filename[index_x+1:index_y])**2+float(filename[index_y+1:index_p])**2))# r value must be the same for one array row
                phi.append(math.atan2(float(filename[index_y+1:index_p]),float(filename[index_x+1:index_y])))
                x_pos.append(float(filename[index_x+1:index_y]))
                if z_available:
                    y_pos.append(float(filename[index_y+1:index_z]))
                    z_pos.append(float(filename[index_z+1:index_p]))
                else:
                    y_pos.append(float(filename[index_y+1:index_p]))

        positions = []
        # Sort list and position regarding sensor positions (from big to small radius and clockwise on constant radius)
        coordinate_list = np.array([[r[0], phi[0], filelist[0]]])
        for i in range(1,len(r)):
            coordinate_list = np.append(coordinate_list, [[r[i], phi[i], filelist[i]]], axis=0)
        sorted_list = sorted(coordinate_list, key=operator.itemgetter(0,1),reverse=True)
        for item in sorted_list:
            self.filelist.append(item[2])
            positions.append((float(item[0]), float(item[1])))

        # Plot sensor positions
        plt.polar(phi, r, '*')

        # Build coordinate arrays
        r = []
        phi = []
        for pos in positions:
            r.append(pos[0])
            phi.append(pos[1])
        r = np.reshape(r,(self.dimensions[0], self.dimensions[1]))
        phi = np.reshape(phi,(self.dimensions[0], self.dimensions[1]))

        # Write coordinates in data dictionary
        self.data['R'] = {}
        self.data['Phi'] = {}
        self.data['X'] = {}
        self.data['Y'] = {}
        self.data['Z'] = {}
        self.data['R']['unit'] = 'mm'
        self.data['Phi']['unit'] = 'rad'
        self.data['X']['unit'] = 'mm'
        self.data['Y']['unit'] = 'mm'
        self.data['Z']['unit'] = 'mm'
        self.data['R']['data'] = r
        self.data['Phi']['data'] = phi
        self.data['X']['data'] = x_pos
        self.data['Y']['data'] = y_pos
        self.data['Z']['data'] = z_pos


    def getData(self):
        """
        reads data from files
        """
        # Select readers regarding file extension, readers read file data
        # only csv-files
        if all([x[-4:]==self.extension_1 for x in self.filelist])==True and all([x[-5:]==self.extension_2 for x in self.filelist])==False:
            for i, filename in enumerate(self.filelist):
                self.readers.append(CSVReader(self.path+str("/") + filename))
        # only hdf5-files
        elif all([x[-4:]==self.extension_1 for x in self.filelist])==False and all([x[-5:]==self.extension_2 for x in self.filelist])==True:
            for i, filename in enumerate(self.filelist):
                self.readers.append(HDFReader(self.path+str("/") + filename))
        # both file types
        else:
            print("Error: CSV-Files and HDF5-Files are in directory, Please select only one type of data!")

        # Write data in data dictionary
        for i in range(len(self.readers[0].datasets)):# for every channel
            # Generate dataset name
            datasetname = 'DS_'+ self.readers[0].sensorline[i] + '_' + self.readers[0].channelline[i]
            datasetname = datasetname.replace(" ", "_")# Remove emptyspaces
            # Write data dictionary
            self.data[datasetname] = {}
            self.data[datasetname]['data'] = {}
            for j in range(len(self.readers[0].datasets[0])):# for every timestep
                dset = []
                for n in range(len(self.readers)):#for every file
                    dset.append(self.readers[n].datasets[i][j])
                dset = np.reshape(dset,(self.dimensions[0], self.dimensions[1]))# self.dim_array.shape)
                self.data[datasetname]['data']['TS_' + str(j+1)] = dset
            # Write unit
            self.data[datasetname]['unit'] = self.readers[0].unitline[i]
            # Write names in a list
            self.datasetnames.append(datasetname)


    def derivedData(self):
        """
        Calculate statistics
        """
        for i, keyname in enumerate(self.datasetnames):# for every dataset
            # Get timekey list
            timekey_list = []
            for n, timekey in enumerate(list(self.data[keyname]['data'].keys())):
                if timekey[:3]=='TS_':
                    timekey_list.append(list(self.data[keyname]['data'].keys())[n])
            # Predefine
            mean_array_list = []
            median_array_list = []
            st_dev_array_list = []
            for n in range(self.data[keyname]['data']['TS_1'].size):# for every array point
                value_list = []
                for timekey in timekey_list:# for every timestep
                    array_list = self.data[keyname]['data'][timekey].flatten()
                    value_list.append(array_list[n])
                # Calculate statistics
                st_dev_array_list.append(statistics.stdev(value_list))
                mean_array_list.append(statistics.mean(value_list))
                median_array_list.append(statistics.median(value_list))
            # Write in data dictionary
            self.data[keyname]['data']['Mean'] = np.reshape(mean_array_list,(self.dimensions[0], self.dimensions[1]))
            self.data[keyname]['data']['Median'] = np.reshape(median_array_list,(self.dimensions[0], self.dimensions[1]))
            self.data[keyname]['data']['Standard_Deviation'] = np.reshape(st_dev_array_list,(self.dimensions[0], self.dimensions[1]))


    def contourPlots(self, setname, periodicity):
        """
        Plot two dimensional data

        Parameters
        ----------
        setname : string
            name of dataset to plot, either statistic sets ('Mean', 'Median' or 'Standard_Deviation') or timestep dataset ('TS_<number>').
        periodicity : integer
            if measurement grid is periodic channel section, periodicity is the number of grids that make up a whole channel,
            otherwise periodicity is 0 or 1, to plot only the original grid
        """
        # Build file structure
        path = self.path_postprocessing
        if not os.path.exists(path + '/plots'):
            os.makedirs(path + '/plots')
        if not os.path.exists(path + '/plots/contourplots'):
            os.makedirs(path + '/plots/contourplots')

        # Get values
        for i, keyname in enumerate(self.datasetnames):# for every data set
            fig, ax = plt.subplots(figsize=(8,8),subplot_kw=dict(projection='polar'))
            r = self.data['R']['data']
            phi = self.data['Phi']['data']
            value = self.data[keyname]['data'][setname]

            # full circle, circumfrerential extrapolate
            if periodicity > 1:
                # Phi steps array
                phi_step = np.zeros((phi.shape[0], phi.shape[1]-1))
                for n in range(phi.shape[0]):
                    for j in range(phi.shape[1]-1):
                        phi_step[n][j] = phi[n][j+1] - phi[n][j]
                # Multiply array
                phi_steps = phi_step
                for n in range(periodicity-1):
                    r = np.concatenate((r,self.data['R']['data'][:,1:]), axis=1)
                    value = np.concatenate((value, self.data[keyname]['data'][setname][:,1:]), axis=1)
                    phi_steps = np.concatenate((phi_steps, phi_step), axis=1)
                # Edite phi array
                phi = np.zeros((r.shape[0], r.shape[1]))
                for n in range(r.shape[0]):
                        for j in range(r.shape[1]-1):
                            phi[n][j+1] = phi[n][j] + phi_steps[n][j]# the plot function sets the interval automatically

            # Set r grid
            ax.set_rorigin(0)
            ax.set_rmin(np.amin(r))
            ax.set_rmax(np.amax(r))
            ax.set_rgrids([round(x,1) for x in np.linspace(np.amin(r),np.amax(r),5)], angle=22)
            label_position=ax.get_rlabel_position()
            if periodicity <= 1:
                ax.text(np.amin(phi)-0.1,ax.get_rmin()+10,'r in mm',rotation=label_position,ha='center',va='center')
            else:
                ax.text(np.radians(label_position+10),ax.get_rmin()+10,'r in mm',rotation=label_position,ha='center',va='center')
            # Plot and colormap
            cm = ax.contourf(phi, r, value, levels=10, cmap='plasma')
            cb =fig.colorbar(cm, ax=ax)#, orientation='horizontal')
            cb.set_label(self.data[keyname]['unit'])
            # For original grid, limit plot
            if periodicity <= 1:
                ax.set_thetamin(math.degrees(np.amin(phi)))
                ax.set_thetamax(math.degrees(np.amax(phi)))

            # Save plot
            if periodicity > 1:
                if not os.path.exists(path + '/plots/contourplots/full_channel'):
                    os.makedirs(path + '/plots/contourplots/full_channel')
                plt.savefig(path + '/plots/contourplots/full_channel/' + keyname + '_' + setname)
            else:
                plt.savefig(path + '/plots/contourplots/' + keyname + '_' + setname)


    def axisPlots(self, setname, axis):
        """
        Plot averaged data along one axis

        Parameters
        ----------
        setname : string
            name of dataset to plot, either statistic sets ('Mean', 'Median' or 'Standard_Deviation') or timestep dataset ('TS_<number>').
        axis : integer
            0 for radial plots, circumferentially averaged.
            1 for circumferential plots, radially averaged.
        """
        # Build file structure
        path = self.path_postprocessing
        # Folder names
        if axis == 0:# radial plot
            name = 'radialplots'
        elif axis == 1:#circumferential plot
            name = 'circumferentialplots'
        else:
            print('Error: No axis is set!')
        # Create folders
        if not os.path.exists(path + '/plots'):
            os.makedirs(path + '/plots')
        if not os.path.exists(path + '/plots/' + name):
            os.makedirs(path + '/plots/' + name)

        for i, keyname in enumerate(self.datasetnames):# for every data set
            # if not setname=='all':
            fig, ax = plt.subplots(figsize=(6,6))#1, figsize=(8, 6))
            statistic_values =[]
            statistic_coordinates = []

            if axis == 0:# radial plots
                # Get values
                for n in range(self.data[keyname]['data'][setname].shape[0]):
                    statistic_values.append(statistics.mean(self.data[keyname]['data'][setname][n]))
                    statistic_coordinates.append(self.data['R']['data'][n][0])
                # Plot
                plt.title('Circumferentially Averaged')
                plt.ylim([min(statistic_coordinates), max(statistic_coordinates)])
                ax.set_xlabel(self.data[keyname]['unit'])
                ax.set_ylabel('r in ' + self.data['R']['unit'])
                plt.plot(statistic_values, statistic_coordinates, 'r')

            elif axis == 1:# circumferential plots
                # Get values
                for n in range(self.data[keyname]['data'][setname].shape[1]):
                    value_list = []
                    coordinate_list = []
                    for j in range(self.data[keyname]['data'][setname].shape[0]):
                        value_list.append(self.data[keyname]['data'][setname][j][n])
                        coordinate_list.append(self.data['Phi']['data'][j][n])
                    statistic_values.append(statistics.mean(value_list))
                    statistic_coordinates.append(statistics.mean(coordinate_list))
                # Plot
                plt.title('Radially Averaged')
                plt.xlim([min(statistic_coordinates), max(statistic_coordinates)])
                ax.set_xlabel('phi in ' + self.data['Phi']['unit'])
                ax.set_ylabel(self.data[keyname]['unit'])
                plt.plot(statistic_coordinates, statistic_values, 'r')
            # Save plot
            plt.savefig(path + '/plots/' + name +'/' + keyname + '_' + setname)


    def run(self, setname, periodicity, dimensions):
        """
        Run complete postprocessing
        """
        # List of dimensions of measurement grid
        self.dimensions = dimensions

        self.createList()
        self.getData()
        self.derivedData()
        self.contourPlots(setname, periodicity)
        self.axisPlots(setname, 0)# radial plots
        self.axisPlots(setname, 1)# circumferential plot

#-----------------------------------------------------------------------------------------------------------------------------------------
