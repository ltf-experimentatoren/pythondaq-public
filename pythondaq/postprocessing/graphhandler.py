#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (C) 2022 Daniel Jaeger, Lukas Selmayr

This file is part of PythonDAQ.

PythonDAQ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PythonDAQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PythonDAQ.  If not, see <http://www.gnu.org/licenses/>.

@author: Lukas Selmayr
@author: Daniel Jaeger
@date: 2022
"""

import matplotlib.pyplot as plt
import locale
import matplotlib.dates as mdates
from matplotlib.ticker import ScalarFormatter

locale.setlocale(locale.LC_NUMERIC, "de_DE.utf8")

class BaseGraph():
    """
    Abstract Class to set functions for following grah drawing classes.
    """

    def __init__(self, values, measDictNames, valDictNames, valsToPlot):
        """
        Init method to check if input values are valid
        """
        self.val_dict = values
        self.measDictNames = measDictNames
        self.valDictNames = valDictNames
        self.valsToPlot = valsToPlot

        self.xLabel = ""
        self.yLabel = []
        self.legend = []

        self._create_figure = True

        # check if all input values are valid
        if not len(self.measDictNames) == len(self.valDictNames) == len(self.valsToPlot):
            raise Exception('The input lists have to have the same length.')
        if not all(elem in self.val_dict for elem in self.measDictNames):
            raise Exception('The name of the measurement dict does not exist!')
        if not all(elem in self.val_dict[measDictNames[i]] for i, elem in enumerate(valDictNames)):
            raise Exception('The name of the value dict does not exist!')
        if not all(elem in self.val_dict[measDictNames[i]][valDictNames[i]] for i, elem in enumerate(valsToPlot)):
            raise Exception('The name of the value to plot does not exist!')


    def setAxis(self, axis):
        """
        Assign the axis of an external figure to plot into.
        This automatically disables creating a new figure.
        """
        self.ax = axis
        self._create_figure = False


    def setXLabel(self, x_label):
        """
        Set the label for the x axis. The unit is automatically appended.
        """
        self.xLabel = x_label


    def setYLabel(self, yLabel):
        """
        Set labels for the y axes. The unit is automatically appended.
        yLabel is a list!
        """
        if type(yLabel) != list:
            raise TypeError("yLabel has to be of type list")
        self.yLabel = yLabel


    def draw(self):
        """
        Graph drawing method
        """
        # Initialize the subplot
        if self._create_figure:
            self.fig, self.ax = plt.subplots(layout='constrained')
            self.fig.set_size_inches(7,4)


    def save(self, filePath):
        """
        Save the graph in a file.
        File format can be added at the end of the file path.
        """
        self.fig.savefig(filePath)



class TimeGraph(BaseGraph):
    """
    Class to easily draw graphs for typical usecases.
    """

    def __init__(self, values, measDictNames, valDictNames, valsToPlot):
        """
        Init method
        The input measDictNames, valDictNames and valsToPlot are lists!
        """
        super().__init__(values, measDictNames, valDictNames, valsToPlot)

        self.xLabel = "Time"
        self.timeLimits = []


    def setLegend(self, legend):
        """
        Set the legend.
        The number of entries must match the number of lines to plot.
        """
        # Check for correct entries
        if type(legend) != list:
            raise TypeError("legend has to be of type list")
        if len(legend) != len(self.measDictNames):
            raise Exception("lengend must match the number of lines to plot")
        # Assign the variable
        self.legend = legend


    def setTimeLimits(self, limits):
        """
        Set limits for the time axis.
        The time limits must be datetime objects.
        """
        self.timeLimits = limits


    def draw(self):
        """
        Draws graph where x axis is a timeline.
        The input yLabel, legend, timeLimits are lists!
        """
        # initialize the subplot and the xaxis
        super().draw()
        self.ax.xaxis_date()
        hm = mdates.DateFormatter('%H:%M:%S')
        self.ax.xaxis.set_major_formatter(hm)
        self.ax_formatter = ScalarFormatter(useOffset=False)
        self.ax.yaxis.set_major_formatter(self.ax_formatter)

        # get the first plot with the input values
        pt_1, = self.ax.plot(self.val_dict[self.measDictNames[0]]['Time of Measurement']['-']['data'], self.val_dict[self.measDictNames[0]][self.valDictNames[0]][self.valsToPlot[0]]['data'], linewidth=1.5)

        # if the input lists have two elements the second plot is created
        if len(self.measDictNames) == 2:
            self.ax2 = self.ax.twinx()
            self.ax2.xaxis.set_major_formatter(hm)
            self.ax2.yaxis.set_major_formatter(self.ax_formatter)
            pt_2, = self.ax2.plot(self.val_dict[self.measDictNames[1]]['Time of Measurement']['-']['data'], self.val_dict[self.measDictNames[1]][self.valDictNames[1]][self.valsToPlot[1]]['data'], linewidth=1.5, c='orange')

        # set the labels of the axes
        self.ax.set_xlabel(self.xLabel)

        # Fill up unspecified y-labels
        for i in range(len(self.yLabel),len(self.valsToPlot)):
            self.yLabel.append(self.valsToPlot[i])

        # check if the plotted value has a unit or not
        unit = self.val_dict[self.measDictNames[0]][self.valDictNames[0]][self.valsToPlot[0]]['unit']
        if unit == '-':
            self.ax.set_ylabel(self.yLabel[0])

        # add the unit to the label
        else:
            self.ax.set_ylabel(self.yLabel[0] + ' in ' + unit)

        # same procedure for the second plot
        if len(self.measDictNames) == 2:
            unit2 = self.val_dict[self.measDictNames[1]][self.valDictNames[1]][self.valsToPlot[1]]['unit']
            if unit2 == '-':
                self.ax2.set_ylabel(self.yLabel[1])
            else:
                self.ax2.set_ylabel(self.yLabel[1] + ' in ' + unit2)

        # Create a legend if desired
        if self.legend != []:
            if len(self.measDictNames) == 1:
                self.ax.legend([pt_1],[self.legend[0]])
            elif len(self.measDictNames) == 2:
                self.ax.legend([pt_1, pt_2], [self.legend[0], self.legend[1]])

        # if there are extra time limits, set limits for x axis
        if self.timeLimits != []:
            self.ax.set_xlim(self.timeLimits)
            if len(self.measDictNames) == 2:
                self.ax2.set_xlim(self.timeLimits)



class XYGraph(BaseGraph):

    def __init__(self, values, measDictNames, valDictNames, valsToPlot):
        """
        Draws a xy graph.
        The input measDictNames, valDictNames and valsToPlot are lists!
        """
        super().__init__(values, measDictNames, valDictNames, valsToPlot)


    def draw(self):
        """
        Draws a graph to compare two values.
        The input labels are lists!
        """

        # initialize the subplot and the axes
        super().draw()
        self.ax_formatter = ScalarFormatter(useOffset=False)
        self.ax.yaxis.set_major_formatter(self.ax_formatter)
        self.ax.xaxis.set_major_formatter(self.ax_formatter)

        # get the plot with the input values
        self.ax.plot(self.val_dict[self.measDictNames[0]][self.valDictNames[0]][self.valsToPlot[0]]['data'], self.val_dict[self.measDictNames[1]][self.valDictNames[1]][self.valsToPlot[1]]['data'], linewidth=1.5)

        # Auto-assign x-label if needed
        if self.xLabel == "":
            self.xLabel = self.valsToPlot[0]
        # Fill up unspecified y-labels
        for i in range(len(self.yLabel),len(self.valsToPlot)-1):
            self.yLabel.append(self.valsToPlot[i+1])

        # set the labels of the axes
        for i, elem in enumerate(self.measDictNames):

            # check if the plotted values have units
            unit = self.val_dict[self.measDictNames[i]][self.valDictNames[i]][self.valsToPlot[i]]['unit']
            if  unit == '-':
                if i == 0:
                    self.ax.set_xlabel(self.xLabel)
                else:
                    self.ax.set_ylabel(self.yLabel[i-1])

            # add the units to the labels of the axes
            else:
                if i == 0:
                    self.ax.set_xlabel(self.xLabel + ' in ' + unit)
                else:
                    self.ax.set_ylabel(self.yLabel[i-1] + ' in ' + unit)



class Figure():
    """
    Class which works as a sheet for positioning graphs inside.
    Graphs are positioned along a rectangular grid
    """
    def __init__(self, title, num_y=1, num_x=1):
        """
        Create a new figure with the given amount of plots in x and y direction
        """
        self.title = title
        self.num_x = num_x
        self.num_y = num_y

        # This is the main storage variable
        self.graphs = [[None for i in range(num_x)] for j in range(num_y)]


    def setGraph(self, graph, pos_y, pos_x):
        """
        Set a graph at the given position
        """
        # Check for the correct positioning
        if (pos_x < 0) or (pos_x >= self.num_x):
            raise IndexError("X-Position out of range")
        if (pos_y < 0) or (pos_y >= self.num_y):
            raise IndexError("Y-Position out of range")

        # Store the graph instance
        self.graphs[pos_y][pos_x] = graph


    def draw(self):
        """
        Draw all specified graphs inside the figure
        """
        # Create the figure
        self.fig, self.axs = plt.subplots(self.num_y,self.num_x,layout='constrained')
        self.fig.set_size_inches(7,5)
        self.fig.suptitle(self.title)

        # Draw all graphs
        for i in range(self.num_y):
            for j in range(self.num_x):
                if (self.num_x > 1) and (self.num_y > 1):
                    self.graphs[i][j].setAxis(self.axs[i,j])
                else:
                    self.graphs[i][j].setAxis(self.axs[i+j])
                self.graphs[i][j].draw()


    def save(self, filePath):
        """
        Save the figure in a file.
        File format can be added at the end of the file path.
        """
        self.fig.savefig(filePath)
