#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (C) 2022 Lukas Selmayr

This file is part of PythonDAQ.

PythonDAQ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PythonDAQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PythonDAQ.  If not, see <http://www.gnu.org/licenses/>.

@author: Lukas Selmayr
@date: 2022
"""

import statistics

class DataReduction():
    """
    Class to handle measured data and to get dict with stationary values and
    averaged and median dicts.
    """

    def __init__(self):
        """
        Init method
        """
        self.values = {}


    def setDictionary(self, values):
        """
        Get the dictionary with all the measured values
        """
        self.values = values


    def getStatDict(self, name, dictName, startTime, endTime):
        """
        Adds all values between two time stamps to the values dict
        The start and end time have to be in datetime format
        """
        self.values[name] = {}

        # check if the start and end time are in the values dict.
        if startTime < min(self.values[dictName]['Time of Measurement']['-']['data']):
            raise Exception('The value for the start time is not in the file!')
        if endTime > max(self.values[dictName]['Time of Measurement']['-']['data']):
            raise Exception('The value for the end time is not in the file!')

        # mqke a new dict with only the values between start and end time
        for meas_val in self.values[dictName]:
            self.values[name][meas_val] = {}
            for chan in self.values[dictName][meas_val]:

                # the metadata has to be considered seperately, because it has a different style than the rest
                if meas_val == 'Metadata':
                    self.values[name][meas_val][chan] = self.values[dictName]['Metadata'][chan]
                    continue

                # add the rest of the values to the dict
                else:
                    self.values[name][meas_val][chan] = {'unit': self.values[dictName][meas_val][chan]['unit'], 'data':[]}
                    for i, time in enumerate(self.values[dictName]['Time of Measurement']['-']['data']):
                        if startTime <= time <= endTime:
                            self.values[name][meas_val][chan]['data'].append(self.values[dictName][meas_val][chan]['data'][i])


    def getAverageDict(self, dictName):
        """
        Adds an average for all values of the given dict to the values dict
        """

        # check if the dict name is in the values dict
        if not dictName in self.values:
            raise Exception('The name of the stationary dict does not exist!')

        # add a new dict to the last level of the given dictionary
        for meas_val in self.values[dictName]:
            for chan in self.values[dictName][meas_val]:

                # the metadata has to be considered seperately, because it has a different style than the rest
                if meas_val == 'Metadata' or meas_val == 'Time of Measurement':
                    continue

                # add the rest of the values to the dict
                else:
                    self.values[dictName][meas_val][chan]['mean'] = statistics.mean(self.values[dictName][meas_val][chan]['data'])


    def getMedianDict(self, dictName):
        """
        Adds an median average for all values of the given dict to the values dict
        Has the same build as the getAverageDict method
        """

        # check if the dict name is in the values dict
        if not dictName in self.values:
            raise Exception('The name of the stationary dict does not exist!')

        # add a new dict to the last level of the given dictionary
        for meas_val in self.values[dictName]:
            for chan in self.values[dictName][meas_val]:

                # the metadata has to be considered seperately, because it has a different style than the rest
                if meas_val == 'Metadata' or meas_val == 'Time of Measurement':
                    continue

                # add the rest of the values to the dict
                else:
                    self.values[dictName][meas_val][chan]['median'] = statistics.median(self.values[dictName][meas_val][chan]['data'])


    def getStandDev(self, dictName):
        """
        Adds the standard deviation for all values of the given dict to the values dict
        """

        # check if the dict name is in the values dict
        if not dictName in self.values:
            raise Exception('The name of the stationary dict does not exist!')

        # add a new dict to the last level of the given dictionary
        for meas_val in self.values[dictName]:
            for chan in self.values[dictName][meas_val]:

                # the metadata has to be considered seperately, because it has a different style than the rest
                if meas_val == 'Metadata' or meas_val == 'Time of Measurement':
                    continue

                # add the rest of the values to the dict
                else:
                    self.values[dictName][meas_val][chan]['standard deviation'] = statistics.pstdev(self.values[dictName][meas_val][chan]['data'])
