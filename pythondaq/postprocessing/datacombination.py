#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (C) 2022 Daniel Jaeger

This file is part of PythonDAQ.

PythonDAQ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PythonDAQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PythonDAQ.  If not, see <http://www.gnu.org/licenses/>.

Combine several measurement files to one dictionay

@author: Daniel Jaeger
@date: 2022
"""

class DataCombination():
    """
    This class provides methods to combine several measurement files
    """
    def __init__(self):
        """
        Initialization method
        """
        self.values = {}


    def setDictionary(self, values):
        """
        Set the dictionary with all the measured values
        """
        self.values = values


    def createCombinedDict(self, result_name, template_name):
        """
        Initialize the combined dictionary
        based on the entries of the template sub-dictionary.
        """
        # Check for a valid template
        if not template_name in self.values:
            raise KeyError("Template name not found in dictionary!")

        # Initialize the result data structure
        self.result_name = result_name
        self.values[result_name] = {}

        # Create the data structure for combined values
        for meas_val in self.values[template_name]:
            self.values[result_name][meas_val] = {}
            for chan in self.values[template_name][meas_val]:

                # The metadata has to be considered seperately,
                # because it has a different style than the rest
                if meas_val == 'Metadata':
                    self.values[result_name][meas_val][chan] = \
                        self.values[template_name]['Metadata'][chan]
                    continue
                else:
                    self.values[result_name][meas_val][chan] = \
                        {'unit': self.values[template_name][meas_val][chan]['unit'], 'data':[]}


    def combineTimeSeries(self, result_name, dict_names=[]):
        """
        Combine the time series of the measurements listed in dict_names.
        If dict_names is empty, all available measurements are combined.
        """
        # TODO: This method still needs to be implemented
        pass


    def combineAverage(self, dict_names=[]):
        """
        Combine the mean vales of the measurements listed in dict_names.
        If dict_names is empty, all available measurements are combined.
        """
        # Select all measurements if nothing is specified
        if dict_names == []:
            dict_names = list(self.values.keys())
            dict_names.remove(self.result_name) # The result dict is not part of input data

        # Iterate over all channels
        for group in self.values[self.result_name]:
            for chan in self.values[self.result_name][group]:

                # The metadata is already combined when creating the data structure
                if group != 'Metadata':
                    for idx_meas in range(len(dict_names)):
                        # Build the time of measurement structure
                        if group == 'Time of Measurement':
                            self.values[self.result_name]['Time of Measurement']['-']['data'].append( \
                                self.values[dict_names[idx_meas]][group][chan]['data'][0])
                        else:
                            # Append the mean values
                            self.values[self.result_name][group][chan]['data'].append( \
                                self.values[dict_names[idx_meas]][group][chan]['mean'])
