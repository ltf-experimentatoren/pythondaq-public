#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (C) 2022 Daniel Jaeger

This file is part of PythonDAQ.

PythonDAQ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PythonDAQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PythonDAQ.  If not, see <http://www.gnu.org/licenses/>.

Simple setup to test the functionality of PythonDAQ.

@author: Daniel Jaeger
@date: 2020
"""

# Python module imports
import datetime

from dataserver.dataserver import DataServer
from sensors import SensorSim

# Initialize the data server
server = DataServer()

# Initialize a simulation sensor
sensor_1 = SensorSim()

# Add the sensor to the data server
server.addSensor(sensor_1)

# Configure a permanently writing logfile
time = datetime.datetime.now()
server.addWriter("logfiles/" + time.strftime("%Y-%m-%d_%H-%M-%S") + "_logfile.csv")

# Configure the trigger server which allows measurement triggering from traverse control
#tr_server = TriggerServer(server.triggerMeasurement)

# Start the data server. Trigger server is started automatically
server.start()

# To record a measurement type while running
#server.triggerMeasurement()

# To stop the server type in (in this order):
#tr_server.stop()
#server.stop()
