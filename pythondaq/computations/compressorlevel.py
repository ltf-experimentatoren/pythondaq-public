#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (C) 2022 Lukas Selmayr

This file is part of PythonDAQ.

PythonDAQ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PythonDAQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PythonDAQ.  If not, see <http://www.gnu.org/licenses/>.

@author: Lukas Selmayr
@date: 2021-2022
"""

import math

from .computation import Computation
from utils import averaging


class CompressorLevel(Computation):
    """
    This class gets the values from the rakes and/or the corrected pressures at the compressor level and
    computes the averages for the compressor level.
    """
    def __init__(self, name, working_fluid):
        """
        This method initializes the class and sets all values to zero and defines the needed variables and return names.
        """
        super().__init__(name, num_returns = 8)

        self.return_names = ["Average total pressure", "Average static pressure", 
                             "Average Machnumber", "Average total temperature", "Average static temperature",
                             "Average velocity", "Average density", "Average Reynoldsnumber"]
        self.units = ["Pa", "Pa", "-", "K", "K", "m/s", "kg/m³", "-"]

        self.values = {}
        self.values["Average total pressure"] = 0
        self.values["Average static pressure"] = 0
        self.values["Average Machnumber"] = 0
        self.values["Average total temperature"] = 0
        self.values["Average static temperature"] = 0
        self.values["Average velocity"] = 0
        self.values["Average density"] = 0
        self.values["Average Reynoldsnumber"] = 0

        self.circ_pos = {}
        self.rakes = []
        self.inRad = 0
        self.outRad = 1
        self.stat_pres_sens = []
        self.tot_pres_sens = []
        self.stat_pres = {}
        self.tot_pres = {}
        self.stat_circ_pos = {}
        self.tot_circ_pos = {}
        self.sens_radi = {}
        self.working_fluid = working_fluid


    def setRakes(self, rakes):
        """
        This method gets the rakes of the compressor level.
        """
        self.rakes = rakes


    def setCompLvlRadii(self, radii):
        """
        This method takes the radii of the compressor level in [m]
        """
        self.inRad = min(radii)
        self.outRad = max(radii)


    def setStatPresSens(self, stat_sens, stat_pres_names):
        """
        This method sets the static pressures of the compressor level if there are more than the ones for the rakes.
        """
        self.stat_pres_sens = stat_sens
        self.stat_pres_names = stat_pres_names


    def setTotPresSens(self, tot_sens, tot_pres_names):
        """
        This method sets the total pressure of the compressor level if there are no rakes.
        """
        self.tot_pres_sens = tot_sens
        self.tot_pres_names = tot_pres_names


    def _get_circ_pos(self):
        """
        This method gets the circumferantial positions of the rakes of the compressor level.
        """
        for rake in self.rakes:
            self.circ_pos[rake] = rake.circ_pos


    def _get_av_tot_pres(self):
        """
        This method gets the average total pressure of the compressor level.
        It's calculated over the rakes or the defined total pressures.
        """
        tot_pres_rake = {}
        tot_pres_val = 0
        i = 0

        # the total pressures are measured by the rakes
        if self.tot_pres_sens == []:
            for rake in self.rakes:
                tot_pres_rake[rake] = rake.values["Average total pressure"]

            self.values["Average total pressure"] = averaging.getCircAverage(tot_pres_rake, self.circ_pos)

        # there are no rakes at the compressor level
        else:
            for sens in self.tot_pres:
                i += 1
                tot_pres_val += averaging.getCircAverage(self.tot_pres[sens], self.tot_circ_pos[sens])

            self.values["Average total pressure"] = tot_pres_val/i


    def _get_av_stat_pres(self):
        """
        This method gets the average static pressure of the compressor level.
        It's calculated over the rakes or the defined static pressures.
        """
        # the static pressures are measured at the rakes
        if self.stat_pres_sens == []:
            stat_pres_rake = {}
            for rake in self.rakes:
                stat_pres_rake[rake] = rake.values["Average static pressure"]

            self.values["Average static pressure"] = averaging.getCircAverage(stat_pres_rake, self.circ_pos)

        # there are no rakes at the compressor level´
        else:
            av_stat_pres_out = 0
            av_stat_pres_in = 0
            i = 0

            stat_pres_in = averaging.getPresOnRad(self.stat_pres, self.sens_radi, self.stat_circ_pos, self.inRad)[0]
            circ_pos_in = averaging.getPresOnRad(self.stat_pres, self.sens_radi, self.stat_circ_pos, self.inRad)[1]

            stat_pres_out = averaging.getPresOnRad(self.stat_pres, self.sens_radi, self.stat_circ_pos, self.outRad)[0]
            circ_pos_out = averaging.getPresOnRad(self.stat_pres, self.sens_radi, self.stat_circ_pos, self.outRad)[1]

            for sens in stat_pres_in:
                for chan in stat_pres_in[sens]:
                    circ_pos_in[sens][chan] = circ_pos_in[sens].pop(chan+"_corr")
                for chan in stat_pres_out[sens]:
                    circ_pos_out[sens][chan] = circ_pos_out[sens].pop(chan+"_corr")
                if circ_pos_in[sens] != {}:
                    av_stat_pres_in += averaging.getCircAverage(stat_pres_in[sens], circ_pos_in[sens])
                if circ_pos_out[sens] != {}:
                    av_stat_pres_out += averaging.getCircAverage(stat_pres_out[sens], circ_pos_out[sens])
                i += 1

            av_stat_pres_in /= i
            av_stat_pres_out /= i

            # there are static pressures at both compressor radii
            if av_stat_pres_in and av_stat_pres_out != 0:
                self.values["Average static pressure"] = (av_stat_pres_in+av_stat_pres_out)/2

            # there are only static pressures at one compressor radius
            else:
                self.values["Average static pressure"] = (av_stat_pres_in+av_stat_pres_out)


    def _get_av_Ma(self):
        """
        This method gets the average Machnumber of the compressor level.
        It's calculated over the Machnumbers of the rakes or the static and total pressure of the compressor level.
        """
        Ma = {}

        # there are rakes at the compressor level
        if self.rakes != []:
            for rake in self.rakes:
                Ma[rake] = rake.values["Machnumber"]
            self.values["Average Machnumber"] = averaging.getCircAverage(Ma, self.circ_pos)

        # there are no rakes at the compressor level
        else:
            val = self.values["Average total pressure"]/self.values["Average static pressure"]
            p = ((self.working_fluid.kappa-1)/self.working_fluid.kappa)
            if math.pow(val, p) > 1:
                self.values["Average Machnumber"] = math.sqrt((2/(self.working_fluid.kappa-1))*(math.pow(val, p)-1))


    def _get_av_tot_temp(self):
        """
        This method gets the average total temperature of the compressor level.
        It's calculated over the rakes. There is no other measuring way implemented.
        """
        tot_temp = {}

        for rake in self.rakes:
            tot_temp[rake] = rake.values["Average total temperature"]
            
        self.values["Average total temperature"] = averaging.getCircAverage(tot_temp, self.circ_pos)


    def _get_av_stat_temp(self):
        """
        This method gets the average static temperature of the compressor level.
        It's calculated over the rakes. There is no other measuring way implemented.
        """
        stat_temp = {}

        for rake in self.rakes:
            stat_temp[rake] = rake.values["Average static temperature"]
            
        self.values["Average static temperature"] = averaging.getCircAverage(stat_temp, self.circ_pos)


    def _get_av_velocity(self):
        """
        This method gets the average flow velocity of the compressor level.
        It's calculated over the rakes. There is no other measuring way implemented.
        """
        velocity = {}

        for rake in self.rakes:
            velocity[rake] = rake.values["Flow velocity"]
            
        self.values["Average velocity"] = averaging.getCircAverage(velocity, self.circ_pos)


    def _get_av_rho(self):
        """
        This method gets the average density of the compressor level.
        It's calculated over the rakes. There is no other measuring way implemented.
        """
        rho = {} 

        for rake in self.rakes:
            rho[rake] = rake.values["Density"]

        self.values["Average density"] = averaging.getCircAverage(rho, self.circ_pos)


    def _get_av_reynoldsnumber(self):
        """
        This method gets the average Reynoldsnumber of the compressor level.
        It's calculated over the rakes. There is no other measuring way implemented.
        """
        rey = {}

        for rake in self.rakes:
            rey[rake] = rake.values["Reynoldsnumber"]

        self.values["Average Reynoldsnumber"] = averaging.getCircAverage(rey, self.circ_pos)


    def perform(self, values, sensors):
        """
        This method gets the values of the corrected total and static pressures and their positions.
        Afterwards it performs the calculations and returns the values of the calculations.
        """
        # get stat pres values
        for pssens in self.stat_pres_sens:
            for chan in pssens.return_names:
                if chan.replace("_corr","") in self.stat_pres_names:
                    self.stat_pres[pssens] = {}

            # get the circ_pos, rad_pos of the static pressure channels
            for k in pssens.sens:
                for vk in sensors:
                    if k in sensors[vk].name:
                        self.stat_circ_pos[pssens] = sensors[vk].circ_pos
                        self.sens_radi.update(sensors[vk].rad_pos)

            for chan in pssens.return_names:
                if chan.replace("_corr","") in self.stat_pres_names:
                    self.stat_pres[pssens][chan] = pssens.values[chan.replace("_corr","")]

        # get tot pres values
        for ptsens in self.tot_pres_sens:
            for chan in ptsens.return_names:
                if chan.replace("_corr","") in self.tot_pres_names:
                    self.tot_pres[ptsens] = {}

            # get the circ_pos, rad_pos of the total pressure channels
            for k in ptsens.sens:
                for vk in sensors:
                    if k in sensors[vk].name:
                        self.tot_circ_pos[ptsens] = sensors[vk].circ_pos

            for chan in ptsens.return_names:
                if chan.replace("_corr","") in self.tot_pres_names:
                    self.tot_pres[ptsens][chan.replace("_corr","")] = ptsens.values[chan.replace("_corr","")]

        # perform the calculations if possible
        self._get_circ_pos()
        self._get_av_tot_pres()
        self._get_av_stat_pres()
        self._get_av_Ma()

        # these calculations are only possible if there are rakes at the compressor level
        if self.tot_pres_sens == []:
            self._get_av_tot_temp()
            self._get_av_stat_temp()
            self._get_av_velocity()
            self._get_av_rho()
            self._get_av_reynoldsnumber()

        return self.values
