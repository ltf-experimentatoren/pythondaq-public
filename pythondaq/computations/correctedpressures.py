#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (C) 2022 Lukas Selmayr

This file is part of PythonDAQ.

PythonDAQ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PythonDAQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PythonDAQ.  If not, see <http://www.gnu.org/licenses/>.

@author: Lukas Selmayr
@date: 2021-2022
"""

from .computation import Computation

class CorrectedPressures(Computation):
    """
    This class gets one or more measured pressures and returns the corrected
    pressure value. If a reference pressure is set, it performs the
    referencing. If a height of the sensor and a measurement height is set, it
    performs a height correction.
    """
    def __init__(self, name, working_fluid, environment, pressures, amb_temp_name):
        """
        Initialization method:
            For the initialization needed: The given name of the computation,
            the ideal gas working_fluid (this is defined before the initialization
            of this computation), a list of the pressures that should be corrected,
            and the name of the ambient temperature sensor.
        """
        super().__init__(name)

        self.num_returns = 0
        self.return_names = []
        self.meas_heights = {}
        self.amb_temp_name = amb_temp_name
        self.amb_temp = 0
        self.working_fluid = working_fluid
        self.g = environment.g
        self.sens = {}
        self.sensor_heights = {}
        self.values = {}
        self.ref_pres = 0
        self.ref_sens = 0
        self.ref_pres_name = ""

        # Initialize the sensor dict
        for pres in pressures:
            self.sens[pres.split(':')[0]] = {}
        for pres in pressures:
            self.sens[pres.split(':')[0]][pres] = 0.0

        # Fill the return_names list and the number of returns
            self.return_names.append(str(pres) + "_corr")
            self.num_returns += 1

        # Define the correct unit list
        self.units = ["Pa" for i in range(self.num_returns)]

        # Add some default range information
        self.computation_range = [[0.0, 1.0] for i in range(self.num_returns)]
        self.value_range = [[0.0, 1.0] for i in range(self.num_returns)]


    def setReference(self, ref_pres_name, cor_pres):
        """
        This method gets the name of the reference pressure and the corrected
        reference pressure value
        """
        self.ref_pres_name = ref_pres_name
        self.ref_sens = cor_pres


    def _correct_height(self, input_pres, height_start, height_end):
        """
        This mehtod recieves a pressure value and the two heights to performs
        a height correction. It returns the height corrected pressure.
        Needed: - Input heights
                - g, R
                - Input pressure
                - Ambient temperature
        """
        height_corr_pres = input_pres*((1+(self.g*height_start)/ \
            (self.working_fluid.R*self.amb_temp))/ \
            (1+(self.g*height_end)/(self.working_fluid.R*self.amb_temp)))

        return height_corr_pres


    def _get_pres(self):
        """
        This method takes the reference pressure if available and performs the
        referencing and the height correction.
        Returns the height corrected and referenced pressure.
        """
        sens_pres = {}
        ref_meas_height = 0

        #get the ref pres value
        if self.ref_sens != 0:
            self.ref_pres = self.ref_sens.values[self.ref_pres_name]

        # get the ref measuring height:
        if self.ref_pres_name != "":
            ref_meas_height = self.meas_heights[self.ref_pres_name]

        # get the pres value at the measuring sensor:
        for sk in self.sens:
            sens_pres[sk] = {}
            for pk in self.sens[sk]:
                sens_pres[sk][pk] = self.sens[sk][pk] \
                + self._correct_height(self.ref_pres, ref_meas_height, self.sensor_heights[sk])

        # perform the height correction of the pressure at sensor height to the height of the measurement point
        for sk in self.sens:
            for pk in self.sens[sk]:
                self.values[pk] = self._correct_height(sens_pres[sk][pk], self.sensor_heights[sk], self.meas_heights[pk])


    def perform(self, values, sensors):
        """
        This method returns the corrected pressures to the setup class.
        It also gets the sensor classes and the measured values.
        """
        # fill pres_sensors dict with values:
        for pk in self.sens:
            for chan in self.sens[pk]:
                self.sens[pk][chan] = values[pk][chan.split(':')[1]]

        # get the heights:
        for sclass in sensors:
            self.sensor_heights[sensors[sclass].name] = sensors[sclass].height
            self.meas_heights.update(sensors[sclass].meas_heights)

        # get the ambient temperature:
        self.amb_temp = values[self.amb_temp_name.split(':')[0]][self.amb_temp_name.split(':')[1]]

        # perform the computation
        self._get_pres()

        return self.values
