#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (C) 2022 Daniel Jaeger

This file is part of PythonDAQ.

PythonDAQ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PythonDAQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PythonDAQ.  If not, see <http://www.gnu.org/licenses/>.

@author: Daniel Jaeger
@date: 2021
"""

from .computation import Computation

class DemoComputation(Computation):
    """
    Class to test Computation class
    """
    def __init__(self, name="DemoComputation"):
        """
        Initialization method
        """
        super().__init__(name)
        self.return_names = ["Sum"]


    def perform(self, values):
        """
        In this method the actual computation takes place.
        """
        summe = 1+1
        return [summe]
