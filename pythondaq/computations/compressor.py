#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (C) 2022 Lukas Selmayr

This file is part of PythonDAQ.

PythonDAQ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PythonDAQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PythonDAQ.  If not, see <http://www.gnu.org/licenses/>.

@author: Lukas Selmayr
@date: 2021-2022
"""

from .computation import Computation
import math

class Compressor(Computation):
    """
    This class calculates the values of the compressor. It calculates the total pressure ratio, the inlet massflow, the corrected massflow,
    the corrected speed, the rotational speed, the mechanical power, the power over temperature differences, the polytropic efficiency and
    the isentropic efficiency.
    """
    def __init__(self, name, working_fluid, environment, name_speed, name_massflow, name_torque):
        """
        This method initializes the class. It sets the values to zero, defines the needed variables and the return names.
        """
        # Call the parent class initialization method
        super().__init__(name, num_returns = 18)

        self.units = ["-", "kg/s", "kg/s", "kg/s", "1/min", "m/s", "W", "W", "W", "-", "-", "-", "-", "-", "-", "%", "%", "%"]
        self.return_names = ["Total pressure ratio",
                             "Inlet Massflow", "Corrected Massflow", 
                             "Measured corrected massflow", "Corrected Speed",
                             "Rotational Speed", "Mechnical power", 
                             "Temp power mCal", "Temp power mMeas", "EtaPol mCal", "EtaPol mMeas", "EtaPol temp",
                             "EtaIs mCal", "EtaIs mMeas", "EtaIs temp",
                             "Percent Speed corr", "Percent mCal corr", "Percent mMeas corr"]

        self.values = {}

        self.values["Total pressure ratio"] = 0
        self.values["Inlet Massflow"] = 0
        self.values["Corrected Massflow"] = 0
        self.values["Measured corrected massflow"] = 0
        self.values["Corrected Speed"] = 0
        self.values["Rotational Speed"] = 0
        self.values["Mechnical power"] = 0
        self.values["Temp power mCal"] = 0
        self.values["Temp power mMeas"] = 0
        self.values["EtaPol mCal"] = 0
        self.values["EtaPol mMeas"] = 0
        self.values["EtaPol temp"] = 0
        self.values["EtaIs temp"] = 0
        self.values["EtaIs mCal"] = 0
        self.values["EtaIs mMeas"] = 0
        self.values["Percent Speed corr"] = 0.0
        self.values["Percent mCal corr"] = 0.0
        self.values["Percent mMeas corr"] = 0.0

        self.working_fluid = working_fluid
        self.environment = environment

        self.name_speed = name_speed
        self.name_massflow = name_massflow
        self.name_torque = name_torque
        self.inlet = 0
        self.outlet = 0
        self.inlet_area = 0

        self.design_speed = 100.0
        self.design_massflow = 1.0


    def setInlet(self, comp_lvl):
        """
        Gets the inlet compressor level.
        """
        self.inlet = comp_lvl

    def setOutlet(self, comp_lvl):
        """
        Gets the outlet compressor level.
        """
        self.outlet = comp_lvl


    def setDesignPoint(self, design_speed_corr, design_massflow_corr):
        """
        Set the parameters of the compressor's design point:
            Corrected speed at design point in 1/min
            Corrected massflow at design point in 1/min
        """
        self.design_speed = design_speed_corr
        self.design_massflow = design_massflow_corr


    def _get_total_pressure_ratio(self):
        """
        This method gets the average total pressure of the inlet rake and the
        outlet rake and returns the total pressure 
        """
        if (self.inlet == 0 or self.outlet == 0):
            raise Exception("Inlet and Outlet must be set")

        self.values["Total pressure ratio"] = \
            self.outlet.values["Average total pressure"]\
            /self.inlet.values["Average total pressure"]


    def _get_corr_massflow(self):
        """
        This method takes the average static pressure and temperature and gets 
        the density at the inlet rake. It also takes the rake Machnumber and
        average static temperature and gets the average velocity. Together with
        the inlet area of the compressor it gets the massflow. With the average
        total pressure and temperature it returns the corrected massflow of the
        inlet area. It also the measured massflow, the average total pressure, 
        temperature, the ISA pressure and temperature and returns the 
        measured corrected massflow
        """
        # A = (R²*r²)*pi
        self.inlet_area = \
                    (self.inlet.outRad**2 - self.inlet.inRad**2)*math.pi

        # m = rho*c*A
        self.values["Inlet Massflow"] = \
                    self.inlet.values["Average density"]* self.inlet.values["Average velocity"]\
                    * self.inlet_area

        # Massflow correction factor:
        # Standard definition: m_corr = m*sqrt(Tt/TISA)*(pISA/pt)
        # Extended definition: m_corr = m*sqrt((Tt*R*kappaISA)/(TISA*RISA*kappa))*(PISA/p)
        correction_factor = \
            math.sqrt((self.inlet.values["Average total temperature"] * self.working_fluid.R * self.environment.kappa_ISA) / \
            (self.environment.T_ISA * self.environment.R_ISA * self.working_fluid.kappa)) * \
            (self.environment.p_ISA/self.inlet.values["Average total pressure"])

        # Compute the corrected massflow
        self.values["Corrected Massflow"] = self.values["Inlet Massflow"] * correction_factor
        self.values["Measured corrected massflow"] = self.meas_massflow * correction_factor


    def _get_corr_speed(self):
        """
        This method takes the measured speed, the inlet average total temperature and
        the ISA temperature and returns the corrected speed
        """
        self.values["Corrected Speed"] = self.meas_speed * \
            math.sqrt((self.environment.kappa_ISA * self.environment.R_ISA * self.environment.T_ISA) / \
            (self.working_fluid.kappa * self.working_fluid.R * self.inlet.values["Average total temperature"]))


    def _get_rot_speed(self):
        """
        This method gets the measured speed, the outer and inner radius of the 
        compressor and returns the rotational speed.
        """
        self.values["Rotational Speed"] = \
                    (self.meas_speed)*math.sqrt((self.inlet.outRad**2+self.inlet.inRad**2)/2)*(2*math.pi)/60


    def _get_power(self):
        """
        This method takes the measured torque, speed, the average inlet and 
        outlet total temperature and returns the power.
        """
        self.values["Mechnical power"] = \
                    self.torque * 2 * math.pi * (self.meas_speed/60)
                    
        self.values["Temp power mCal"] = \
                    self.values["Inlet Massflow"]\
                    *self.working_fluid.cp*(self.outlet.values["Average total temperature"]\
                    -self.inlet.values["Average total temperature"])

        self.values["Temp power mMeas"] = \
                    self.meas_massflow\
                    *self.working_fluid.cp*(self.outlet.values["Average total temperature"]\
                    -self.inlet.values["Average total temperature"])


    def _get_eta_pol(self):
        """
        This method first takes the average inlet pressure, temperature, the 
        average outlet pressure, the measured torque, the speed and the 
        massflow and returns the etaPol. Then it takes the average
        inlet pressure, temperature, the average outlet temperature and 
        pressure and returns the etaPol from the temperature.
        """
        if self.values["Inlet Massflow"] != 0 and (self.torque*2*math.pi*self.meas_speed/60)/(self.values["Inlet Massflow"]\
            *self.working_fluid.cp*self.inlet.values["Average total temperature"]) > (-1) and \
           self.outlet.values["Average total pressure"] != 0 and self.inlet.values["Average total pressure"] != 0 and self.torque != 0 and self.meas_speed != 0:

            self.values["EtaPol mCal"] = \
                    ((self.working_fluid.kappa-1)/self.working_fluid.kappa)*((math.log( \
                    self.outlet.values["Average total pressure"] \
                    /self.inlet.values["Average total pressure"])) \
                    /(math.log((self.torque*2*math.pi*self.meas_speed/60) \
                      /(self.values["Inlet Massflow"] \
                        *self.working_fluid.cp*self.inlet.values["Average total temperature"])+1)))

        else: self.values["EtaPol mCal"] = 0

        if self.meas_massflow != 0 and (self.torque*2*math.pi*self.meas_speed/60)/(self.values["Inlet Massflow"]\
            *self.working_fluid.cp*self.inlet.values["Average total temperature"]) > (-1) and \
           self.outlet.values["Average total pressure"] != 0 and self.inlet.values["Average total pressure"] != 0 and self.torque != 0 and self.meas_speed != 0:

            self.values["EtaPol mMeas"] = \
                    ((self.working_fluid.kappa-1)/self.working_fluid.kappa)*((math.log( \
                    self.outlet.values["Average total pressure"] \
                    /self.inlet.values["Average total pressure"])) \
                    /(math.log((self.torque*2*math.pi*self.meas_speed/60) \
                      /(self.meas_massflow \
                        *self.working_fluid.cp*self.inlet.values["Average total temperature"])+1)))

        else: self.values["EtaPol mMeas"] = 0

        if self.outlet.values["Average total temperature"] != 0 and self.inlet.values["Average total temperature"] != 0 and \
           self.outlet.values["Average total pressure"] != 0 and self.inlet.values["Average total pressure"] != 0 \
           and self.outlet.values["Average total temperature"] != self.inlet.values["Average total temperature"]:

               self.values["EtaPol temp"] = \
                    ((self.working_fluid.kappa-1)/self.working_fluid.kappa)* math.log( \
                    self.outlet.values["Average total pressure"]\
                    /self.inlet.values["Average total pressure"]) \
                    /math.log(self.outlet.values["Average total temperature"]\
                    /self.inlet.values["Average total temperature"])

        else: self.values["EtaPol temp"] = 0


    def _get_eta_is(self):
        """
        This method first takes the average inlet temperature, pressure, the 
        average outlet pressure, the inlet massflow, the torque and the speed 
        and returns etaIs machanical. Then it takes the average inlet pressure,
        temperature, the average outlet temperature and pressure and returns 
        etaIs temp.
        """
        if self.inlet.values["Average total pressure"] != 0 and \
            self.torque*2*math.pi*(self.meas_speed/60) != 0:

            self.values["EtaIs mCal"] = \
                (self.values["Inlet Massflow"]*self.working_fluid.cp \
                 *self.inlet.values["Average total temperature"] \
                 *((self.outlet.values["Average total pressure"] \
                 /self.inlet.values["Average total pressure"]) \
                 ** ((self.working_fluid.kappa-1)/self.working_fluid.kappa)-1))/(self.torque*2*math.pi*(self.meas_speed/60))

            self.values["EtaIs mMeas"] = \
                (self.meas_massflow*self.working_fluid.cp \
                 *self.inlet.values["Average total temperature"] \
                 *((self.outlet.values["Average total pressure"] \
                 /self.inlet.values["Average total pressure"]) \
                 ** ((self.working_fluid.kappa-1)/self.working_fluid.kappa)-1))/(self.torque*2*math.pi*(self.meas_speed/60))

        else: self.values["EtaIs mCal"] = 0

        if self.inlet.values["Average total pressure"] != 0 and \
            self.outlet.values["Average total temperature"] - self.inlet.values["Average total temperature"] != 0:

            self.values["EtaIs temp"] = \
                (self.inlet.values["Average total temperature"]\
                 *((self.outlet.values["Average total pressure"] \
                 /self.inlet.values["Average total pressure"]) \
                 ** ((self.working_fluid.kappa-1)/self.working_fluid.kappa)-1)) \
                 /(self.outlet.values["Average total temperature"]\
                 -self.inlet.values["Average total temperature"])

        else: self.values["EtaIs temp"] = 0


    def _get_percent_values(self):
        """
        Get the percent of design speed and design massflow
        """
        self.values["Percent Speed corr"] = self.values["Corrected Speed"] / \
            self.design_speed * 100.0
        self.values["Percent mCal corr"] = self.values["Corrected Massflow"] / \
            self.design_massflow * 100.0
        self.values["Percent mMeas corr"] = self.values["Measured corrected massflow"] / \
            self.design_massflow * 100.0


    def perform(self, values, sensors):
        """
        This method gets the measured speed, the measured massflow and the measured torque.
        Afterwards it performs the calcuations and returns the values.
        """
        # get meas_speed, meas_massflow, torque:
        self.meas_speed = values[self.name_speed.split(':')[0]][self.name_speed.split(':')[1]]
        self.meas_massflow = values[self.name_massflow.split(':')[0]][self.name_massflow.split(':')[1]]
        self.torque = values[self.name_torque.split(':')[0]][self.name_torque.split(':')[1]]

        self._get_total_pressure_ratio()
        self._get_corr_massflow()
        self._get_corr_speed()
        self._get_rot_speed()
        self._get_power()
        self._get_eta_pol()
        self._get_eta_is()
        self._get_percent_values()

        return self.values
