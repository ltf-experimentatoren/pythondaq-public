#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (C) 2022 Lukas Selmayr

This file is part of PythonDAQ.

PythonDAQ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PythonDAQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PythonDAQ.  If not, see <http://www.gnu.org/licenses/>.

@author: Lukas Selmayr
@date: 2021-2022
"""

from .computation import Computation
from utils import averaging
import math


class Rake(Computation):
    """
    This class gets the corrected pressures, the measured temperatures and, the
    radii of the pressure sensors and the compressor and computes the average
    pressures and temperature, the Machnumber and the reynoldsnumber at the rake.
    """
    def __init__(self, name, working_fluid, corr_tot_pres, corr_stat_pres):
        """
        Initialization method:
            For the initialization needed: The given name of the computation,
            the ideal gas working_fluid (this is defined before the initialization
            of this computation), a list of the corrected total pressures, a list
            of the corrected static pressures and a list of the temperature sensors.
        """
        super().__init__(name, num_returns = 9)

        self.return_names = ["Average total pressure", "Average static pressure",
                             "Machnumber", "Average total temperature", "Average static temperature",
                             "Dynamic viscosity", "Density", "Flow velocity", "Reynoldsnumber"]
        self.units = ["Pa", "Pa", "-", "K", "K", "kg/ms", "kg/m³", "m/s", "-"]
        self.corr_tot_pres = corr_tot_pres
        self.corr_stat_pres = corr_stat_pres
        self.working_fluid = working_fluid
        self.temp_sens_rec = {}
        self.tot_temp = {}
        self.stat_temp = {}
        self.values = {}
        self.tot_pres_sens_radi = {}
        self.stat_pres_sens_radi = {}
        self.temp_sens_radi = {}
        self.rake_radi = [1,0]
        self.tot_pres_sensors = {}
        self.stat_pres_sensors = {}
        self.stat_pres_names = []
        self.circ_pos = 0
        self.circ_pos_stat_pres = {}


    def setStatPresNames(self, stat_pres):
        """
        This method takes the four static pressures for the rake and saves their names
        """
        self.stat_pres_names = stat_pres


    def setTempNames(self, temp_names):
        """
        This method takes the temperature channels for the rake and saves their names
        """
        self.temp_names = temp_names

        # initialize the temp sensor dict
        self.temp_sensors = {}
        for tsens in temp_names:
            self.temp_sensors[tsens.split(':')[0]] = {}
            self.temp_sens_rec[tsens.split(':')[0]] = {}
            self.temp_sensors[tsens.split(':')[0]][tsens.split(':')[1]] = 0.0
            self.temp_sens_rec[tsens.split(':')[0]][tsens.split(':')[1]] = 1.0

        # Fill the return_names list and the number of returns
            self.return_names.append(tsens)
            self.units.append("K")
            self.computation_range.append([0.0,1.0])
            self.value_range.append([0.0,1.0])
            self.num_returns += 1


    def setCompRadii(self, radii):
        """
        This method takes the radii of the rake from hub to tip in [m]
        """
        self.rake_radi = radii


    def setCircPosition(self, circ_pos):
        """
        This method takes the circumferentail position of the rake for
        the averaging of the pressures at the compressor levels
        """
        self.circ_pos = circ_pos


    def setYawAngle(self, yaw_angle):
        """
        This method gets the known yaw angle of the flow for the calculation
        of the massflow
        """
        self.yaw_angle = yaw_angle


    def _get_av_tot_pres(self):
        """
        Takes the corrected pressures of the total pressure sensors and gets the pressure average for the rake
        """

        if self.tot_pres_sensors == {}:
            raise Exception("You have to define total pressure sensors")
            return 0

        self.values["Average total pressure"] = averaging.getCorrAverage(self.tot_pres_sensors, self.tot_pres_sens_radi, self.rake_radi)


    def _get_av_stat_pres(self):
        """
        Takes the corrected pressures of the static pressure sensors and gets the average static pressure at the walls
        at the circ_pos of the rake and then the average pressure at the rake
        """
        self.values["Average static pressure"] = 0
        if self.stat_pres_sensors == {}:
            raise Exception("You have to define static pressure sensors")
            return 0

        if len(self.stat_pres_names) < 2 or len(self.stat_pres_names) > 4 or len(self.stat_pres_names) == 3:
            raise Exception("You have to define four static pressures around the rake on both compressor radii or two static pressures on one compressor radius!")
            return 0

        av_stat_pres_out = 0
        av_stat_pres_in = 0

        # get the average static pressure on the inner compressor radius
        if min(self.rake_radi) in self.stat_pres_sens_radi.values():
            av_stat_pres_in = averaging.getLinInterpolation(averaging.getPresOnRad(self.stat_pres_sensors, self.stat_pres_sens_radi, self.circ_pos_stat_pres, min(self.rake_radi))[0],\
            averaging.getPresOnRad(self.stat_pres_sensors, self.stat_pres_sens_radi, self.circ_pos_stat_pres, min(self.rake_radi))[1], self.circ_pos)

        # get the average static pressure on the outer compressor radius
        if max(self.rake_radi) in self.stat_pres_sens_radi.values():
            av_stat_pres_out = averaging.getLinInterpolation(averaging.getPresOnRad(self.stat_pres_sensors, self.stat_pres_sens_radi, self.circ_pos_stat_pres, max(self.rake_radi))[0],\
            averaging.getPresOnRad(self.stat_pres_sensors, self.stat_pres_sens_radi, self.circ_pos_stat_pres, max(self.rake_radi))[1], self.circ_pos)

        # if the static pressures are not on the compressor radii something is wrong
        if av_stat_pres_in and av_stat_pres_out == 0:
            raise Exception("Your static pressure sensors are not valid. Check if the circ_pos, the sens_radi and the rake_radi is correct.")
            return 0

        # when static pressure sensors are at both compressor radii the average for the compressor position is calculated
        if av_stat_pres_in and av_stat_pres_out != 0:
            self.values["Average static pressure"] = (av_stat_pres_in+av_stat_pres_out)/2

        # when there are only pressure sensors at one of the two compressor radii
        else:
            self.values["Average static pressure"] = av_stat_pres_in+av_stat_pres_out


    def _get_Ma(self):
        """
        Takes the average static and total pressure of the rake and returns the average Machnumber of the rake
        """
        val = self.values["Average total pressure"]/self.values["Average static pressure"]
        p = ((self.working_fluid.kappa-1)/self.working_fluid.kappa)
        if math.pow(val, p) > 1: self.values["Machnumber"] = math.sqrt((2/(self.working_fluid.kappa-1))*(math.pow(val, p)-1))
        else: self.values["Machnumber"] = 0


    def _get_temps(self):
        """
        Gets the recovery temperature from the sensors and returns the static and total temperatures at the sensor 
        and the average temperature at the rake
        """
        kap_half = (self.working_fluid.kappa-1)/2
        for tsk in self.temp_sensors:
            self.tot_temp[tsk] = {}
            self.stat_temp[tsk] = {}
            for tck in self.temp_sensors[tsk]:
                self.tot_temp[tsk][tck] \
                = self.temp_sensors[tsk][tck] \
                * ((1+kap_half*self.values["Machnumber"]**2)/(1+self.temp_sens_rec[tsk][tck]*kap_half*self.values["Machnumber"]**2))


                self.stat_temp[tsk][tck] \
                = self.temp_sensors[tsk][tck] \
                / (1+self.temp_sens_rec[tsk][tck]*kap_half*self.values["Machnumber"]**2)


        self.values["Average total temperature"] = averaging.getCorrAverage(self.tot_temp, self.temp_sens_radi, self.rake_radi)
        self.values["Average static temperature"] = averaging.getCorrAverage(self.stat_temp, self.temp_sens_radi, self.rake_radi)


    def _get_reynoldsnumber(self):
        """
        Takes the average static pressure, temperature, the average flow 
        velocity, the dynamic viscosity and the outer and inner radius of the compressor and returns
        the average Reynoldsnumber at the inlet rake
        """
        eta0 = 1.5028*10**(-6)
        Tss = 123.6

        self.values["Dynamic viscosity"] = \
            eta0*((self.values["Average static temperature"]**(3/2))/(self.values["Average static temperature"]+Tss))

        # Density = ps/R*Ts
        self.values["Density"] = \
                    self.values["Average static pressure"]   \
                    /(self.working_fluid.R*self.values["Average static temperature"])

        # c = M * sqrt(kappa*R*Ts)
        self.values["Flow velocity"] = \
                    self.values["Machnumber"]   \
                    * math.sqrt(self.working_fluid.kappa * self.working_fluid.R * \
                    self.values["Average static temperature"])

        self.values["Reynoldsnumber"] = \
            (self.values["Flow velocity"]*self.values["Density"] \
            *(2*max(self.rake_radi)-2*min(self.rake_radi))) \
            /self.values["Dynamic viscosity"]


    def perform(self, values, sensors):
        """
        This method gets the values of the corrected total and static pressures and the measured temperatures.
        Afterwards it performs the calculations and returns the values of the calculations.
        """
        # fill total pressure sensors dict with values:
        for ptsens in self.corr_tot_pres:
            self.tot_pres_sensors[ptsens] = {}
            for pti in ptsens.return_names:
                self.tot_pres_sensors[ptsens][pti] = ptsens.values[pti.replace("_corr","")]
            # get the circ_pos, rad_pos of the total pressure channels
            for k in ptsens.sens:
                for sk in sensors:
                    if k in sk:
                        self.circ_pos_stat_pres[ptsens] = sensors[sk].circ_pos
                        self.tot_pres_sens_radi.update(sensors[sk].rad_pos)

        # fill static pressure sensors dict with values:
        for pssens in self.corr_stat_pres:
            for chan in pssens.return_names:
                if chan.replace("_corr","") in self.stat_pres_names:
                    self.stat_pres_sensors[pssens] = {}

            # get the circ_pos, rad_pos of the static pressure channels
            for k in pssens.sens:
                for sk in sensors:
                    if k in sk:
                        for chan in pssens.sens[k]:
                            self.circ_pos_stat_pres[pssens] = sensors[sk].circ_pos
                            self.stat_pres_sens_radi[chan] = sensors[sk].rad_pos[chan]

            for chan in pssens.return_names:
                if chan.replace("_corr","") in self.stat_pres_names:
                    self.stat_pres_sensors[pssens][chan] = pssens.values[chan.replace("_corr","")]
    
        # fill temperature sensors dict with values:
        for tsk in self.temp_names:
            self.temp_sensors[tsk.split(':')[0]][tsk.split(':')[1]] = values[tsk.split(':')[0]][tsk.split(':')[1]]
            self.temp_sens_rec[tsk.split(':')[0]][tsk.split(':')[1]] = sensors[tsk.split(':')[0]].rec[tsk]
            self.temp_sens_radi.update(sensors[tsk.split(':')[0]].rad_pos)

        # perform the computations
        self._get_av_tot_pres()
        self._get_av_stat_pres()
        self._get_Ma()
        self._get_temps()
        self._get_reynoldsnumber()
        
        # fill return dict with the remaining temperature values
        for tsk in self.tot_temp:
            for tck in self.tot_temp[tsk]:
                self.values[tck] = self.tot_temp[tsk][tck]

        return self.values
