#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (C) 2022 Lukas Selmayr

This file is part of PythonDAQ.

PythonDAQ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PythonDAQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PythonDAQ.  If not, see <http://www.gnu.org/licenses/>.

@author: Lukas Selmayr
@date: 2021-2022
"""

from .computation import Computation
import math


class DimensionlessCompressor(Computation):
    """
    This class calculates the work coefficient and the row coefficients.
    """
    def __init__(self, name, compressor, flow_levels):
        """
        This method initialzes the variables and sets the return names.
        """
        super().__init__(name, num_returns = 2)

        self.return_names = ["Work coefficient mCal", "Work coefficient mMeas"]
        self.units = ["-", "-"]
        self.compressor = compressor
        self.flow_levels = flow_levels
        self.alpha = math.pi/2
        self.values = {}

        # Coefficients for each compressor row
        for flev in flow_levels:
            self.return_names.append("Row work coefficient " + flev.name)
            self.units.append("-")
            self.computation_range.append([0.0, 1.0])
            self.value_range.append([0.0, 1.0])
            self.num_returns += 1


    def setAlpha(self, alpha):
        """
        Sets alpha to get c_m in _get_flow_coefficient.
        IMPORTANT: Alpha is in degree.
        """
        self.alpha = alpha/360*2*math.pi


    def _get_flow_coefficient(self, alpha):
        """
        Takes the rotational speed, the average flow velocity and returns the
        row coefficient
        """
        for flev in self.flow_levels:
            if self.compressor.values["Rotational Speed"] != 0:

                self.values["Row coefficient " + flev.name] = \
                    (flev.values["Average velocity"] *math.sin(alpha))\
                    / self.compressor.values["Rotational Speed"]

            else: self.values["Row coefficient " + flev.name] = 0


    def _get_work_coefficient(self):
        """
        Takes the measured speed, torque, the rotational speed and the massflow
        and returns the work coefficient
        """
        if self.compressor.values["Inlet Massflow"] != 0 and self.compressor.values["Rotational Speed"] !=0:

            self.values["Work coefficient mCal"] = \
                 (self.compressor.torque*2*math.pi*self.compressor.meas_speed/60) \
                 /(self.compressor.values["Inlet Massflow"]*self.compressor.values["Rotational Speed"]**2)

        else: self.values["Work coefficient mCal"] = 0

        if self.compressor.meas_massflow != 0 and self.compressor.values["Rotational Speed"] !=0:

            self.values["Work coefficient mMeas"] = \
                 (self.compressor.torque*2*math.pi*self.compressor.meas_speed/60) \
                 /(self.compressor.meas_massflow*self.compressor.values["Rotational Speed"]**2)

        else: self.values["Work coefficient mMeas"] = 0


    def perform(self, values, sensors):
        """
        This method performs the calculations.
        """
        self._get_work_coefficient()
        self._get_flow_coefficient(self.alpha)

        return self.values
