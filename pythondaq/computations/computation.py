# -*- coding: utf-8 -*-
"""
Created on Mon Jul 19 17:04:58 2021

@author: Simon Stolz
"""

class Computation():
    """
    Abstract class for the implementation of an online computation.
    """
    def __init__(self,name, num_returns = 1):
        """
        Initialization method
        """
        self.name = name

        self.num_returns = num_returns
        self.return_names = ["Return_1"]
        self.units = ["-"]

        # The range, the computation can handle
        self.computation_range = [[0.0, 1.0] for i in range(self.num_returns)]
        # The expected range of the return signal
        self.value_range = [[0.0, 1.0] for i in range(self.num_returns)]


    def perform(self,values,sensors):
        """
        In this method the actual computation should take place.
        Reimplement this method for the actual used computation
        """
        return [0]
