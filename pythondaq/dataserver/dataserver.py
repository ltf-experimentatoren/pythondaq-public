#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (C) 2022 Daniel Jaeger

This file is part of PythonDAQ.

PythonDAQ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PythonDAQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PythonDAQ.  If not, see <http://www.gnu.org/licenses/>.

This module contains the central data server of PythonDAQ

@author: Daniel Jaeger
@date: 2020
"""

# Time loop is realized with the advanced python scheduler
from apscheduler.schedulers.background import BackgroundScheduler
from opcua import ua, Server
import datetime
import time

from .opcuaunits import UaUnitDescriptor
from sensors.sensor import Sensor
from computations.computation import Computation
from datastorage.csvhandler import CSVWriter
from datastorage.hdfhandler import HDFWriter


class DataServer():
    """
    Class to implement the central data server of the DAQ System
    """
    def __init__(self, server_ip = "127.0.0.1", interval = 0.5):
        """
        Constructor function which includes some initialization code.
        The interval is the acquisition and data storage interval in seconds.
        """
        # Initialize some lists
        self.serverinfo = {}
        self.sensors = {}
        self.computations = {}
        self.opc_channels = []
        self.values = {}

        # Some storage varibles
        self.server_ip = server_ip
        self.interval = interval
        self.t_start = time.time()

        # Initialize the writer variable
        self.writers = []

        # Display welcome message
        print("Welcome to PythonDAQ! Copyright (C) 2022  Daniel Jaeger, and others;")
        print("see CONTRIBUTING for the full list of contributors.")
        print("This program comes with ABSOLUTELY NO WARRANTY.")
        print("This is free software, and you are welcome to redistribute it")
        print("under certain conditions; see COPYING for details.\n")

        # Initialize the opc server
        self.unitdescriptor = UaUnitDescriptor()
        self.initializeOPCServer()


    def initializeOPCServer(self):
        """
        Initialize and configure the opc server
        """
        # Initialize the opc server
        self.opc_server = Server()
        url_server = f"opc.tcp://{self.server_ip}:4840"
        self.opc_server.set_endpoint(url_server)

        # Create an adress space
        self.opc_addspace = self.opc_server.register_namespace("OPCUA_DAQ_SERVER")

        # Create the head node
        self.node_head = self.opc_server.get_objects_node()

        # Create a node with information of the data server
        node_server = self.node_head.add_object(self.opc_addspace, "DataServer")

        # Execution time
        self.serverinfo['exectime'] = node_server.add_variable( \
            self.opc_addspace, "Execution Time", 0.0)
        # Set the EngineeringUnits of the execution time
        self.unitdescriptor.search_displayname("ms")
        eu = self.unitdescriptor.get_opcua_euinformation()
        self.serverinfo['exectime'].add_property(self.opc_addspace,\
            "EngineeringUnits", eu, datatype = 887)

        # Loop time
        self.serverinfo['looptime'] = node_server.add_variable( \
            self.opc_addspace, "Loop Time", 0.0)
        # Set the engineering units of the loop time
        eu = self.unitdescriptor.get_opcua_euinformation()
        self.serverinfo['looptime'].add_property(self.opc_addspace,\
            "EngineeringUnits", eu, datatype = 887)

        # Create folders for all our sensors and computations
        # so the tree gets organized
        self.folder_sensors = self.opc_server.nodes.objects.add_folder( \
            self.opc_addspace, "Sensors")
        self.folder_computations = self.opc_server.nodes.objects.add_folder( \
            self.opc_addspace, "Computations")


    def addWriter(self,filename,max_pts = -1):
        """
        Initializes a writer object to store measurement data.
        The writer type depends on the filename extension.

        @author: Patrick Bachmann
        """
        # Add Writer-Class dependend on file extension
        if filename[-4:] == ".csv":
            self.writers.append(CSVWriter(filename, self.sensors, self.computations, max_pts))
        elif filename[-5:] == ".hdf5":
            self.writers.append(HDFWriter(filename, self.sensors, self.computations, max_pts))
        else:
            print("Error: Wrong Filename!")


    def addSensor(self,sensor):
        """
        Add a sensor to the server
        """
        if not isinstance(sensor, Sensor):
            raise Exception("Sensor has to be of type 'Sensor'")
            return

        # Assign the sensor object to the dict
        self.sensors[sensor.name] = sensor

        # Create a new opc-node for the sensor
        node_sensor = self.folder_sensors.add_object(self.opc_addspace, sensor.name)

        # Create a sub-list for the sensor's channels
        self.opc_channels.append([])
        self.values[sensor.name] = {}

        # Add a channel for the timing information
        # TODO: Rework this using opc server's timing
        #self.time = node_sensor.add_variable(self.opc_addspace, "Time", 0)
        #self.time.set_writable()


        # Add one channel in the sub-list for each sensor channel
        for i in range(sensor.num_channels):
            # Create the OPC variable
            var = node_sensor.add_variable(self.opc_addspace, \
                sensor.channel_names[i], 0.0, ua.VariantType.Float, 17570)

            # Set the InstrumentRange
            inst_range = ua.Range()
            inst_range.Low = sensor.sensor_range[i][0]
            inst_range.High = sensor.sensor_range[i][1]
            var.add_property(self.opc_addspace, "InstrumentRange", inst_range, datatype = 884)

            # Set the EURange
            eu_range = ua.Range()
            eu_range.Low = sensor.value_range[i][0]
            eu_range.High = sensor.value_range[i][1]
            var.add_property(self.opc_addspace, "EURange", eu_range,datatype = 884)

            # Set the EngineeringUnits
            self.unitdescriptor.search_displayname(sensor.units[i])
            eu = self.unitdescriptor.get_opcua_euinformation()
            var.add_property(self.opc_addspace, "EngineeringUnits", eu, datatype = 887)

            # Store our OPC variable in the dictionary
            self.opc_channels[-1].append(var)

            # Also initialize the shape of the value array
            self.values[sensor.name][sensor.channel_names[i]] = 0.0


    def addComputation(self,computation):
        """
        Add a computation to the server
        """
        if not isinstance(computation, Computation):
            raise Exception("Computation has to be of type 'Computation'")
            return

        # Assign the computation object to the list
        self.computations[computation.name] = computation

        # Create a new op-node for the sensor
        node_computation = self.folder_computations.add_object(self.opc_addspace, computation.name)

        # Create a sub-list for the computation's return values
        self.opc_channels.append([])
        self.values[computation.name] = {}

        # Add one return value in the sub-list for each computation return value
        for i in range(computation.num_returns):
            # Create the OPC variable
            var = node_computation.add_variable(self.opc_addspace, \
                computation.return_names[i], 0.0, ua.VariantType.Float, 17570)

            # Set the InstrumentRange
            inst_range = ua.Range()
            inst_range.Low = computation.computation_range[i][0]
            inst_range.High = computation.computation_range[i][1]
            var.add_property(self.opc_addspace, "InstrumentRange", inst_range, datatype = 884)

            # Set the EURange
            eu_range = ua.Range()
            eu_range.Low = computation.value_range[i][0]
            eu_range.High = computation.value_range[i][1]
            var.add_property(self.opc_addspace, "EURange", eu_range, datatype = 884)

            # Set the EngineeringUnits
            self.unitdescriptor.search_displayname(computation.units[i])
            eu = self.unitdescriptor.get_opcua_euinformation()
            var.add_property(self.opc_addspace, "EngineeringUnits", eu, datatype = 887)

            # Store our OPC variable in the list
            self.opc_channels[-1].append(var)

            # Also initialize the shape of the value array
            self.values[computation.name][computation.return_names[i]] = 0.0


    def start(self):
        """
        Start the server's main loop
        """
        # Start the opc server
        self.opc_server.start()
        # Start the scheduler to do the loop
        self.scheduler = BackgroundScheduler()
        self.job = self.scheduler.add_job(self.measureAndStore, 'interval', \
            seconds=self.interval)
        self.scheduler.start()


    def stop(self):
        """
        Shut down the server
        """
        self.job.remove()
        self.scheduler.shutdown()
        # Close network connection
        for sens in self.sensors:
            self.sensors[sens].close()

        # Stop the writer if it was set up
        for writer in self.writers:
            writer.close()

        # Stop the OPC server
        self.opc_server.stop()


    def measureAndStore(self):
        """
        Get the measurement data from the sensors,
        perform online computations
        and feed the data into the opc server
        """
        # TODO For a big amount of sensors we need a way to parallelize this task

        # Record the time in which we start the iteration and the loop time
        loop_time = (time.time() - self.t_start)*1000.0
        self.t_start = time.time()

        # Get the values from each sensor and assign it to the channels
        for sens in self.sensors.values():
            self.values[sens.name] = sens.getValue()[1]
            # TODO: Fix the timesteps
            #self.time.set_value(timestamp)

        # Perform online computations
        for comp in self.computations.values():
            self.values[comp.name] = comp.perform(self.values, self.sensors)

        # Feed the data into the opc server
        for idx_dev, dev in enumerate(self.opc_channels):
            for idx_ch, channel in enumerate(dev):
                channel.set_value(self.values[list(self.values)[idx_dev]][list(self.values[list(self.values)[idx_dev]])[idx_ch]])

        # Store the data if a writer is configured
        for i, writer in enumerate(self.writers):
            writer.writeData(self.values)
            if writer.maxReached():
                writer.close()
                self.writers.pop(i)
                print("Writing finished")

        # Write the timing information to the OPC server
        self.serverinfo['looptime'].set_value(loop_time)
        exec_time = (time.time() - self.t_start)*1000.0
        self.serverinfo['exectime'].set_value(exec_time)


    def triggerMeasurement(self,name="measurement",pts=40, fileformat = "hdf5"):
        """
        Start a single measurement.
        Parameters:
            name ... The name of the measurement.
                     Date, time and file extension will be added automatically.
            pts ... The number of points in the measurement.
            fileformat ... The file format and extension (e.g. hdf5 or csv), without dot!
        """
        time = datetime.datetime.now()
        name = f"measurements/{time.strftime('%Y-%m-%d_%H-%M-%S')}_{name}.{fileformat}"
        self.addWriter(name,pts)

