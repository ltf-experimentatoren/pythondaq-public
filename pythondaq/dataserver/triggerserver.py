#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (C) 2022 Daniel Jaeger

This file is part of PythonDAQ.

PythonDAQ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PythonDAQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PythonDAQ.  If not, see <http://www.gnu.org/licenses/>.

Remote measurement trigger system

@author: Daniel Jaeger
@date: 2020
"""

import socket
import select
import threading

BUFFER_SIZE = 1024 # Advisable to keep it as an exponent of 2
TCP_PORT = 5000


class TriggerServer():
    """
    This class provides a simple server that allows you to trigger the measurement
    from any other computer in the network
    """
    def __init__(self, trigger_fuc, autostart = True):
        """
        Initialization method.
        Arguments:
            trigger_func is the function to be executed when a trigger signal is received
            autostart determines if the server should bes started automatically.
        """
        # List to keep track of socket descriptors
        self.CONNECTION_LIST = []

        # Triggered function
        self.trigger_func = trigger_fuc

        # Start the server if desired
        if autostart:
            self.start()


    def start(self):
        """
        This method starts the server
        """
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        #self.server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        # TODO: Try if '' works here Attention, this binds all adapters which is ok for us
        self.server_socket.bind(("0.0.0.0", TCP_PORT))
        self.server_socket.listen(10)

        # Add server socket to the list of readable connections
        self.CONNECTION_LIST.append(self.server_socket)

        # Create some exchange variables
        self._shutdown = False

        # Start the server thread
        self.server_thread = threading.Thread(target=self.run)
        self.server_thread.start()


    def run(self):
        """
        This method is executed when the server is running. It contains the main loop
        """
        print("Trigger server started on port " + str(TCP_PORT))

        while True:
            # Get the list sockets which are ready to be read through select
            # Timeout ensures that the shutdown procedure works
            read_sockets,write_sockets,error_sockets = select.select(self.CONNECTION_LIST,[],[],1)

            for sock in read_sockets:
                #New connection
                if sock == self.server_socket:
                    # Handle the case in which there is a new connection recieved through server_socket
                    sockfd, addr = self.server_socket.accept()
                    self.CONNECTION_LIST.append(sockfd)
                    print("TS: Client (%s, %s) connected" % addr)

                #Some incoming message from a client
                else:
                    # Data recieved from client, process it
                    try:
                        data = sock.recv(BUFFER_SIZE)
                        if data == b"":
                            # Receiving an empty string means that the client
                            # has closed the connection
                            # Close the connection also at server side
                            print("TS: Client (%s, %s) is offline" % addr)
                            sock.close()
                            self.CONNECTION_LIST.remove(sock)
                        elif data:
                            # Meaningful data received
                            data = data.decode()
                            if "trigger" in data:
                                print("TS: Trigger received from client %s %s" % addr)
                                # Check if there is a file name provided
                                if len(data)>7:
                                    # Trigger the measurement with additional filename
                                    self.trigger_func(data.split()[-1])
                                else:
                                    # Trigger the measurement without additional filename
                                    self.trigger_func()
                    except:
                        #In Windows, sometimes when a TCP program closes abruptly,
                        # a "Connection reset by peer" exception will be thrown
                        # Handle this case here
                        print("TS: Client (%s, %s) is offline" % addr)
                        sock.close()
                        self.CONNECTION_LIST.remove(sock)
                        continue

            # Shutdown mechanism
            if self._shutdown:
                break


    def stop(self):
        """
        Shut down the server
        """
        # Set the shutdown flag
        self._shutdown = True
        # Wait for the server to shut down
        self.server_thread.join()
        # Close the network connections
        for con in self.CONNECTION_LIST:
            con.close()
        self.server_socket.close()
        print("Trigger server stopped")



def printFunction():
    """
    This function is used to test the trigger server
    """
    print("Hello from the print function!")
    print("Trigger signal received")



if __name__ == "__main__":
    """
    Main function which is executed when the file is executed directly
    """
    tr_server = TriggerServer(printFunction)