#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (C) 2022 Daniel Jaeger

This file is part of PythonDAQ.

PythonDAQ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PythonDAQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PythonDAQ.  If not, see <http://www.gnu.org/licenses/>.

Functions to correctly set units in the OPC UA server

@author: Daniel Jaeger
@date: 2022
"""

import csv
from opcua import ua


class UaUnitDescriptor():
    def __init__(self):
        """
        Initialization method
        Parse the CSV file specifyinf the conversion
        """
        self.dict = {}
        self.dict['UNECECode'] = []
        self.dict['UnitId'] = []
        self.dict['DisplayName'] = []
        self.dict['Description'] = []

        self.idx = -1 # The currently selected index

        with open('dataserver/UNECE_to_OPCUA.csv', 'r') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                keys = list(row.keys())
                self.dict['UNECECode'].append(row[keys[0]])
                self.dict['UnitId'].append(int(row['UnitId']))
                self.dict['DisplayName'].append(row['DisplayName'])
                self.dict['Description'].append(row['Description'])

        self.num_entries = len(self.dict['UNECECode'])


    def search_unece(self, unece_code):
        """
        Search the corresponding entry from a given UNECE-Code
        """
        i = 0
        while i < self.num_entries:
            if self.dict['UNECECode'][i] == unece_code:
                self.idx = i
                break
            else:
                i += 1
        return i


    def search_displayname(self, display_name):
        """
        Search the corresponding entry from a given display name
        """
        i = 0
        while i < self.num_entries:
            if self.dict['DisplayName'][i] == display_name:
                self.idx = i
                break
            else:
                i += 1

        # If we reach the end without finding an entry, we set the index to -1
        if i == self.num_entries:
            self.idx = -1

        return i


    def get_unece(self):
        """
        Get the UNECE-Code
        Make sure that the index is set correctly using one of the
        search functions before making call to this one.
        """
        if self.idx == -1:
            return "-"
        else:
            return self.dict['UNECECode'][self.idx]


    def get_unitid(self):
        """
        Get the OPC UA unit id.
        Make sure that the index is set correctly using one of the
        search functions before making call to this one.
        """
        if self.idx == -1:
            return 0
        else:
            return self.dict['UnitId'][self.idx]


    def get_displayname(self):
        """
        Get the OPC UA display name.
        Make sure that the index is set correctly using one of the
        search functions before making call to this one.
        """        
        if self.idx == -1:
            return "-"
        else:
            return self.dict['DisplayName'][self.idx]


    def get_description(self):
        """
        Get the OPC UA unit id.
        Make sure that the index is set correctly using one of the
        search functions before making call to this one.
        """
        if self.idx == -1:
            return "-"
        else:
            return self.dict['Description'][self.idx]


    def get_opcua_euinformation(self):
        """
        Return the complete OPC UA EUInformation structure
        """
        ua.LocalizedText()
        
        eu_info = ua.EUInformation()
        eu_info.UnitId = self.get_unitid()
        eu_info.DisplayName = ua.LocalizedText(self.get_displayname())
        eu_info.Description = ua.LocalizedText(self.get_description())
        return eu_info
