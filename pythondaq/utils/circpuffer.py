#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (C) 2022 Daniel Jaeger

This file is part of PythonDAQ.

PythonDAQ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PythonDAQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PythonDAQ.  If not, see <http://www.gnu.org/licenses/>.

Classes for circular puffers (ringpuffers)

@author: Daniel Jaeger
@date: 2020-2022
"""

import numpy as np

class CircularPuffer():
    """
    Circular puffer class.
    The row index rotates while the column index remains constant.
    # BUG: This version is super slow, fix it before using
    """
    def __init__(self,num_rows, num_cols = 1, dtype = float):
        """
        Initialize a circular puffer with the given number of rows and columns
        """
        self.num_rows = num_rows
        self.num_cols = num_cols
        self.dtype = dtype
        self.idx_row = num_rows-1 # This is the index of the last edited row
        self.memory = np.zeros((num_rows,num_cols), dtype)


    def get_row(self, rel_index):
        """
        Set a single row of the data set.
        rel_index ... the relative index and works the following way:
            rel_index = 0: Return the newest element
            rel_iddex = 1: Return the second newest element
            rel_index = num_rows -1: Return the oldest element
        """
        # Determine the index from which the row is picked
        pick_index = (self.idx_row - rel_index) % self.num_rows
        return self.memory[pick_index,:]


    def get_multiple_rows(self, num_to_get):
        """
        Get a copy of the data in the buffer. The data is sorted:
            num_to_get = 1: Return the newest element (new index 0)
            num_to_get = 2: Return the newest and second newest element (new index 1)
            ...and so on.
        """
        # Initialize a new numpy array
        return_data = np.empty((num_to_get, self.num_cols), self.dtype)

        # Sort in all required data
        for i in range(num_to_get):
            return_data[i] = self.get_row(i)

        return return_data


    def append(self, new_row):
        """
        Add a new row to the ringpuffer and forward the index
        The new row has to be a numpy array of dimension (1,num_cols) or (num_cols,)
        """
        # Forward the index to point to the oldest element
        self._forward_index()
        # Assisgn the new_row to the memory array
        self.memory[self.idx_row] = new_row


    def append_multiple(self, new_data):
        """
        Append multiple rows to the puffer
        The new data have to have the dimension (#new_rows, num_cols)
        """
        num_new_rows = new_data.shape[0]
        for i in range(num_new_rows):
            self.append(new_data[i])


    def _forward_index(self):
        """
        Forward the index to point to the oldest element in the puffer
        """
        self.idx_row = self.idx_row + 1

        if(self.idx_row >= self.num_rows):
            self.idx_row = 0


    def _backward_index(self):
        """
        Switch the index to point to the second newest element in the puffer
        """
        self.idx_row = self.idx_row - 1

        if(self.idx_row < 0):
            self.idx_row = self.num_rows - 1



class CircularPufferMarked(CircularPuffer):
    """
    Circular puffer class with an additional index pointer.
    The additional pointer can be used to mark already processed data.
    # BUG: This version is super slow, fix it before using
    """
    def __init__(self,num_rows, num_cols = 1, dtype = float):
        """
        Initialize a circular puffer with the given number of rows and columns
        """
        # Initialize the parent's class variables
        super().__init__(num_rows, num_cols, dtype)
        # Initialize the additional index pointer variable
        self.idx_marked = 0


    def get_marker(self):
        """
        Returns the actual marker position
        """
        return self.idx_marked


    def get_marker_offset(self):
        """
        Returns the number of rows between the marker and the newest data point.
        If the newest data point is newer than the marker position, the offset is >0
        """
        offset = (self.idx_row - self.idx_marked) & self.num_rows
        return offset


    def set_marker(self):
        """
        Set the marker to the last element appended to the puffer
        """
        self.idx_marked = self.idx_row


    def get_row_at_marker(self, offset = 0):
        """
        Return the data of the row at which the marker currently points
        offset ... the offset from the marker and works the following way:
            offset = 0: Return the row at which the marker currently points
            offset = 1: Return the second newest element measured from the marker
            offset = -1: Return row appended after marker was set
        Attention: It is possible to return data
        newer than the marker if offset is big enough!
        """
        pick_index = (self.idx_marked - offset) % self.num_rows
        return self.memory[pick_index,:]


    def get_rows_since_marker(self, original_order = False):
        """
        Return all data which are newer than the position of the marker.
        The element at which the marker points is not inclueded.
        This method is not safe against puffer overrun!

        Data sorting if original_order = False:
            index 0 is the newest data point
            index 1 is the second newest data point
            last index is the data point once newer than the marker

        Data sorting if original_order = True
            index_0 is the data point once newer than the marker
            semi-last index is the second newest data point
            last index is the newest data point
        """
        # Determine the number of elements to return
        num_to_get = (self.idx_row - self.idx_marked) % self.num_rows

        # Initialize a new numpy array
        return_data = np.zeros((num_to_get, self.num_cols), self.dtype)

        # Sort in the return data to the array
        for i in range(num_to_get):
            if original_order:
                return_data[num_to_get-1-i] = self.get_row_at_marker(i-num_to_get)
            else:
                return_data[i] = self.get_row_at_marker(i-num_to_get)

        return return_data


    def forward_marker(self, steps = 1):
        """
        Forward the "marked" pointer by the given number of steps.
        steps > 0 means forwarding towards newer data values
        """
        # Do not allow back_stepping in this function
        if (steps < 0):
            return
            # TODO: Maybe we should raise an exception at this point

        # Consider running over the zero, perhaps several times
        self.idx_marked = (self.idx_marked + steps) % self.num_rows



class CircularPufferList():
    """
    Circular puffer class.
    The row index rotates while the column index remains constant.
    This class has an implementation using python lists.
    It is well faster than the numpy version
    """
    def __init__(self,num_rows, num_cols = 1):
        """
        Initialize a circular puffer with the given number of rows and columns
        """
        self.num_rows = num_rows
        self.num_cols = num_cols
        self.idx_row = num_rows-1 # This is the index of the last edited row
        self.memory = [[0 for j in range(num_cols)] for i in range(num_rows)]


    def get_row(self, rel_index):
        """
        Set a single row of the data set.
        rel_index ... the relative index and works the following way:
            rel_index = 0: Return the newest element
            rel_iddex = 1: Return the second newest element
            rel_index = num_rows -1: Return the oldest element
        """
        # Determine the index from which the row is picked
        pick_index = (self.idx_row - rel_index) % self.num_rows
        return self.memory[pick_index]


    def get_multiple_rows(self, num_to_get):
        """
        Get a copy of the data in the buffer. The data is sorted:
            num_to_get = 1: Return the newest element (new index 0)
            num_to_get = 2: Return the newest and second newest element (new index 1)
            ...and so on.
        """
        # Initialize a new numpy array
        return_data = []

        # Sort in all required data
        for i in range(num_to_get):
            return_data.append(self.get_row(i))

        return return_data


    def append(self, new_row):
        """
        Add a new row to the ringpuffer and forward the index
        The new row has to be a python list of dimension num_cols
        """
        # Forward the index to point to the oldest element
        self._forward_index()
        # Assisgn the new_row to the memory array
        self.memory[self.idx_row] = new_row


    def _forward_index(self):
        """
        Forward the index to point to the oldest element in the puffer
        """
        self.idx_row = self.idx_row + 1

        if(self.idx_row >= self.num_rows):
            self.idx_row = 0


    def _backward_index(self):
        """
        Switch the index to point to the second newest element in the puffer
        """
        self.idx_row = self.idx_row - 1

        if(self.idx_row < 0):
            self.idx_row = self.num_rows - 1
