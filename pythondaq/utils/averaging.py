#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (C) 2022 Lukas Selmayr

This file is part of PythonDAQ.

PythonDAQ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PythonDAQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PythonDAQ.  If not, see <http://www.gnu.org/licenses/>.

This file contains all average related functions that are used more than once in
computation classes of the PythonDAQ.

@author: Lukas Selmayr
@date: 2021-2022
"""


def getUniformAverage(sensors_level):
    """
    This method gets a uniform average over the number of values.
    """
    tot_val = 0
    count = 0

    for sens in sensors_level:
        for chan in sensors_level[sens]:
            tot_val += sensors_level[sens][chan]
            count += 1

    val = tot_val/count
    return val


def getCorrAverage(sensors, radii, rake_radi):
    """
    This method averages the values on a rake. It gets the values, the radial position and
    the inner and outer radii of the compressor.
    If there are no radii, it gets the uniform average. If there are no rake_radi set, it also
    gets the uniform average.
    """
    val = 0
    diff = {}
    # set the inner wall of the compressor as first radius
    rad = {"rN": min(rake_radi)}
    sorted_rad = {}
    sorted_keys = sorted(radii, key=radii.get)
    for w in sorted_keys:
        sorted_rad[w] = radii[w]

    # get only the radii that correspond to the sensor values
    for sens in sensors:
        for chan in sensors[sens]:
            for k in sorted_rad:
                if (k + "_corr") in chan:
                    rad[k] = sorted_rad[k]
                if k in chan:
                    rad[k] = sorted_rad[k]

    # no radii are given, so the uniform average is used
    if rad == {"rN": min(rake_radi)}:
        return getUniformAverage(sensors)

    elif rake_radi != [1, 0] or all(value <= 1 for value in rad.values()):
        # set the outer wall of the compressor as last radius
        rad["rG"] = max(rake_radi)
        rad_list = list(rad)

        for k in rad_list[1:]:
            if k == "rG":
                break

            # get the area of the value closest to the inner compressor radius
            if rad_list[rad_list.index(k)-1] == "rN":
                diff[k] = (((rad[rad_list[rad_list.index(k)+1]]+rad[k])/2)**2-((rad[rad_list[rad_list.index(k)-1]])**2))

            # get the area of the value closest to the outer compressor radius
            elif rad_list[rad_list.index(k)+1] == "rG":
                diff[k] = (((rad[rad_list[rad_list.index(k)+1]])**2)-((rad[k]+rad[rad_list[rad_list.index(k)-1]])/2)**2)

            # get all other areas
            else:
                diff[k] = (((rad[rad_list[rad_list.index(k)+1]]+rad[k])/2)**2-((rad[k]+rad[rad_list[rad_list.index(k)-1]])/2)**2)

        # calculate the average with the areas
        for sens in sensors:
           for chan in sensors[sens]:
               val += sensors[sens][chan]*abs(diff[chan.replace("_corr","")])/(max(rake_radi)**2-min(rake_radi)**2)

    # the compressor radii are not set, so the uniform average is build
    else:
        return getUniformAverage(sensors)

    return val


def getCircAverage(sensors, circ_radii):
    """
    This method averages values over a circle. It gets the to be averaged values and their position in degree.
    If the positions are emtpy, it just gets the average over the number of values.
    With the positions it calculates the area of the circle that the value covers.
    """
    val = 0
    diff = {}
    sorted_rad = {}
    sorted_keys = sorted(circ_radii, key=circ_radii.get)
    for w in sorted_keys:
        sorted_rad[w] = circ_radii[w]
    sorted_rad_list = list(sorted_rad)

    if sorted_rad == {} or len(list(set(list(sorted_rad.values())))) == 1:
        count = 0

        # get the average over the number of values
        for sens in sensors:
            val += sensors[sens]
            count += 1

        return val/count

    for k in sorted_rad:
        # get the area of the first value of the circle
        if diff =={}:
            diff[k] = 360 - ((sorted_rad[sorted_rad_list[-1]]+sorted_rad[k]+360)/2 \
                    - (sorted_rad[k] + sorted_rad[sorted_rad_list[sorted_rad_list.index(k)+1]])/2)

        # get the area of the last value of the circle
        elif k == sorted_rad_list[-1]:
            diff[k] = (sorted_rad[sorted_rad_list[0]]+360+sorted_rad[k])/2 \
                    - (sorted_rad[k] + sorted_rad[sorted_rad_list[sorted_rad_list.index(k)-1]])/2

        # get all other areas
        else:
            diff[k] = (sorted_rad[sorted_rad_list[sorted_rad_list.index(k)+1]]+sorted_rad[k])/2 \
                    - (sorted_rad[sorted_rad_list[sorted_rad_list.index(k)-1]]+sorted_rad[k])/2

    # average the values over the areas
    for sens in sensors:
        val += sensors[sens]*diff[sens]/360

    return val


def getLinInterpolation(val, pos, x):
    """
    This method performs a linear interpolation of two values
    The way to insert the position values has to be:
        First the value on the right of x
        Second the value on the left of x
    """
    d = []

    for k in val:
        for chan in val[k]:
            if (chan + "_corr") in pos[k]:
                d.append([pos[k][chan + "_corr"], val[k][chan]])

    delta_theta_x = x - d[0][0]
    delta_theta = d[1][0] - d[0][0]

    # When the positions on the circle are crossing the zero degree line
    if delta_theta_x < 0:
       delta_theta_x += 360

    if delta_theta < 0:
       delta_theta += 360

    output = d[0][1] + delta_theta_x * ((d[1][1] - d[0][1])/delta_theta)

    if len(d) == 4:
        delta_theta_x_1 = x - d[2][0]
        delta_theta_1 = d[3][0] - d[2][0]

        if delta_theta_x_1 < 0:
            delta_theta_x_1 += 360

        if delta_theta_1 < 0:
            delta_theta_1 += 360

        output += d[2][1] + delta_theta_x_1 * ((d[3][1] - d[2][1])/delta_theta_1)
        output /= 2

    return output


def getPresOnRad(pres, pres_rad, circ_rad, rad):
    """
    This method returns all values that are on one radius,
    as well as their position on the circle in degree.
    """
    pres_output = {}
    circ_output = {}

    for k in pres:
        pres_output[k] = {}
        circ_output[k] = {}

    for corrSens in pres:
        for k in pres[corrSens]:
            if pres_rad[k.replace("_corr","")] == rad and k in pres[corrSens]:
                circ_output[corrSens][k] = circ_rad[corrSens][k.replace("_corr", "")]
                pres_output[corrSens][k.replace("_corr", "")] = pres[corrSens][k]

    return [pres_output, circ_output]
