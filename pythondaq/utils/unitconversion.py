#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (C) 2022 Daniel Jaeger

This file is part of PythonDAQ.

PythonDAQ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PythonDAQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PythonDAQ.  If not, see <http://www.gnu.org/licenses/>.

@author: Daniel Jaeger
@date: 2020
"""

def psi_to_pascal(data):
    """
    Convert pressure values in psi to pascals.
    The input data can be a single variable or a list.
    """
    
    if type(data) is list:
        data = [6894.76 * i for i in data]
    else:
        data = 6894.76 * data
    return data


def pascal_to_psi(data):
    """
    Convert pressure values in pascals to psi.
    The input data can be a single variable or a list.
    """
    if type(data) is list:
        data = [0.000145038 * i for i in data]
    else:
        data = 0.000145038 * data
    return data