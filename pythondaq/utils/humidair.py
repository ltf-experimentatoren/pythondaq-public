#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (C) 2022 Daniel Jaeger

This file is part of PythonDAQ.

PythonDAQ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PythonDAQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PythonDAQ.  If not, see <http://www.gnu.org/licenses/>.

This module contains an implementation for the calculation
of fluid parameters for non-condensing humid air.

The calculation is based on the description given in:
https://www.tu-chemnitz.de/mb/TechnThDyn/hxDokumentation.pdf

@author: Daniel Jaeger
@date: 2022
"""

import math


class HumidAir():
    """
    Class to handle the fluid parameters of humid air.
    Calculation is limited to non-condensig humidity!
    """
    def __init__(self):
        """
        Initialization method
        """
        # Thermodynamic properties (initialized to ISA_0)
        self.p = 101325.0
        self.T = 288.15

        # Humidiy parameters
        self.x = 0.0        # Absoulte humidity in kg water / kg dry air
        self.phi = 0.0      # Relative humidity

        # Specific gas constants
        self.R_A = 287.08   # Gas constant of dry air in J/(kg K)
        self.R_V = 461.53   # Gas constatnt of water vapor in J/(kg K)

        # Calculate the rest of the properties
        self.recalculate()


    def setRelHumidity(self, phi):
        """
        Set the relative humidity and recalculate the fluid parameters.
        phi = 0...1
        """
        # Check if phi is within limits
        if (phi < 0.0) or (phi > 1.0):
            raise ValueError("Relative humidity is limited to 0...1")
        self.phi = phi

        # Recalculate parameters
        self.recalculate()


    def getSatVapPres(self):
        """
        Get the saturated vapor pressure based on the current temperature
        """
        # Source for the formulae:
        # Glück, Bernd: Zustands- und Stoffwerte,
        # VEB Verlag für Bauwesen, Berlin, 2. Auflage, 1991

        # Get the temperature in degrees celcius
        T_degC = self.T - 273.15

        # Get the saturated vapor pressure depending on the temperature
        if ((T_degC >= -20.0) and (T_degC < 0.01)):
            p_VS = 611.0 * math.exp(-4.909965e-4 + 0.08183197*T_degC - 5.552967e-4 * T_degC**2 - \
                2.228376e-5 * T_degC**3 - 6.211808e-7 * T_degC**4)
        elif ((T_degC >= 0.01) and (T_degC < 100.0)):
            p_VS = 611.0 * math.exp(-1.91275e-4 + 7.258e-2 * T_degC - 2.939e-4 * T_degC**2 + \
                9.841e-7 * T_degC**3 - 1.92e-9 * T_degC**4)
        elif ((T_degC >= 100.0) and (T_degC < 200.0)):
            p_VS = 611.0 * math.exp(6e-5 + 7.13274e-2 * T_degC - 2.581631e-4 * T_degC**2 + \
                6.311955e-7 * T_degC**3 - 7.167112e-10 * T_degC**4)
        else:
            raise ValueError("Temperature out of range -20 ... 200°C")

        return p_VS


    def getDensity(self):
        """
        Get the density of the humid air in units:
        kg of humid air/m^3
        """
        rho = (1+self.x)/(self.x+0.622) * self.p/(self.R_V*self.T)
        return rho


    def getHeatCapacity(self):
        """
        Get the specific heat capacity depending on the current fluid parameters
        in units: J/(kg of humid air * K)
        """
        # Get the temperature in degrees celcius
        #T_degC = self.T + 273.15

        # Table values for the heat capacity
        # Source:
        # Häußler, W.: Lufttechnische Berechnungen im Mollier-i,x-Diagramm,
        # Verlag Theodor Steinkopff, Dresden 1969
        #t_table = [-20.0, 0.0, 20.0, 40.0, 60.0, 80.0, \
        #    100.0, 120.0, 140.0, 160.0, 180.0, 200.0]
        #cp_air_table = [1005.5, 1005.6, 1005.8, 1006.2, 1006.5, 1007.1, \
        #    1007.7, 1008.5, 1009.4, 1010.3, 1011.4, 1012.6]
        #cp_vap_table = [1857.0, 1858.0, 1860.0, 1863.0, 1865.0, 1868.0, \
        #    1872.0, 1875.0, 1879.0, 1883.0, 1887.0, 1892.0]

        # TODO Interpolate table values
        # TODO Remove dummies for 20°C
        cp_air = 1005.8
        cp_vap = 1860.0

        # Calulate the heat capacity of the humid air
        cp = (cp_air + self.x * cp_vap) / (1+self.x)
        return cp


    def recalculate(self):
        """
        Recalculate fluid properties
        """
        # Get the absolute humidity
        p_VS = self.getSatVapPres()
        p_V = self.phi * p_VS
        self.x = self.R_A / self.R_V * p_V / (self.p - p_V)

        # Recalculate thermodynamic properties
        self.rho = self.getDensity()
        self.cp = self.getHeatCapacity()
        self.R = (self.R_A + self.x * self.R_V) / (1+self.x)
        self.cv = self.cp - self.R
        self.kappa = self.cp/self.cv
