#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (C) 2022 Daniel Jaeger

This file is part of PythonDAQ.

PythonDAQ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PythonDAQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PythonDAQ.  If not, see <http://www.gnu.org/licenses/>.

This module contains classes for storing information
and calculations regarding the test environment.

@author: Daniel Jaeger
@date: 2022
"""

class StandardEnvironment():
    """
    This class provides storage for some information
    regarding the test environment.
    """
    def __init__(self, g=9.80665):
        """
        Initialization method
        """
        # Test location
        self.g = g # The local gravitational constant in m/s2

        # International standard atmosphere
        self.T_ISA = 288.15
        self.p_ISA = 101325.0
        self.kappa_ISA = 1.4
        self.R_ISA = 287.05
