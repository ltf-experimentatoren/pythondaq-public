#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (C) 2022 Daniel Jaeger

This file is part of PythonDAQ.

PythonDAQ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PythonDAQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PythonDAQ.  If not, see <http://www.gnu.org/licenses/>.

This module launches the VisualizationClient of the PythonDAQ package
It should be called from outside using the command
python3 visualizationclient.py

@author: Daniel Jaeger
@date: 2020
"""

import sys
import os

from PyQt5.QtWidgets import QApplication
from PyQt5 import QtGui

import gui

if __name__ == "__main__":
    # Create an instance of QApplication
    app = QApplication(sys.argv)

    # Set the application's icon
    filepath = os.path.dirname(os.path.abspath(__file__))
    filepath = os.path.join(filepath,"gui/icons/pydaq_logo.png")
    icon = QtGui.QIcon()
    icon.addPixmap(QtGui.QPixmap(filepath))
    app.setWindowIcon(icon)

    # Set the application's display name
    app.setApplicationDisplayName("Python DAQ")

    # Create an instance of your application's GUI
    window = gui.VCGUI()

    # Show your application's GUI
    window.show()

    # Run your application's event loop (or main loop)
    sys.exit(app.exec())
