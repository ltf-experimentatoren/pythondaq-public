#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (C) 2022 Daniel Jaeger

This file is part of PythonDAQ.

PythonDAQ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PythonDAQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PythonDAQ.  If not, see <http://www.gnu.org/licenses/>.

Module to implement the graphical user interface (GUI) of VisualizationClient.
This module is part of PythonDAQ

@author: Daniel Jaeger
@date: 2020-2022
"""

# Qt imports
from PyQt5 import QtWidgets
from PyQt5.QtCore import QTimer
from PyQt5.QtCore import Qt, pyqtSlot

# Imprt of opcuaua-widgets
from uawidgets import resources    # Necessary to show icons
from uawidgets.tree_widget import TreeWidget

# Import of own mudules
import core
from .livechannelwidget import LiveChannelWidget
from .attributeswidget import AttributesWidget
from .graphwidget import GraphWidget, XYGraphWidget
from .viewdialogs import AddViewDialog
from .aboutdialog import AboutDialog


# Attention: The code does not run well if called from spyder
# Call from outside insead


class VCGUI(QtWidgets.QMainWindow):
    """
    Main window clas for measurement data visualization
    """
    def __init__(self):
        """
        This function is called when the object is created.
        It creates the GUI and initializes all necessary variables
        An instance of the VisualizationClient Core is created for
        running the backend.
        """
        # Call the initialization procedure of the parent class (QMainWindow)
        super().__init__()

        # Create the main window widget
        self.setWindowTitle('PythonDAQ VisualizationClient')

        # Rescale it to use whole available screen
        self.desktop = QtWidgets.QDesktopWidget()
        coords = self.desktop.availableGeometry().getCoords()
        self.setGeometry(coords[0],coords[1],coords[2],coords[3])

        # Create an instance of the VCCore
        self.core_client = core.VCCore()

        # Initialize some storage variables
        self.connected = False        # Connection to the server not established
        self.update_interval = 500    # The interval to update data
        self.channels = []            # Array to store the channel widgets

        # Build the GUI
        self._createBody()
        self._createMenu()
        self._createStatusBar()

        # Setup the tree view mechanisms
        self.setup_tree_view()


    def _createBody(self):
        """
        Build the GUI of the Visualization Client.
        This function contains all the geometric code
        """
        # Create the side frame and fill it with content
        side_frame = QtWidgets.QWidget()
        side_frame.setMinimumWidth(150)

        # Initialize the layout manager for the side frame
        side_layout = QtWidgets.QVBoxLayout()

        # Create a field to enter the server url and its label
        url_label = QtWidgets.QLabel("Server URL")
        default_url = "opc.tcp://127.0.0.1:4840"
        self.url_widget = QtWidgets.QLineEdit(default_url)

        # Create the connect button and assign its callback
        self.connect_button = QtWidgets.QPushButton("Connect")
        self.connect_button.clicked.connect(self.atConnectButton)

        # Tree to display available data structure
        self.tree_view = QtWidgets.QTreeView()
        #self.tree_view.setHeaderHidden(True)
        #self.tree_view.setModel(self.treeModel)    
        self.tree_view.setDragEnabled(True)
        self.tree_view.setDragDropMode(QtWidgets.QAbstractItemView.DragOnly)

        # Add the widgets to the layout manager
        side_layout.addWidget(url_label)
        side_layout.addWidget(self.url_widget)
        side_layout.addWidget(self.connect_button)
        side_layout.addWidget(self.tree_view)

        # Set the layout as actual layout manager
        side_frame.setLayout(side_layout)

        # Create the central frame and fill it with content
        livechannel_dock = QtWidgets.QDockWidget()
        livechannel_dock.setAllowedAreas(Qt.RightDockWidgetArea)
        livechannel_dock.setWindowTitle("Channels")

        livechannel_widget = LiveChannelWidget()
        livechannel_dock.setWidget(livechannel_widget)
        self.addDockWidget(Qt.DockWidgetArea(2), livechannel_dock)
        
        # Create the graph view in a dock widget
        graph_dock = QtWidgets.QDockWidget()
        graph_dock.setAllowedAreas(Qt.RightDockWidgetArea)
        graph_dock.setWindowTitle("Graph")

        graph_view = GraphWidget()
        graph_dock.setWidget(graph_view)
        self.addDockWidget(Qt.DockWidgetArea(2), graph_dock)
        self.tabifyDockWidget(livechannel_dock,graph_dock)

        # Create the attributes view in a dock widget
        attr_dock = QtWidgets.QDockWidget()
        attr_dock.setAllowedAreas(Qt.RightDockWidgetArea)
        attr_dock.setWindowTitle("Attributes")

        attr_view = AttributesWidget()
        attr_dock.setWidget(attr_view)
        self.addDockWidget(Qt.DockWidgetArea(2), attr_dock)
        self.tabifyDockWidget(livechannel_dock,attr_dock)

        # Resize the first dock to appropriate width
        self.resizeDocks([livechannel_dock], [0.7 * self.geometry().width()], Qt.Horizontal);

        # Store the widgets and docks in a list for later access
        self.view_list = [livechannel_widget, graph_view, attr_view]
        self.dock_list = [livechannel_dock, graph_dock, attr_dock]

        # Put the livechannel dock to the top
        livechannel_dock.raise_()

        # Create the splitter and add the two frames
        splitter = QtWidgets.QSplitter(Qt.Horizontal)
        splitter.addWidget(side_frame)
        #splitter.addWidget(central_frame)
        self.setCentralWidget(splitter)

        # TODO Add a scrollbar in case the window gets too small


    def _createMenu(self):
        """
        Create the menu and fill it with content
        """
        # Create the menu bar and fill it with actions
        # View menu
        view_menu = self.menuBar().addMenu("View")
        show_graph_action = QtWidgets.QAction("Show Graph", self)
        show_attr_action = QtWidgets.QAction("Show Attributes",self)
        add_graph_action = QtWidgets.QAction("Add View...",self)
        add_graph_action.triggered.connect(self._add_view)
        view_menu.addAction(show_graph_action)
        view_menu.addAction(show_attr_action)
        view_menu.addAction(add_graph_action)

        # Help menu
        help_menu = self.menuBar().addMenu("Help")
        about_action = QtWidgets.QAction("&About", self)
        vc_help_action = QtWidgets.QAction("VisualizationClient documentation", self)
        pydaq_help_action = QtWidgets.QAction("PythonDAQ documentation", self)
        help_menu.addAction(about_action)
        help_menu.addAction(vc_help_action)
        help_menu.addAction(pydaq_help_action)
        about_action.triggered.connect(self._about)


    def _createStatusBar(self):
        """
        Create a status bar and fill it with content
        """
        self.statusbar = self.statusBar()
        self.statusbar.showMessage("Disconnected")


    def atConnectButton(self):
        """
        This function is called every time the Connect-Button is clicked.
        It triggers the connection/disconnection and modifies the GUI

        """
        if self.connected:
            # Stop the measurement loop
            self.timer.stop()
            
            # Clear the tree and the display widgets
            for view in self.view_list:
                view.clear()
            self.tree_model.clear()

            # Do the disconnection procedure
            self.core_client.disconnect()
            self.connected = False
            self.url_widget.setEnabled(True)
            self.connect_button.setText("Connect")
            self.statusbar.showMessage("Disconnected")

        else:
            # Conect to the server
            url = self.url_widget.text()
            # Only proceed if the connection is successful
            try:
                self.core_client.connect(url)
                self.url_widget.setEnabled(False)
                self.connected = True
                self.connect_button.setText("Disconnect")
                self.statusbar.showMessage(f"Connected to server: {url}")
            except Exception:
                self.statusbar.showMessage(f"Unable to connect to server: {url}", 5000)
                return

            # Initialize the tree model and view
            self.tree_model.set_root_node(self.core_client.get_root_node())

            # Start the measurement loop
            # TODO: Remove this to a later point
            self.timer = QTimer(self)
            self.timer.timeout.connect(self.update_numbers)
            self.timer.start(self.update_interval)


    def closeEvent(self, event):
        """
        This function is called before the window is closed.
        It disconnects from the OPC server before shutting down.
        """
        if self.connected:
            self.core_client.disconnect()


    def update_numbers(self):
        """
        Update the numbers in all the views
        """
        for wid in self.view_list:
            wid.update_data()


    def setup_tree_view(self):
        """
        Setup the tree view data structure and connect it to the view,
        as well as all necessary functions.
        """
        # Setup the tree
        self.tree_model = TreeWidget(self.tree_view)

        # Setup the context menu
        self.tree_view.setContextMenuPolicy(Qt.CustomContextMenu)
        self.tree_view.customContextMenuRequested.connect(self._show_context_menu_tree)
        self.tree_context_menu = QtWidgets.QMenu()

        # Create the context actions and connect to functions
        # TODO: Move this to another point in the code
        add_to_attributes_action = QtWidgets.QAction("Show Attributes", self)
        add_to_livechannel_action = QtWidgets.QAction("Add to Livechannel View", self)
        add_to_graph_action = QtWidgets.QAction("Add to Graph", self)

        add_to_attributes_action.setData(2)
        add_to_livechannel_action.setData(0)
        add_to_graph_action.setData(1)

        add_to_attributes_action.triggered.connect(self._add_channel_to_view)
        add_to_livechannel_action.triggered.connect(self._add_channel_to_view)
        add_to_graph_action.triggered.connect(self._add_channel_to_view)

        # Add the actions to the context menu
        self.tree_context_menu.addAction(add_to_attributes_action)
        self.tree_context_menu.addAction(add_to_livechannel_action)
        self.tree_context_menu.addAction(add_to_graph_action)

        self.tree_context_menu.addSeparator()


    def _show_context_menu_tree(self, position):
        """
        This funtion is called, when there is a right click on an item in the tree.
        Do not use this function by yourself!
        """
        # Get the currently selected node
        current_node = self.tree_model.get_current_node()
        
        # Show the context menu for the selected node
        if current_node:
            self.tree_context_menu.exec_(self.tree_view.viewport().mapToGlobal(position))


    @pyqtSlot()
    def _add_channel_to_view(self):
        """
        Called when we add a channel to a view.
        Do not use this function by yourself!
        """
        # Get the currently selected node
        current_node = self.tree_model.get_current_node()

        # Get the desired view index
        view_idx = self.sender().data()

        # Add the node to the livechannel_widget
        if current_node:
            self.view_list[view_idx].add_channel_from_node(current_node)


    def _add_view(self):
        """
        Method to add a graph to the visualization
        """
        # Get the view index (for naming and handling the actions)
        view_idx =len(self.view_list)

        # Launch a dialog to find out the desired view
        default_name = "View " + str(view_idx+1)
        dialog = AddViewDialog(default_name)
        ret_val = dialog.exec()
        view_name = dialog.getName()

        # Create a view according to the selection
        if ret_val == 1:
            new_view = LiveChannelWidget(view_name)
        elif ret_val == 2:
            new_view = GraphWidget(view_name)
        elif ret_val == 3:
            new_view = XYGraphWidget(view_name)
        else:
            return

        # Create a dock widget and place the view
        new_dock = QtWidgets.QDockWidget()
        new_dock.setAllowedAreas(Qt.RightDockWidgetArea)
        new_dock.setWindowTitle(view_name)
        new_dock.setWidget(new_view)
        self.addDockWidget(Qt.DockWidgetArea(2), new_dock)
        self.tabifyDockWidget(self.dock_list[0],new_dock)

        # Add the view and the dock to the lists
        self.view_list.append(new_view)
        self.dock_list.append(new_dock)

        # Add the new view to the context menu
        add_to_view_action = QtWidgets.QAction(f"Add to {view_name}", self)
        add_to_view_action.setData(view_idx)
        add_to_view_action.triggered.connect(self._add_channel_to_view)
        self.tree_context_menu.addAction(add_to_view_action)


    def _about(self):
        """
        Show the "About PythonDAQ VisualizationClient" Dialog
        """
        self.aboutWindow = AboutDialog()
        self.aboutWindow.show()
