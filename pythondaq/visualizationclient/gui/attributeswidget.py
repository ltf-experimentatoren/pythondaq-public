#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (C) 2022 Daniel Jaeger

This file is part of PythonDAQ.

PythonDAQ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PythonDAQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PythonDAQ.  If not, see <http://www.gnu.org/licenses/>.

This module contains a widget for showing the OPC UA attributes of an object.

@author: Daniel Jaeger
@date: 2020-08-29
"""

from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt

from uawidgets.attrs_widget import AttrsWidget


class AttributesWidget(QtWidgets.QWidget):
    def  __init__(self, name="Attributes"):
        """
        Initialization method
        """
        # Call the parent class initialization methd
        super().__init__()

        # Storage variables
        self.name = name            # The displayed name of the widget

        # Create the layout manager for the central frame
        layout = QtWidgets.QVBoxLayout()
        layout.setAlignment(Qt.AlignTop)

        # Draw a header label for the channel list
        header = QtWidgets.QLabel(f"<h2>{self.name}</h2>")

        # Create a tree view which hosts the attributs manager
        self.attrView = QtWidgets.QTreeView(self)
        self.attrView.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustToContents)
        self.attrView.setEditTriggers(QtWidgets.QAbstractItemView.AllEditTriggers)
        self.attrView.setProperty("showDropIndicator", False)
        self.attrView.setTextElideMode(Qt.ElideNone)
        self.attrView.setAutoExpandDelay(-1)
        self.attrView.setIndentation(18)
        self.attrView.setSortingEnabled(True)
        self.attrView.setWordWrap(True)

        # Create the attributes manager
        self.attr_manager = AttrsWidget(self.attrView)

        # Add the widgets to the layout manager
        layout.addWidget(header)
        layout.addWidget(self.attrView)

        # Set the layout as actual layout manager
        self.setLayout(layout)


    def add_channel_from_node(self,current_node):
        """
        Display the attributs
        """
        self.attr_manager.show_attrs(current_node)


    def update_data(self):
        """
        Update the live data
        """
        # Not necessary for this widget
        pass


    def clear (self):
        """
        Clear the attributes
        """
        self.attr_manager.clear()
