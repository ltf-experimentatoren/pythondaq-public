#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (C) 2022 Daniel Jaeger

This file is part of PythonDAQ.

PythonDAQ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PythonDAQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PythonDAQ.  If not, see <http://www.gnu.org/licenses/>.

This module contains widgets showing graphical data representation.

@author: Daniel Jaeger
@date: 2020-2022
"""

from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QWidget, QLabel, QPushButton
from PyQt5.QtWidgets import QHBoxLayout, QVBoxLayout, QGridLayout
from PyQt5.QtWidgets import QDialog
from PyQt5.QtWidgets import QStyle

from PyQt5.QtCore import Qt, QDateTime

from PyQt5.QtGui import QPainter

from PyQt5 import QtChart
from asyncua.sync import SyncNode



class BaseGraphWidget(QtWidgets.QWidget):
    def  __init__(self, name="Graph"):
        """
        Initialization method
        Do not forget to setup the user interface in inherited classes
        """
        # Call the parent class initialization methd
        super().__init__()

        # Storage variables
        self.name = name            # The displayed name of the widget
        self.series_length = 40     # The length of the time series
        self.has_xcontrols = False  # Create X-axis controls or not

        # Initialize a list of nodes and channels
        self.node_list = []
        self.channel_list = []


    def _setupUi(self):
        """
        Setup the user interface
        """
        # Create the layout manager for the central frame
        self.layout = QtWidgets.QVBoxLayout()
        self.layout.setAlignment(Qt.AlignTop)

        # Create the chart
        self.chart  = QtChart.QChart()
        self.chart.setTitle(self.name)

        # Create the axes
        self._create_axes()

        # Create the chart view
        chartview = QtChart.QChartView(self.chart)
        chartview.setRenderHint(QPainter.Antialiasing)

        # Add the widgets to the layout manager
        self.layout.addWidget(chartview)

        # Add the graph controls
        self._create_graph_controls()

        # Set the layout as actual layout manager
        self.setLayout(self.layout)


    def _create_axes(self):
        """
        Create the x and y axis for the chart.
        This goes to a separate method to easily replace it
        when implementing sub-classes
        """
        # Implement this method in the respective sub-class
        self.axisX = None
        self.axisY = None


    def _create_graph_controls(self):
        """
        Create widgets to control the graph layout
        """
        # Create a widgets which will hold all the controls
        self.graph_controls = QtWidgets.QWidget()
        gc_layout = QtWidgets.QHBoxLayout()

        # Menu button
        menu_button = QtWidgets.QPushButton("Menu")
        menu_button.clicked.connect(self._show_graph_menu)
        gc_layout.addWidget(menu_button)

        if self.has_xcontrols:
            # X-max field
            gc_layout.addWidget(QtWidgets.QLabel("X-max"))
            self.x_max_field = QtWidgets.QDoubleSpinBox()
            self.x_max_field.setMaximum(1e6)
            self.x_max_field.setMinimum(-1e6)
            self.x_max_field.setValue(1.0)
            self.x_max_field.setKeyboardTracking(False)
            self.x_max_field.valueChanged.connect(self._adjust_x_limits)
            gc_layout.addWidget(self.x_max_field)

            # X-min field
            gc_layout.addWidget(QtWidgets.QLabel("X-min"))
            self.x_min_field = QtWidgets.QDoubleSpinBox()
            self.x_min_field.setMaximum(1e6)
            self.x_min_field.setMinimum(-1e6)
            self.x_min_field.setValue(0.0)
            self.x_min_field.setKeyboardTracking(False)
            self.x_min_field.valueChanged.connect(self._adjust_x_limits)
            gc_layout.addWidget(self.x_min_field)

        # Y-max field
        gc_layout.addWidget(QtWidgets.QLabel("Y-max"))
        self.y_max_field = QtWidgets.QDoubleSpinBox()
        self.y_max_field.setMaximum(1e6)
        self.y_max_field.setMinimum(-1e6)
        self.y_max_field.setValue(1.0)
        self.y_max_field.setKeyboardTracking(False)
        self.y_max_field.valueChanged.connect(self._adjust_y_limits)
        gc_layout.addWidget(self.y_max_field)

        # Y-min field
        gc_layout.addWidget(QtWidgets.QLabel("Y-min"))
        self.y_min_field = QtWidgets.QDoubleSpinBox()
        self.y_min_field.setMaximum(1e6)
        self.y_min_field.setMinimum(-1e6)
        self.y_min_field.setValue(0.0)
        self.y_min_field.setKeyboardTracking(False)
        self.y_min_field.valueChanged.connect(self._adjust_y_limits)
        gc_layout.addWidget(self.y_min_field)

        # Field for the number of data points
        gc_layout.addWidget(QtWidgets.QLabel("# of data points"))
        self.x_number_field = QtWidgets.QSpinBox()
        self.x_number_field.setValue(self.series_length)
        self.x_number_field.setKeyboardTracking(False)
        self.x_number_field.valueChanged.connect(self._adjust_series_length)
        gc_layout.addWidget(self.x_number_field)

        # Set the layout and add the object to the graph widget
        self.graph_controls.setLayout(gc_layout)
        self.layout.addWidget(self.graph_controls)


    def _show_graph_menu(self):
        """
        Show a menu to adjust different settings of the graph
        """
        menu = GraphMenu(self)
        ret_val = menu.exec()


    def _adjust_x_limits(self):
        """
        Adjust the x-limits of the graph
        """
        # Get the limits from the fields
        x_max = self.x_max_field.value()
        x_min = self.x_min_field.value()

        # Set the new limits
        if x_max > x_min:
            self.axisX.setRange(x_min,x_max)


    def _adjust_y_limits(self):
        """
        Adjust the y-limits of the graph
        """
        # Get the limits from the fields
        y_max = self.y_max_field.value()
        y_min = self.y_min_field.value()

        # Set the new limits
        if y_max > y_min:
            self.axisY.setRange(y_min,y_max)


    def _adjust_series_length(self):
        """
        Adjust the number of data points to be displayed
        """
        self.series_length = self.x_number_field.value()


    def add_channel(self, name, unit = "-"):
        """
        Add a channel to the widget
        """
        # Create the line series which holds the data
        series = QtChart.QLineSeries()
        
        # Set the name of the series (for legend)
        series.setName(name)
        
        # Add the series to the chart
        self.chart.addSeries(series)
        
        # Attatch the axes to the series
        series.attachAxis(self.axisX)
        series.attachAxis(self.axisY)
        
        # Add the series to the channel_list
        self.channel_list.append(series)

        # # Create a button to remove the channel
        # icon = self.style().standardIcon(getattr(QtWidgets.QStyle, "SP_TitleBarCloseButton"))
        # remove_button = QtWidgets.QPushButton(icon,"")
        # remove_button.clicked.connect(self._remove_channel_from_button)


    def add_channel_from_node(self, node):
        """
        Add a channel, get the information automatically from the node.
        """
        # Check if node is an opcua node
        if not isinstance(node, SyncNode):
            print("Error: Selected item is nota an opcua node")
            return

        # Check if node is already in list and skip in this case
        if node in self.node_list:
            return

        # Add the node to the node list
        self.node_list.append(node)

        # Get the name and unit
        name = node.read_display_name().Text
        unit = "-" # Default value if no unit can be found
        for ch in node.get_children():
            if ch.read_browse_name().Name == "EngineeringUnits":
                eu_info = ch.get_value()
                unit = eu_info.DisplayName.Text

        # Add the channel widget
        self.add_channel(name, unit)


    def remove_channel(self, index):
        """
        Revove a channel from the widget
        """
        # Remove the channel from the graph
        self.chart.removeSeries(self.channel_list[index])

        # Remove the entries from the lists
        self.channel_list.pop(index)
        self.node_list.pop(index)


    def clear(self):
        """
        Remove all the channels.
        this must be done beginning with the last channel
        """
        num = len(self.channel_list)
        
        # Remove all channels from the graph
        self.chart.removeAllSeries()
        
        for i in range(num):
            j = num - i - 1
        #     parent = self.channel_list[j].parent()
            self.channel_list.pop(j)
            self.node_list.pop(j)


    def update_data(self):
        """
        Plot the new value for all nodes in the node list
        """
        # Implement this method in the respective sub-class
        pass



class GraphWidget(BaseGraphWidget):
    """
    Class which implements a graph widget for plotting time series
    """
    def  __init__(self, name="Graph"):
        """
        Initialization method
        """
        # Call the parent class initialization methd
        super().__init__(name)

        # Create the user interface
        self._setupUi()


    def _create_axes(self):
        """
        Create the x and y axis for the chart.
        This goes to a separate method to easily replace it
        when implementing sub-classes
        """
        # Create the x-axis (time)
        self.axisX = QtChart.QDateTimeAxis()
        self.axisX.setTickCount(5)
        self.axisX.setFormat("dd.MM.yyyy hh:mm:ss");
        self.axisX.setTitleText("Time");
        self.chart.addAxis(self.axisX,Qt.AlignBottom)

        # Create the y-axis (value)
        self.axisY = QtChart.QValueAxis();
        self.axisY.setTitleText("Value");
        self.chart.addAxis(self.axisY, Qt.AlignLeft);

        # Set default axis limits
        qdatetime_max = QDateTime.currentDateTime()
        qdatetime_min = qdatetime_max.addMSecs(-5000)
        self.axisX.setRange(qdatetime_min,qdatetime_max)
        self.axisY.setRange(0.0,1.0)


    def update_data(self):
        """
        Plot the new value for all nodes in the node list
        """
        #Get the current time for plotting
        qtime = QDateTime.currentDateTime()
        time = qtime.toMSecsSinceEpoch()

        for i, node in enumerate(self.node_list):
            # Get the value from the opc server
            value = node.get_value()

            # Remove the first data point if the list is already filled
            # TODO: Replace this odd procedure by a custom series containing a ringpuffer for data
            # This is dangerous for the memory. test well!
            while (len(self.channel_list[i].pointsVector()) >= self.series_length):
                self.channel_list[i].remove(0)

            # Append new data to the series
            self.channel_list[i].append(time,value)

        # Update the x-axis
        if len(self.channel_list) > 0:
            time_min_ms = int(self.channel_list[0].at(0).x())
            qdatetime_min = QDateTime()
            qdatetime_min.setMSecsSinceEpoch(time_min_ms)
            self.axisX.setRange(qdatetime_min,qtime)



class XYGraphWidget(BaseGraphWidget):
    """
    Class which implements a graph widget for plotting XY graphs
    i.e. plotting two different channels against each other
    """
    def  __init__(self, name="XYGraph"):
        """
        Initialization method
        """
        # Call the parent class initialization methd
        super().__init__(name)

        # Do some configuration
        self.has_xcontrols = True

        # Create the user interface
        self._setupUi()


    def _create_axes(self):
        """
        Create the x and y axis for the chart.
        This goes to a separate method to easily replace it
        when implementing sub-classes
        """
        # Create the x-axis
        self.axisX = QtChart.QValueAxis()
        self.axisX.setTickCount(5)
        self.axisX.setTitleText("Value 1");
        self.chart.addAxis(self.axisX,Qt.AlignBottom)

        # Create the y-axis
        self.axisY = QtChart.QValueAxis();
        self.axisY.setTitleText("Value 2");
        self.chart.addAxis(self.axisY, Qt.AlignLeft);

        # Set default axis limits
        self.axisX.setRange(0.0, 1.0)
        self.axisY.setRange(0.0,1.0)


    def add_channel(self, name, unit = "-"):
        """
        Add a channel to the widget
        Attention: For the XY graph we just have 1 channel each 2 nodes
        """
        #Check if we have an even number of channels to plot
        # otherwise do not do anything
        if  len(self.node_list)%2 == 1:
            return

        # Create the line series which holds the historic data
        line_series = QtChart.QLineSeries()
        point_series = QtChart.QScatterSeries()

        # Set the name of the series (for legend)
        line_series.setName(name)
        point_series.setName(name)

        # Add the series to the chart
        self.chart.addSeries(line_series)
        self.chart.addSeries(point_series)

        # Attatch the axes to the series
        line_series.attachAxis(self.axisX)
        line_series.attachAxis(self.axisY)
        point_series.attachAxis(self.axisX)
        point_series.attachAxis(self.axisY)

        # Predefine the point series' first point
        point_series.append(0,0)

        # Setup some appearence stuff for the point series
        point_series.setColor(line_series.color())
        self.chart.legend().markers(point_series)[0].setVisible(False);

        # Add the series to the channel_list
        self.channel_list.append(line_series)
        self.channel_list.append(point_series)


    def update_data(self):
        """
        Plot the value for all pairs of XY-data
        """
        #Check if we have an even number of channels to plot
        # otherwise do not do anything
        num_nodes = len(self.node_list)
        if  num_nodes%2 == 1:
            return

        for i in range(int(num_nodes/2)):
            # Get the values from the opc server
            value_x = self.node_list[2*i].get_value()
            value_y = self.node_list[2*i+1].get_value()

            # Remove the first data point if the list is already filled
            # TODO: Replace this odd procedure by a custom series containing a ringpuffer for data
            # This is dangerous for the memory. test well!
            while (len(self.channel_list[i].pointsVector()) >= self.series_length):
                self.channel_list[2*i].remove(0)

            # Append new data to the series
            self.channel_list[2*i].append(value_x,value_y)

            # For the scatter series we just replace the specific point
            self.channel_list[2*i+1].replace(0, value_x, value_y)


# ----------------------------------------------------------------------------

class GraphMenu(QDialog):
    """
    Popup window to adjust different settings of a graph
    """
    def __init__(self, parent):
        """
        Initialization method
        """
        # Call the parent class initialization method
        super().__init__(parent)

        # Build the user interface
        self._setupUi()


    def _setupUi(self):
        """
        Setup the user interface
        """
        layout = QVBoxLayout()

        header = QLabel("<h2>Graph Settings</h2>")
        layout.addWidget(header)

        # List of all channels which are currently active
        layout.addWidget(QLabel("Channels"))
        self._create_channelgrid()
        layout.addWidget(self.channelgrid)

        # Function to draw custom data
        draw_data_button = QPushButton("Draw custom data")
        layout.addWidget(draw_data_button)

        # Create OK and cancel buttons
        ok_button = QPushButton("OK")
        ok_button.clicked.connect(self.accept)
        cancel_button = QPushButton("Cancel")
        cancel_button.clicked.connect(self.reject)
        apply_button = QPushButton("Apply")
        button_layout = QHBoxLayout()
        button_layout.addWidget(ok_button)
        button_layout.addWidget(cancel_button)
        button_layout.addWidget(apply_button)
        button_container = QWidget()
        button_container.setLayout(button_layout)
        layout.addWidget(button_container)

        self.setLayout(layout)


    def _create_channelgrid(self):
        """
        Create a grid to display information about each channel
        """
        self.channelgrid = QWidget()
        layout = QGridLayout()

        for i, node in enumerate(self.parent().node_list):
            name = node.read_display_name().Text
            layout.addWidget(QLabel(name),i,0)
            icon = self.style().standardIcon(getattr(QStyle, "SP_TitleBarCloseButton"))
            remove_button = QPushButton(icon,"")
            remove_button.clicked.connect(self._remove_channel_from_button)
            layout.addWidget(remove_button,i,1)

        self.channelgrid.setLayout(layout)


    def _remove_channel_from_button(self):
        """
        Remove one channel from the view.
        Triggered by the signal, do no call programmatically.
        """
        grid_layout = self.channelgrid.layout()
        # Get the row index (=series index) of the button
        idx_grid = grid_layout.indexOf(self.sender())
        pos = grid_layout.getItemPosition(idx_grid)
        idx_row = pos[0]

        # Remove the channel
        self.parent().remove_channel(idx_row)

        # Remove the specific row in the dialog
        num_cols = grid_layout.columnCount()
        for idx_col in range(num_cols):
            item = grid_layout.itemAtPosition(idx_row, idx_col)
            item.widget().deleteLater()
