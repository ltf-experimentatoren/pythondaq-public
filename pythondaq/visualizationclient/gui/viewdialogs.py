#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (C) 2022 Daniel Jaeger

This file is part of PythonDAQ.

PythonDAQ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PythonDAQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PythonDAQ.  If not, see <http://www.gnu.org/licenses/>.

Module to implement dialogs for adding and selecting graphs
This module is part of PythonDAQ

@author: Daniel Jaeger
@date: 2021-2022
"""

from PyQt5.QtWidgets import QWidget, QDialog, QButtonGroup, QVBoxLayout, QHBoxLayout
from PyQt5.QtWidgets import QLabel, QRadioButton, QPushButton, QLineEdit


class AddViewDialog(QDialog):
    """
    Dialog to add a graph to the visualization
    """
    def __init__(self, default_name):
        """
        Initialization method
        """
        # Call the parent class initialization method
        super().__init__()

        # Storage variables
        self.view_name = default_name

        # Build the user interface
        self._setupUi()


    def _setupUi(self):
        """
        Fill the dialog with content
        """
        # Create a layout
        layout = QVBoxLayout()

        # Create radio buttons for all options
        option_1 = QRadioButton("Livechannel View")
        option_1.toggle()
        option_2 = QRadioButton("Line Graph")
        option_3 = QRadioButton("XY Graph")

        # Create a button group and add the options
        self.button_group = QButtonGroup()
        self.button_group.addButton(option_1)
        self.button_group.addButton(option_2)
        self.button_group.addButton(option_3)
        self.button_group.setId(option_1,1)
        self.button_group.setId(option_2,2)
        self.button_group.setId(option_3,3)

        # Add all radiobuttons to the layout
        layout.addWidget(option_1)
        layout.addWidget(option_2)
        layout.addWidget(option_3)
        self.setLayout(layout)

        # Create OK and cancel buttons
        ok_button = QPushButton("OK")
        ok_button.clicked.connect(self.accept)
        cancel_button = QPushButton("Cancel")
        cancel_button.clicked.connect(self.reject)
        button_layout = QHBoxLayout()
        button_layout.addWidget(ok_button)
        button_layout.addWidget(cancel_button)
        button_container = QWidget()
        button_container.setLayout(button_layout)
        layout.addWidget(button_container)

        # Setup field for defining the name of the new view
        layout.addWidget(QLabel("Name"))
        self.name_field = QLineEdit()
        self.name_field.setText(self.view_name)
        layout.addWidget(self.name_field)


    def accept(self):
        """
        Sets the return code and closes the dialog
        """
        # Get the id of the seleected option
        selection = self.button_group.checkedId()
        # Return the id or -1 if no selection is made
        self.done(selection)


    def reject(self):
        """
        Quit the dialog with cancel signal
        No checks have to be performed
        """
        # Return -1 to show that no option was selected
        self.done(-1)


    def getName(self):
        """
        Get the selected name for the new view
        """
        return str(self.name_field.text())
