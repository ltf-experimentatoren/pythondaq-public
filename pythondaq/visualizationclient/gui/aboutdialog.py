#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (C) 2022 Daniel Jaeger

This file is part of PythonDAQ.

PythonDAQ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PythonDAQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PythonDAQ.  If not, see <http://www.gnu.org/licenses/>.

@author: Daniel Jaeger
@date: 2022
"""

import os

from PyQt5.QtWidgets import QWidget, QTabWidget, QTextEdit, QVBoxLayout
from PyQt5.QtWidgets import QLabel


class AboutDialog(QTabWidget):
    """
    Class for the about dialog
    """
    def __init__(self):
        """
        Initialization method
        """
        # Call the parent class initalization method
        super().__init__()

        # Setup the window
        self.setFixedSize(600, 400)
        # Create the widgets
        self.setupUi()


    def setupUi(self):
        """
        Set up all widgets inside the user interface
        """
        # Get the path to this python file to search for the text files
        file_path = os.path.dirname(os.path.abspath(__file__))

        # First tab - overview
        overview_tab = QWidget()
        overview_layout = QVBoxLayout()
        overview_layout.addWidget(QLabel("<h2>About PythonDAQ VisualizationClient</h2>"))
        disclaimer = QLabel("Copyright (C) 2022  Daniel Jaeger, and others;\n" +\
            "see 'Contributors' tab for the full list of contributors.\n"+\
            "This program comes with ABSOLUTELY NO WARRANTY.\n"+\
            "This is free software, and you are welcome to redistribute it\n"+\
            "under certain conditions; see 'License' tab for details.\n")
        overview_layout.addWidget(disclaimer)
        overview_tab.setLayout(overview_layout)
        self.addTab(overview_tab, "Overview")

        # Second tab - contributors
        with open(file_path + "/../../../CONTRIBUTING",'r') as contrib_file:
            contrib_text = contrib_file.read()
        contrib_tab = QTextEdit()
        contrib_tab.setText(contrib_text)
        contrib_tab.setReadOnly(True)
        self.addTab(contrib_tab, "Contributors")

        # Third tab - license
        with open(file_path + "/license_gpl30.md",'r') as lic_file:
            lic_text = lic_file.read()
        lic_tab = QTextEdit()
        lic_tab.setMarkdown(lic_text)
        lic_tab.setReadOnly(True)
        self.addTab(lic_tab, "License")
