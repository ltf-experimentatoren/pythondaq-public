#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (C) 2022 Daniel Jaeger

This file is part of PythonDAQ.

PythonDAQ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PythonDAQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PythonDAQ.  If not, see <http://www.gnu.org/licenses/>.

This module contains a widget for showing live data.

@author: Daniel Jaeger
@date: 2020-08-28
"""

from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt

from asyncua.sync import SyncNode


class LiveChannelWidget(QtWidgets.QWidget):
    def  __init__(self, name="Channels"):
        """
        Initialization method
        """
        # Call the parent class initialization methd
        super().__init__()

        # Storage variables
        self.name = name            # The displayed name of the widget

        # Create the layout manager for the central frame
        self.layout = QtWidgets.QVBoxLayout()
        self.layout.setAlignment(Qt.AlignTop)

        # Draw a header label for the channel list
        header = QtWidgets.QLabel(f"<h2>{self.name}</h2>")

        # Add the widgets to the layout manager
        self.layout.addWidget(header)

        # Set the layout as actual layout manager
        self.setLayout(self.layout)

        # Initialize a list of nodes and channels
        self.node_list = []
        self.channel_list = []


    def add_channel(self, name, unit):
        """
        Add a channel to the widget
        """
        # Create a widget which holds the content
        frame = QtWidgets.QWidget()
        layout = QtWidgets.QHBoxLayout()

        # Create the data field
        ch = QtWidgets.QLineEdit('0')
        ch.setReadOnly(True)
        self.channel_list.append(ch)

        # Create the label and unit
        label = QtWidgets.QLabel(name)
        unit = QtWidgets.QLabel(unit)
        
        # Create a button to remove the channel
        icon = self.style().standardIcon(getattr(QtWidgets.QStyle, "SP_TitleBarCloseButton"))
        remove_button = QtWidgets.QPushButton(icon,"")
        remove_button.clicked.connect(self._remove_channel_from_button)

        # Layout management
        layout.addWidget(label)
        layout.addWidget(ch)
        layout.addWidget(unit)
        layout.addWidget(remove_button)
        frame.setLayout(layout)

        # Add the widget to the livechannel-widget
        self.layout.addWidget(frame)


    def add_channel_from_node(self, node):
        """
        Add a channel, get the information automatically from the node.
        """
        # Check if node is an opcua node
        if not isinstance(node, SyncNode):
            print("Error: Selected item is nota an opcua node")
            return

        # Check if node is already in list and skip in this case
        if node in self.node_list:
            return

        # Add the node to the node list
        self.node_list.append(node)

        # Get the name and unit
        name = node.read_display_name().Text
        unit = "-" # Default value if no unit can be found
        for ch in node.get_children():
            if ch.read_browse_name().Name == "EngineeringUnits":
                eu_info = ch.get_value()
                unit = eu_info.DisplayName.Text

        # Add the channel widget
        self.add_channel(name, unit)


    def remove_channel(self, widget):
        """
        Revove a channel from the widget
        """
        # Get all the children of the channel widget
        children = widget.children()

        # Get the index in the channel_list
        index = self.channel_list.index(children[2])

        # Delete the objects
        for obj in children:
            obj.deleteLater()
        widget.deleteLater()

        # Remove the entries from the lists
        self.channel_list.pop(index)
        self.node_list.pop(index)


    def _remove_channel_from_button(self):
        """
        Remove one channel from the view.
        Triggered by the signal, do no call programmatically.
        """
        # Get the parent (=channel-widget) of the button which emitted the signal        
        parent = self.sender().parent()

        # Remove the channel
        self.remove_channel(parent)


    def clear(self):
        """
        Remove all the channels.
        this must be done beginning with the last channel
        """
        num = len(self.channel_list)

        for i in range(num):
            j = num - i - 1
            parent = self.channel_list[j].parent()
            self.remove_channel(parent)


    def update_data(self):
        """
        Update the value for all nodes in the node list
        """
        for i, node in enumerate(self.node_list):
            value = node.get_value()
            self.channel_list[i].clear()
            self.channel_list[i].setText("{:.2f}".format(value))
