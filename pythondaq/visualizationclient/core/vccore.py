#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (C) 2022 Daniel Jaeger

This file is part of PythonDAQ.

PythonDAQ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PythonDAQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PythonDAQ.  If not, see <http://www.gnu.org/licenses/>.

Core module for the VisualizationClient of PythonDAQ

@author: Daniel Jaeger
@date: 2020
"""

# FreeOPCUA library is used to implement the opc client
# https://github.com/FreeOpcUa
# Install it using pip3 install freeopcua and pip3 install cryptography

from asyncua.sync import Client


class VCCore():
    """
    This class defines the backend of the visualization client.
    """
    def __init__(self):
        """
        Initialization method.
        """
        pass


    def connect(self,url):
        """
        Initialize the OPC UA client to archieve the data from the server
        """
        self.opc_client = Client(url)
        self.opc_client.connect()

        self.root = self.opc_client.nodes.root
        children = self.root.get_children()
        objects = children[0].get_children()
        self.variables = objects[1].get_variables()
        print(f"Connected to URL {url}")


    def disconnect(self):
        """
        Disconnect from the OPC server
        """
        self.opc_client.disconnect()
        #self.opc_client.close_session()
        print("Disconnected")


    def getVariableNames(self):
        """
        Returns a list containing all variable names located on the server
        """
        names = []
        for var in self.variables:
            names.append(var.get_display_name().Text)
        return names


    def getUnits(self):
        """
        Get the unit for the given variables
        """
        units = []
        for var in self.variables:
            # !!!! Hard coded workaround to make the gui work
            units.append('Pa')
        return units


    def getData(self):
        """
        Get the latest data value for the given variables
        """
        data = []
        for var in self.variables:
            data.append(var.get_value())
        return data


    def get_root_node(self):
        """
        Get the root node
        """
        return self.root
