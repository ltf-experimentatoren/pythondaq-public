#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (C) 2022 Lukas Selmayr

This file is part of PythonDAQ.

PythonDAQ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PythonDAQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PythonDAQ.  If not, see <http://www.gnu.org/licenses/>.

Read measurement data from MDF4 files

@author: Lukas Selmayr
@date: 2022
"""

import numpy as np
import xml.etree.ElementTree as ElementTree

try:
    from asammdf import MDF
    asam_available = True
except ImportError:
    asam_available = False

class MDFDictReader():
    """
    Class to read measurement data from MDF4 files.
    The data is returned as a dictionary.
    """

    '''
    TODO:
        numpy array time richtig einpflegen
        getChannel coden
        getAllChannels coden
        mit SSD ausprobieren
    '''

    def __init__(self, fileName):
        """
        Initialization method.
        The filename is the full path to the measurement file
        """

        if (asam_available == False):
            raise ImportError("Package asammdf is not available")

        self.meas_dict = {}

        self.file = MDF(fileName)
        self.file_dict = self.file.info()

        # start time of measurement for timestep correction
        self.start_time = self.file.start_time

        self.meas_dict = {'Metadata': {'Version': self.file_dict['version'], 'Program': self.file_dict['program']}}

        # Get the xml data from the comment
        xml_root = ElementTree.fromstring(self.file_dict['comment'])

        # Get the infos out of the comment string
        for parent in xml_root:
            for child in parent:
                self.meas_dict['Metadata'][child.attrib['name']] = child.text

        # Create time dict in mdf dict
        for group in range(self.file_dict['groups']):
            group_key = 'group ' + str(group)
            self.meas_dict[group_key] = {}
            self.meas_dict['Time of Measurement'] = {'-': 0}
            for i in range(self.file_dict[group_key]['channels count']-1):
                self.meas_dict[group_key][self.file_dict[group_key]['channel '+str(i+1)].split('"')[1]] = {'unit': '-', 'data': []}


    def getChannel(self, channelName):
        """
        Read one channel of the mdf file
        """

        channel = self.file.select([channelName])[0]

        for group in range(self.file_dict['groups']):
            group_key = 'group ' + str(group)
            if channelName == 'Timestamp':
                dt = channel.samples*10**(6)
                time_list = np.datetime64(self.start_time) + dt.astype('timedelta64[us]')
                self.meas_dict['Time of Measurement']['-'] = {'unit': '-', 'data': time_list}
            else:
                self.meas_dict[group_key][channelName] = {'unit': channel.unit, 'data': channel.samples}


    def getAllChannels(self):
        """
        Get all data from the mdf file
        """

        # get the measurement time channel
        dt = self.file.select(['Timestamp'])[0].samples*10**(6)
        time_list = np.datetime64(self.start_time) + dt.astype('timedelta64[us]')
        self.meas_dict['Time of Measurement']['-'] = {'unit': '-', 'data': time_list}

        # get data from all channels and fill dicts with numpy arrays
        for group in range(self.file_dict['groups']):
            group_key = 'group ' + str(group)
            for sig in self.file.iter_channels():
                self.meas_dict[group_key][sig.name] = {'unit': sig.unit, 'data': sig.samples}



    def getDictionary(self):
        """
        Get the prepared data dictionary
        """
        return self.meas_dict
