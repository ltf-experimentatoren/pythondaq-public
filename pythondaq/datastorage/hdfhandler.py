#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (C) 2022 Daniel Jaeger, Patrick Bachmann, Lukas Selmayr

This file is part of PythonDAQ.

PythonDAQ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PythonDAQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PythonDAQ.  If not, see <http://www.gnu.org/licenses/>.

Write and read measurement data to and from hdf5 files

@author: Daniel Jaeger
@author: Patrick Bachmann
@author: Lukas Selmayr
@date: 2021-2022
"""

import h5py
import datetime
import numpy as np


class HDFWriter():
    """
    Class to store measurement values as hdf5
    """
    def __init__(self,filename, sensors, computations, max_pts = -1):
        """
        Initialize the class
        Set the maximum number of datapoints to -1 for infinite recording
        """
        # Create file
        file = h5py.File(filename,'w')

        # Create metadata group
        file.create_group("Metadata")
        # Date
        dt = datetime.datetime.now()
        today = str(dt.strftime("%Y-%m-%d %H:%M:%S"))
        date = today[:11]
        file.create_dataset("Metadata/Date", data=date.encode("ascii"))
        # Time
        time = today[11:]
        file.create_dataset("Metadata/Time", data=time.encode("ascii"))
        # Software Version
        sv = "0.0.1"
        file.create_dataset("Metadata/Software_Version", data=sv.encode("ascii"))

        # Create Data group
        file.create_group("Data")

        # Write channel labeling as dataset attributes
        sensorline = []
        channelline = []
        unitline = []
        for sens in sensors:
            for i in range(sensors[sens].num_channels):
                sensorline.append(sensors[sens].name)
                channelline.append(sensors[sens].channel_names[i])
                unitline.append(sensors[sens].units[i])
        for comp in computations:
            for i in range(computations[comp].num_returns):
                sensorline.append(computations[comp].name)
                channelline.append(computations[comp].return_names[i])
                unitline.append(computations[comp].units[i])
        # For every channel
        for i in range(len(sensorline)):
            set_d = file.create_dataset("Data/Dataset_"+str(i+1), dtype='f', data=np.array([]), maxshape=(None,))
            set_d.attrs['Sensorname'] = sensorline[i]
            set_d.attrs['Channelname'] = channelline[i]
            set_d.attrs['Unit'] = unitline[i]

        # One dataset fo measurement time
        file.create_dataset("Data/Measurement_Time", data=np.array([]), maxshape=(None,))

        # Close File
        file.close()

        # Some storage variables
        self.max_datapts = max_pts # -1 for continuous writing
        self.datapts = 0

        # Open file in append-mode
        self.file = h5py.File(filename,'a')


    def close(self):
        """
        Close the file
        """
        self.file.close()


    def writeData(self, data):
        """
        Write measurement data
        """
        # Add current measure time to maesurement time dataset
        now = datetime.datetime.now()
        current_time = now.strftime("%H:%M:%S.%f")[:-3]
        self.file['Data']["Measurement_Time"].resize((self.file['Data']["Measurement_Time"].shape[0]+1,))
        self.file['Data']["Measurement_Time"][-1] = np.array(float(current_time.replace(':','')))

        # list of values
        line = []
        # add data to list of values
        for sublist in data.values():
            for elem in sublist.values():
                line.append(elem)
        # add values to datasets
        for i in range(len(line)):
            setname = "Dataset_"+str(i+1)
            self.file['Data'][setname].resize((self.file['Data'][setname].shape[0]+1,))
            self.file['Data'][setname][-1] = line[i]

        # Increment the counter
        self.datapts +=1


    def maxReached(self):
        """
        Return true if the maximum number of lines have been written
        """
        if self.max_datapts == -1:
            return False
        else:
            if self.datapts >= self.max_datapts:
                return True
            else:
                return False


    def setMaxDataPts(self, number):
        """
        Set the maximum number of data points
        """
        self.max_datapts = number



class HDFReader():
    """
    Class to read measurement values from hdf5 file
    """
    def __init__(self,filename):
        """
        Initialize the class and
        Read data from file

        Parameters
        ----------
        filename : string
            name of data file with extension.
        """
        # Predefine
        self.datasets = []
        self.sensorline = []
        self.channelline = []
        self.unitline = []

        # Open file in read mode
        file = h5py.File(filename, "r")

        # Read metadata
        datetime_string = str(file['Metadata']['Time'][()])
        datetime_string = datetime_string.replace('b', '')
        date_string = str(file['Metadata']['Date'][()])
        date_string = date_string.replace('b','')
        self.time = datetime.time.fromisoformat(datetime_string[1:-1])
        self.date = datetime.date.fromisoformat(date_string[1:-2])
        self.version = str(file['Metadata']['Software_Version'][()])[2:7]

        # Read measurement time
        measurement_time = file["Data"]["Measurement_Time"][:]
        self.measurement_time = []

        for time in measurement_time:
            if len(str(time)) == 9:
                str_time = str(time)+"0"
            elif len(str(time)) == 8:
                str_time = str(time).replace('.0','')
            else:
                str_time = str(time)
            self.measurement_time.append(datetime.datetime.combine(self.date, datetime.time.fromisoformat(str_time[:2]+":"+str_time[2:4]+":"+str_time[4:])))
        # Read data and attributes
        amount_sets = len(list(file['Data'].keys()))-1
        for i in range(amount_sets):
            setname = "Dataset_"+str(i+1)
            self.sensorline.append(file["Data"][setname].attrs['Sensorname'])
            self.channelline.append(file["Data"][setname].attrs['Channelname'])
            self.unitline.append(file["Data"][setname].attrs['Unit'])
            self.datasets.append(file["Data"][setname][:].tolist())
        # Close file
        file.close()


class HDFDictReader():
    """
    Class to read measurement values from hdf5 file.
    The data is ouputted as a dictionary.
    """
    def __init__(self,filename):
        """
        Initialize the class and
        Read data from file

        Parameters
        ----------
        filename : string
            name of data file with extension.
        """
        # Initialize the data structure
        self.meas_dict = {}
        self.meas_dict['Metadata'] = {}
        meas_time = []
        datasets = []
        sensorline = []
        channelline = []
        unitline = []

        # Open file in read mode
        file = h5py.File(filename, "r")

        # Read metadata
        date = datetime.date.fromisoformat(str(file['Metadata']['Date'][()])[2:12])
        self.meas_dict['Metadata']['Date'] = date
        self.meas_dict['Metadata']['Time'] = datetime.time.fromisoformat(str(file['Metadata']['Time'][()])[2:10])
        self.meas_dict['Metadata']['Software_Version'] = str(file['Metadata']['Software_Version'][()])[2:7]

        # Read measurement time
        measurement_time = file["Data"]["Measurement_Time"][:]
        for i, time in enumerate(measurement_time):
            meas_time.append(str(time)[:2]+":"+str(time)[2:4]+":"+str(time)[4:6])
            meas_time[i] = datetime.time.fromisoformat(meas_time[i])
            meas_time[i] = datetime.datetime.combine(date, meas_time[i])
        self.meas_dict["Time of Measurement"] = {'-':{'unit': '-', 'data': meas_time}}

        # Read data and attributes
        amount_sets = len(list(file['Data'].keys()))-1
        for i in range(amount_sets):
            setname = "Dataset_"+str(i+1)
            sensorline.append(file["Data"][setname].attrs['Sensorname'])
            channelline.append(file["Data"][setname].attrs['Channelname'])
            unitline.append(file["Data"][setname].attrs['Unit'])
            datasets.append(file["Data"][setname][:])

        # Create a field for each sensor
        for sens in sensorline:
            if sens not in self.meas_dict.keys():
                self.meas_dict[sens] = {}

        # Create a field for each channel
        for i, chan in enumerate(channelline):
            self.meas_dict[sensorline[i]][chan] = {}

        # Store meta-infos of each channel
        for i, unit in enumerate(unitline):
            self.meas_dict[sensorline[i]][channelline[i]]['unit'] = unit

        for i, data in enumerate(datasets):
            self.meas_dict[sensorline[i]][channelline[i]]['data'] = data.tolist()

        # Close file
        file.close()

    def getDictionary(self):
        """
        Get the full data dictionary
        """
        return self.meas_dict


    def getChannelline(self):
        """
        Get an array of all channel names
        """
        return self.channelline
