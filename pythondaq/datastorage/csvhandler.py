#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (C) 2022 Daniel Jaeger, Patrick Bachmann, Lukas Selmayr

This file is part of PythonDAQ.

PythonDAQ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PythonDAQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PythonDAQ.  If not, see <http://www.gnu.org/licenses/>.

Write and Read measurement data to and from csv file

@author: Daniel Jaeger
@author: Patrick Bachmann
@author: Lukas Selmayr
@date: 2020-2022
"""

import csv
import os
import datetime


class CSVWriter():
    """
    Class to store measurement values as csv
    """
    def __init__(self,filename, sensors, computations, max_pts = -1):
        """
        Initialize the class
        Set the maximum number of datapoints to -1 for infinite recording
        """
        # Open file and initialize the writer
        self.file = open(filename, 'w', newline='', encoding = 'utf-8')
        self.writer = csv.writer(self.file, delimiter=';', quotechar='|', quoting=csv.QUOTE_MINIMAL)

        # Write header
        self.writer.writerow(["Measurement File"])
        time = datetime.datetime.now()
        self.writer.writerow(["Date",time.strftime("%Y-%m-%d %H:%M:%S")])
        self.writer.writerow(["Software Version","0.0.1"])
        self.writer.writerow([])

        # Channel labeling
        sensorline = []
        channelline = []
        unitline = []
        # First column is time data
        sensorline.append("Time of Measurement")
        channelline.append("-")
        unitline.append("-")
        # Sensor and channel names
        for sens in sensors:
            for i in range(sensors[sens].num_channels):
                sensorline.append(sensors[sens].name)
                channelline.append(sensors[sens].channel_names[i])
                unitline.append(sensors[sens].units[i])
        for comp in computations:
            for i in range(computations[comp].num_returns):
                sensorline.append(computations[comp].name)
                channelline.append(computations[comp].return_names[i])
                unitline.append(computations[comp].units[i])
        # Write the data to the file
        self.writer.writerow(sensorline)
        self.writer.writerow(channelline)
        self.writer.writerow(unitline)
        self.writer.writerow([])

        # Some storage variables
        self.max_datapts = max_pts # -1 for continuous writing
        self.datapts = 0


    def close(self):
        """
        Close the file
        """
        self.file.close()


    def writeData(self, data):
        """
        Add one set of measurement data to file
        """
        # list of values for one row
        line = []

        # Measurement time
        now = datetime.datetime.now()
        current_time = now.strftime("%H:%M:%S.%f")[:-3]
        line.append(current_time)

        # add data to list of values
        for sublist in data.values():
            for elem in sublist.values():
                line.append(str(elem))

        # Write data to the file
        self.writer.writerow(line)

        # Increment the counter
        self.datapts +=1


    def maxReached(self):
        """
        Return true if the maximum number of lines have been written
        """
        if self.max_datapts == -1:
            return False
        else:
            if self.datapts >= self.max_datapts:
                return True
            else:
                return False


    def setMaxDataPts(self,number):
        """
        Set the maximum number of data points
        """
        self.max_datapts = number

# Durchaus interessant koennte der DictWriter sein
#csv.DictWriter
#https://docs.python.org/3/library/csv.html



class CSVReader():
    """
    Class to read measurement values from csv file
    """
    def __init__(self, filename, path=None):
        """
        Initialize the class and
        Read data from file

        Parameters
        ----------
        filename : string
            name of data file with extension.
        path : string (optional)
            path to data file.
        """
        # Predefine
        self.datasets = []
        self.measurement_time = []

        # Form full path if necessary
        if path:
            full_path = os.path.join(path, filename)
        else:
            full_path = filename

        # Open file in read mode
        with open(full_path, 'r', encoding = 'utf-8') as file:
            # Read data in text format
            text_data = file.readlines()

        # Remove "\n"
        text_data = [l.replace("\n", "") for l in text_data]

        # Header
        datetime_string = text_data[1]
        datetime_string = datetime_string.replace('Date;', '')
        datetime_array = datetime_string.split()
        self.date = datetime.date.fromisoformat(datetime_array[0])
        self.time = datetime.time.fromisoformat(datetime_array[1])
        version = text_data[2]
        self.version = version.replace('Software Version;', '')

        # Channel labeling
        sensorline = text_data[4]
        sensorline = sensorline.split(';')
        sensorline.pop(0)# Remove first column (measurement time)
        self.sensorline = sensorline
        channelline = text_data[5]
        channelline = channelline.split(';')
        channelline.pop(0)
        self.channelline = channelline
        unitline = text_data[6]
        unitline = unitline.split(';')
        unitline.pop(0)
        self.unitline = unitline

        # Create sublists
        for i in range(len(self.sensorline)):
            self.datasets.append([])

        # Read data
        text_data_m = text_data[8:]# without header
        for i in range(len(text_data_m)):
            datalist = text_data_m[i].split(';')
            for n in range(len(datalist)):
                if n == 0:
                    row_time = datetime.time.fromisoformat(datalist[n])
                    self.measurement_time.append(datetime.datetime.combine(self.date, row_time))

                else:
                    self.datasets[n-1].append(float(datalist[n]))


    def getChannelline(self):
        return self.channelline



class CSVDictReader():
    """
    Class to read measurement data from CSV files.
    The data is ouputted as a dictionary.

    @author: Daniel Jaeger
    """

    def __init__(self, filename):
        """
        Initialization method.
        The filename is the full path to the measurement file
        """
        # Check if the filename is a valid file
        if not os.path.isfile(filename):
            return

        # Initialize the data structure
        self.meas_dict = {}
        self.meas_dict['Metadata'] = {}

        # Open the text file and perform readout
        with open(filename, 'r', encoding = 'utf-8') as csvfile:
            # Initialize the CSV reader
            reader_1 = csv.reader(csvfile, delimiter = ';')

            # Forward to second row
            reader_1.__next__()

            # Read header (date, time and software version)
            datetime_string = reader_1.__next__()[1]
            datetime_array = datetime_string.split()
            self.date = datetime.date.fromisoformat(datetime_array[0])
            self.time = datetime.time.fromisoformat(datetime_array[1])
            self.meas_dict['Metadata']['Date'] = self.date
            self.meas_dict['Metadata']['Time'] = self.time

            self.version = reader_1.__next__()[1]
            self.meas_dict['Metadata']['Software_Version'] = self.version

            # Forward through empty line
            reader_1.__next__()

            self.sensorline = reader_1.__next__()
            self.channelline = reader_1.__next__()
            self.unitline = reader_1.__next__()

            # Create a field for each sensor
            for sens in self.sensorline:
                if sens not in self.meas_dict.keys():
                    self.meas_dict[sens] = {}

            # Create a field for each channel
            for i, chan in enumerate(self.channelline):
                self.meas_dict[self.sensorline[i]][chan] = {}

            # Store meta-infos of each channel
            for i, unit in enumerate(self.unitline):
                self.meas_dict[self.sensorline[i]][self.channelline[i]]['unit'] = unit
                self.meas_dict[self.sensorline[i]][self.channelline[i]]['data'] = []


            # Forward reader by 1 (empty line)
            reader_1.__next__()

            # Iterate through all data rows
            for row in reader_1:
                for i, elem in enumerate(row):
                    if 'Time' in self.sensorline[i]:
                        row_time = datetime.time.fromisoformat(elem)
                        self.meas_dict[self.sensorline[i]][self.channelline[i]]['data'].append( \
                            datetime.datetime.combine(self.date, row_time))
                    else:
                        self.meas_dict[self.sensorline[i]][self.channelline[i]]['data'].append(float(elem))


    def getDictionary(self):
        """
        Get the full data dictionary
        """
        return self.meas_dict


    def getChannelline(self):
        """
        Get an array of all channel names
        """
        return self.channelline
