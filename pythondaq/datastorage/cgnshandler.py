#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (C) 2022 Patrick Bachmann, Lukas Selmayr

This file is part of PythonDAQ.

PythonDAQ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PythonDAQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PythonDAQ.  If not, see <http://www.gnu.org/licenses/>.

Read measurement data from cgns file

@author: Patrick Bachmann
@author: Lukas Selmayr
@date: 2021-2022
"""

import h5py
import CGNS.MAP
import CGNS.PAT.cgnslib as cgns_l
import numpy as np
import CGNS.PAT.cgnskeywords as CK
import os
import math


class CGNSHandler():
    """
    Class to read measurement values from cgns file
    """
    def __init__(self):
        """
        Initialize the class
        """
        # dictionary with all data in it
        self.data = {}

        # path and name of cgns file
        self.filepath = ''

        # Array dimensions
        self.dimensions = []


    def readCGNS(self,filename, path, setname):
        """
        Read data from file

        Parameters
        ----------
        filename : string
            name of data file with extension.
        path : string
            path to data file.
        setname : string
            name of dataset which is stored in file, either statistic sets ('Mean', 'Median' or 'Standard_Deviation') or timestep dataset ('TS_<number>').
        """
        # Open file in read mode
        self.filepath = path + '/' + filename
        file = h5py.File(self.filepath, "r")

        # Read coordinates
        x = file['Base']['Measurement_Data']['GridCoordinates']['CoordinateX'][' data'][:]
        y = file['Base']['Measurement_Data']['GridCoordinates']['CoordinateY'][' data'][:]

        # Set dimensions
        self.dimensions = [x.shape[0], x.shape[1]]

        # Convert coordinates to polar system
        r = np.zeros((self.dimensions[0],self.dimensions[1]))
        phi = np.zeros((self.dimensions[0],self.dimensions[1]))
        for i in range(self.dimensions[0]):
            for n in range(self.dimensions[1]):
                r[i][n] = math.sqrt(x[i][n]**2 + y[i][n]**2)
                phi[i][n] = math.atan2(y[i][n], x[i][n])

        # Save Coordinates
        if not 'R' in self.data.keys():
            self.data['R'] = {}
            self.data['R']['unit'] = 'mm'
            self.data['R']['data'] = r
        if not 'Phi' in self.data.keys():
            self.data['Phi'] = {}
            self.data['Phi']['unit'] = 'rad'
            self.data['Phi']['data'] = phi

        # Filter all files that are datasets
        keylist = []
        for i, keyname in enumerate(list(file['Base']['Measurement_Data']['FlowSolution'].keys())):
            if keyname[:3]=='DS_':
                keylist.append(list(file['Base']['Measurement_Data']['FlowSolution'].keys())[i])

        # List of dataset names
        self.keylist = keylist

        # Read data arrays
        for keyname in keylist:
            # Build dictionary structure
            if not keyname in self.data.keys():
                self.data[keyname] = {}
                self.data[keyname]['data'] = {}
            # Read data
            self.data[keyname]['data'][setname] = file['Base']['Measurement_Data']['FlowSolution'][keyname][' data'][:]

        # Close file
        file.close()


    def writeCGNS(self, cgns_filename, path_postprocessing, dimensions, data):
        """
        Write cgns file

        Parameters
        ----------
        cgns_filename : string
            name of file with extension.
        path_postprocessing : string
            path where file should be stored.
        dimensions : list with length two
            length of dimensions of the two dimensional measurement grid.
        data : dictionary
            dictionary with all measurement data from traverse data handler.
        """
        self.filepath = path_postprocessing + '/' + cgns_filename + '.cgns'
        self.dimensions = dimensions

        # Tree
        T=cgns_l.newCGNSTree()

        # Base
        B = cgns_l.newBase(T,'Base',2,3) # name, physical dim, topological dim

        # Dimensional Units
        units=(CK.Kilogram_s,CK.Meter_s,CK.Second_s,CK.Kelvin_s,CK.Radian_s)
        cgns_l.newDimensionalUnits(B, units)

        # Reference State
        cgns_l.newReferenceState(B, CK.ReferenceState_s)

        # Data Class
        cgns_l.newDataClass(B, 'Dimensional')

        # Zone
        zsize = np.array([[self.dimensions[0],self.dimensions[0]-1,0],[self.dimensions[0],self.dimensions[0]-1,0]], order="F")
        Z = cgns_l.newZone(B, 'Measurement_Data', zsize, CK.Structured_s)#, 'Stream_Channel')

        # Reshape lists to fit the dimensions
        shaped_data = {}
        for keyname in data:
            shaped_data[keyname] = {}
        for keyname in data:
            if keyname != 'Metadata':
                if len(data[keyname]['data']) != 0:
                    shaped_data[keyname]['data'] = np.reshape(data[keyname]['data'],(self.dimensions[0], self.dimensions[1]))

        z = np.zeros((self.dimensions[0],self.dimensions[1]))

        # Grid Coordinates
        G = cgns_l.newGridCoordinates(Z,CK.GridCoordinates_s)
        cgns_l.newDataArray(G,CK.CoordinateX_s, shaped_data['X']['data'])
        cgns_l.newDataArray(G,CK.CoordinateY_s, shaped_data['Y']['data'])
        cgns_l.newDataArray(G,CK.CoordinateZ_s, z)

        # Flow Solution
        F = cgns_l.newFlowSolution(Z, 'FlowSolution')

        # Write dataset arrays
        for keyname in shaped_data:
            if keyname != 'Metadata' and keyname != 'R' and keyname != 'Phi' and keyname != 'X' and keyname != 'Y' and keyname != 'Z':
                cgns_l.newDataArray(F, keyname, shaped_data[keyname]['data'])

        if os.path.exists(self.filepath):
          os.remove(self.filepath)

        CGNS.MAP.save(self.filepath, T)