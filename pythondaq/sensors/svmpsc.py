#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (C) 2022 Daniel Jaeger

This file is part of PythonDAQ.

PythonDAQ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PythonDAQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PythonDAQ.  If not, see <http://www.gnu.org/licenses/>.

Module to aquire data from SVM PSC pressure measurement system.
For product information see www.svm-tec.de/shop

@author: Daniel Jaeger
@date: 2021-2022
"""

import socket
import datetime
import re

from .sensor import Sensor


class SvmXPscLan(Sensor):
    """
    Class for data aquisition from the SVM xPSC rack pressure measurement system via LAN.
    The command set is taken from the user manual on the homepage.
    Data aquisition is done in the request (poll) mode.
    """
    # Class variables
    TCP_PORT = 10001
    BUFFER_SIZE = 1024
    read_command = b"?\n"
    num_slots = 8

    def __init__(self, ip_address, name = "SvmPsc"):
        """
        Initialization method
        The number of channels is a list containing the number of channels for each slot
        """
        # Call the parent's initialization method
        super().__init__()

        # Store some instance variables
        self.ip = ip_address
        self.name = name

        # Initialization of the list variables
        self.num_channels = 0
        self.channel_names = []
        self.units = []
        self.sensor_range = []
        self.value_range = []
        self.gain = []
        self.offset = []

        # Network communication is always initialized automatically because
        # we have to query the module structure before proceeding
        self.initCommunication()

        # TODO: Implement module setup
        # Swith to request mode
        # Do the filter settings

        # Get one chunk of data without flattening to read module structure
        sample_data = self.get_raw_value(False)[1]

        # Do the channel configuration
        for idx_slot in range(self.num_slots):
            chan_in_slot = len(sample_data[idx_slot])
            self.num_channels += chan_in_slot
            for idx_ch in range(chan_in_slot):
                self.channel_names.append(f"S{idx_slot+1}Ch{idx_ch+1}")
                self.units.append("Pa")
                # Unfortunately we cannot get any range information from the sensor
                self.sensor_range.append([0.0,1.0])
                self.value_range.append([0.0,1.0])
                # Set the gain and offset to defaults
                self.gain.append(1.0)
                self.offset.append(0.0)

    # -------------------------------------------------------------------------
    # PythonDAQ-specific methods

    def initCommunication(self):
        """
        Initialize network communication with the sensor
        """
        self.sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        self.sock.connect((self.ip, self.TCP_PORT))


    def get_raw_value(self, flatten = True):
        """
        Get the acutal readings from the sensors
        """
        # Send the request command
        self.sock.sendall(self.read_command)

        # As the answer can be distributed over several ethernet frames
        # we have to make sure that we correctly received the whole data set
        set_complete = False
        num_iter = 0
        data_str = ""

        while((set_complete == False) and (num_iter < 5)):
                # Receive the answer and convert it to a string
                answer = str(self.sock.recv(self.BUFFER_SIZE))
                # Cut unnecessary characters and append the result string
                data_str = data_str + answer[2:-1]
                # Count the number of newline arguments
                num_newline = len(re.findall('n', data_str))
                # Check if #1 and #8 are in the dataset (set is not shifted)
                one_in_set = '#1' in data_str
                eight_in_set = '#8' in data_str
                # Combine the checks
                set_complete = (num_newline==8) and one_in_set and eight_in_set
                num_iter = num_iter + 1

        # TODO: Error message response handling

        # Get the actual timestamp
        timestamp = datetime.datetime.now()

        # Cut the result string into the slots of the rack
        data_str = data_str[:-4]    # Remove trailing characters
        data_list = data_str.split("\\r\\n")    # Split into individual slots

        # Split into channels and remove the slot numbering
        data_list = [slot.split("\\t") for slot in data_list]
        data_list = [slot[1:] for slot in data_list]

        # Convert the list to floating point
        data_list_fp = [[float(ch) for ch in slot] for slot in data_list]
        
        # Flatten the list if desired
        if flatten:
            data_list_fp = [ch for slot in data_list_fp for ch in slot]

        return [timestamp, data_list_fp]


    def close(self):
        """
        Close the network connection
        """
        self.sock.close()

    # -------------------------------------------------------------------------
    # General configuration functions

    def loadEEPROM(self):
        """
        Load a configuration from the EEPROM
        """
        self.sock.sendall(b"EE_LOAD\n")
        answer = str(self.sock.recv(self.BUFFER_SIZE))

        if "#EEPROM:loaded" in answer:
            return True
        else:
            return False


    def saveEEPROM(self):
        """
        Save configuration to the EEPROM
        """
        self.sock.sendall(b"EE_SAVE\n")
        answer = str(self.sock.recv(self.BUFFER_SIZE))

        if "#EEPROM:saved" in answer:
            return True
        else:
            return False


    def getDeviceInfo(self):
        """
        Get information about rack master and the modules in all slots
        Use commands "*IDN?" and "*IDN? x"
        """
        answer_dict = {}

        # Get the information about the rack master
        answer = self._send_and_receive("*IDN?")
        a_lst = answer.split()
        if len(a_lst) != 4:
            print(f"Error when decoding master information, response was {answer}")
            return
        answer_dict["Master"] = {"Model":a_lst[0], "SW_Version":a_lst[1], \
                                 "Serial_Number":a_lst[3]}

        # Now we get the information about all slots
        for idx_slot in range(self.num_slots):
            answer = self._send_and_receive(f"*IDN? {idx_slot+1}")
            if "#EMPTY" in answer:
                answer_dict[f"Slot_{idx_slot+1}"] = {"Model":"empty", "Serial_Number":None}
            else:
                a_lst = answer.split()
                answer_dict[f"Slot_{idx_slot+1}"] = {"Model":a_lst[0], "Serial_Number":a_lst[1]}

        # Finally return the data structure
        return answer_dict


    def getRate(self):
        """
        Get the rate which with the system is sending data in ms
        Acutally the whole code just works if rate=0 (request mode)
        """
        answer = self._send_and_receive("RATE?")
        # Just get the actual rate in ms
        answer = answer.split('=')[1]
        answer = answer.split('m')[0]
        return int(answer)


    def setRate(self, rate):
        """
        Set the data sending rate in ms.
        Be careful: Acutally the whole code just works if rate=0 (request mode)!
        """
        # Convert the rate to integer
        rate = int(rate)
        # Check if the rate is within the limits
        if (rate < 10) or (rate > 5000):
            print("Error, wrong rate, has to be between 10 and 5000 ms")
            return
        # Send the rate to the system
        return self._send_and_receive(f"RATE {rate}")


    def reset(self):
        """
        Reset the system to default settings
        """
        return self._send_and_receive("*RST")


    def performTara(self, slot = -1):
        """
        Perform a tara operation (rezero) for the specified slot.
        Set slot to -1 to perform tara on all slots.
        """
        # Convert the slot number to integer
        slot = int(slot)
        # Check if the rate is within the limits
        if ((slot < 1) or (slot > 8)) and (slot!=-1):
            print("Error, wrong slot, has to be between 1 and 8 or -1")
            return
        # Send the command
        return self._send_and_receive(f"TARA {slot}")


    def performPurge(self, slot = -1):
        """
        Perform a purge operation  for the specified slot.
        Set slot to -1 to perform purge on all slots.
        """
        # Convert the slot number to integer
        slot = int(slot)
        # Check if the rate is within the limits
        if ((slot < 1) or (slot > 8)) and (slot!=-1):
            print("Error, wrong slot, has to be between 1 and 8 or -1")
            return
        # Send the command
        return self._send_and_receive(f"PURGE {slot}")


    def getPurgeTime(self):
        """
        Get the time for which a purge operation is performed in ms
        """
        answer = self._send_and_receive("PURGE_TIME?")
        answer = answer.split()[1]
        answer = answer.split('m')[0]
        return int(answer)


    def setPurgeTime(self, time):
        """
        Get the time for which a purge operation is performed in ms
        """
        time = int(time)
        return self._send_and_receive(f"PURGE_TIME {time}")


    def getFilterTime(self):
        """
        Get the time of the exponential filter in ms
        """
        answer = self._send_and_receive("FILTER?")
        answer = answer.split()[1]
        return int(answer)


    def setFilterTime(self, time):
        """
        Set the time of the exponential filter in ms
        time=0 deactivates the filter
        """
        time = int(time)
        return self._send_and_receive(f"FILTER {time}")


    def setSimulationMode(self, on_off):
        """
        Switch the simulation mode on or off
        """
        if on_off:
            answer = self._send_and_receive("SIM 1")
        else:
            answer = self._send_and_receive("SIM 0")
        return answer

    # -------------------------------------------------------------------------
    # Private methods

    def _send_and_receive(self, command):
        """
        Send a command, wait for the complete answer and cut unnecessary characters.
        The command is handed to this function as string and automatically
        appended by termination characters and converted to bytes.
        The answer is expected to be terminated by CR-LF literal.
        """
        num_iter = 0
        data_str = ""

        # Send the command
        command += "\n"
        self.sock.sendall(command.encode('ascii'))

        # Wait for the answer to complete
        while(num_iter < 5):
            # Receive the answer and convert it to a string
            answer = str(self.sock.recv(self.BUFFER_SIZE))
            # Cut unnecessary characters and append the result string
            data_str += answer[2:-1]
            if "\\r\\n" in data_str:
                break
            num_iter +=1

        # Cut the termination characters and return the answer
        return data_str[:-4]
