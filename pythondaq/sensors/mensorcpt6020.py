#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (C) 2022 Daniel Jaeger

This file is part of PythonDAQ.

PythonDAQ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PythonDAQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PythonDAQ.  If not, see <http://www.gnu.org/licenses/>.

Module to aquire data from Mensor CPT 6020 high precission pressure sensors.

Be sure to check the following items of the sensor
Command-Set must be set to default (=0)
Unit index must be set to Pa (=23)

@author: Daniel Jaeger
@date: 2021-2022
"""

import serial
import datetime

from .sensor import Sensor


class MensorCpt6020(Sensor):
    """
    Class for data aquisition from the Mensor CPT 6020 high precission
    digital pressure sensor.
    Communication is realized via serial RS485 multidrop.
    """
    # Class variables
    num_channels = 1
    units = ["Pa"]

    def __init__(self, comport, address = "*", name = "MensorCPT6020", init_comm = True):
        """
        Initialization method
        Parameters:
            comport is the computer's serial port
            address is the RS485 address character. Default '*' is broadcast
            name is the displayed name of the sensor
            init_comm decides it the communication should be initialized
        """
        # Call the parent's initialization method
        super().__init__()

        # Store some instance variables
        self.comport = comport
        self.address = address
        self.name = name

        # Initialization of the list variables
        self.num_channels = 1
        self.channel_names = ["P1"]

        # Init network communication if desired
        if init_comm:
            self.initCommunication()

    # -------------------------------------------------------------------------
    # PythonDAQ-specific methods

    def initCommunication(self, create_new_connection = True):
        """
        Initialize serial communication with the sensor
        """
        if create_new_connection:
            # Communication parameters are 57600,N,8,1
            self.ser = serial.Serial(self.comport, 57600, timeout=5)

        # Get the command version to make sure everything works
        cmd_version = self.getCommandsetVersion()
        # TODO: Implement abortion if this is not set correctly
        if cmd_version != 0:
            print(f"Error, wrong command set version {cmd_version} insted of 0!")
            return

        # Readout the sensor range and the used units
        self.sensor_range = [self.getRange()]
        self.value_range = [self.getRange()]
        self.units = [self.getUnit()[0]]


    def setSerialConnection(self, serial_connection):
        """
        Assign an exsiting serial connection to the instance
        This has to be used for more than one sensor in multidrop mode.
        The serial connection passed to this method has to be already open.
        """
        self.ser = serial_connection


    def get_raw_value(self, flatten = True):
        """
        Get the acutal readings from the sensor
        """
        # Send the read command and receive the answer
        pressure = self.getPressureReading()
        # Get the actual timestamp
        timestamp = datetime.datetime.now()
        return [timestamp, [pressure]]


    def close(self):
        """
        Close the serial communication
        """
        self.ser.close()

    # -------------------------------------------------------------------------
    # General configuration functions
    # Mensor global commands

    def getAddress(self):
        """
        Get the RS485 address from the sensor
        """
        return self._send_and_receive("ADDRESS?")


    def getAllAddresses(self):
        """
        Get the address from all connected sensors
        This command is sent in broadcast mode.
        Attention: Possible line jamming, so this method is not reliable!
        """
        answer = self._send_and_receive("ADDRESS?", broadcast = True)
        return answer


    def setAddress(self, address):
        """
        Assign a new address to the sensor.
        Update the instance variable aswell
        """
        answer = self._send_and_receive(f"ADDRESS {address}")
        print(answer)
        if "Ready" in answer:
            self.address = address
            print(f"The address of the sensor has been updated to {self.address}")
        else:
            print("Error while setting the new address")


    def getDeviceInfo(self):
        """
        Get information about the sensor
        Use command "*IDN?"
        """
        # Get the information from the sensor
        answer = self._send_and_receive("*IDN?")
        a_lst = answer.split(', ')
        # Check the answer length
        if len(a_lst) != 4:
            print(f"Error when decoding answer, it was: {answer}")
            return
        # Decode the answer
        answer_dict = {"Manufacturer":a_lst[0], "Model":a_lst[1], \
                       "Serial_Number":a_lst[2], "SW_Version":a_lst[3]}
        return answer_dict


    def saveChanges(self):
        """
        Save configuration changes to the EEPROM
        """
        answer = self._send_and_receive("SAVE")
        if "Ready" in answer:
            print("Successfully saved settings to EEPROM")
        else:
            print(f"Error while saving settings to EEPROM, sensor answer is: {answer}")


    def getPressureType(self):
        """
        Get the pressure measuring type
        """
        answer = self._send_and_receive("TYPE?")
        return answer


    def getFilterPercentage(self):
        """
        Get the filter percentage between 1 and 99 percent
        """
        answer = self._send_and_receive("FILTER?")
        return int(answer)


    def setFilterPercentage(self, percentage):
        """
        Set the filter percentage to a value between 1 and 99 percent
        """
        # Round to integer value if necessary
        percentage = int(percentage)
        # Check if we are in the possible range
        if (percentage < 1) or (percentage > 99):
            raise ValueError("Incorrect percentage, has to be between 1 and 99, " \
                             + f"but was {percentage}!")
        # Send the new value to the sensor
        answer = self._send_and_receive(f"FILTER {percentage}")
        if "Ready" in answer:
            print(f"The filter percentage has been updated to {percentage}")
        else:
            print(f"Error while setting the new filter percentage, sensor answer is: {answer}")


    def getFilterWindow(self):
        """
        Get the filter window
        The value 0 ... 99 to correspond to a filter window of 0 ... 0.099%FS
        """
        answer = self._send_and_receive("WINDOW?")
        return int(answer)


    def setFilterWindow(self, value):
        """
        Set 0 ... 99 to correspond to a filter window of 0 ... 0.099%FS
        """
        # Round to integer value if necessary
        value = int(value)
        # Check if we are in the possible range
        if (value < 0) or (value > 99):
            raise ValueError("Incorrect window, has to be between 0 and 99, but was {value}!")
        # Send the new value to the sensor
        answer = self._send_and_receive(f"WINDOW {value}")
        if "Ready" in answer:
            print(f"The filter window has been updated to {value}")
        else:
            print(f"Error while setting the new filter window, sensor answer is: {answer}")


    def getBaudrate(self):
        """
        Get the baudrate
        """
        answer = self._send_and_receive("BAUD?")
        return int(answer)


    def setBaudrate(self, rate):
        """
        Set the baud rate.
        Possible values are: 9600, 19200, 57600, 115200
        """
        # Round to integer value if necessary
        rate = int(rate)
        # Check if we are in the possible range
        if (rate!=9600) and (rate!=19200) and (rate!=57600) and (rate!=115200):
            raise ValueError("Incorrect baudrate, has to be between 0 and 99, but was {rate}!")
        # Send the new value to the sensor
        answer = self._send_and_receive(f"BAUD {rate}")
        if "Ready" in answer:
            print(f"The baudrate has been updated to {rate}")
        else:
            print(f"Error while setting the new baudrate, sensor answer is: {answer}")


    def clearErrorStack(self):
        """
        Clear the sensor's error stack
        """
        answer = self._send_and_receive("CERR")
        if "Ready" in answer:
            print("Successfully clear the error stack")
        else:
            print(f"Error while clearing the error stack, sensor answer is: {answer}")


    def reset(self):
        """
        Reset the sensor to default parameters:
        Filter ... 90
        Window ... 20
        Baud ... 57600
        CMD_SET ... 0
        CUST_UNIT ... 1
        """
        answer = self._send_and_receive("DEFAULT")
        if "Ready" in answer:
            print("Successfully resetted the sensor to default parameters")
        else:
            print(f"Error while resetting to default, sensor answer is: {answer}")


    def getCommandsetVersion(self):
        """
        Read the command set version from the sensor.
        0 is the  default new command set
        1 is the mensor legacy command set
        """
        answer = self._send_and_receive("CMD_SET?")
        if len(answer) == 1:
            return int(answer)
        else:
            return int(answer[-1])


    def setCommandsetVersion(self, cmd_set_version):
        """
        Set the version of the command set
        0 ... default new command set
        1 ... Mensor legacy command set
        This implementation works only with the default new command set
        """
        # Round to integer value if necessary
        cmd_set_version = int(cmd_set_version)
        # Check if we are in the possible range
        if (cmd_set_version!=0) and (cmd_set_version!=1):
            raise ValueError("Incorrect command set version, has to be 0 or 1, but was {cmd_set_version}!")
        answer = self._send_and_receive(f"CMD_SET {cmd_set_version}")
        if "Ready" in answer:
            print(f"Successfully set the command set to {cmd_set_version}")
        else:
            print(f"Error while setting the command set, sensor answer is: {answer}")

    # Commands not implemented: PWD_CHANGE, PWD

    # -------------------------------------------------------------------------
    # Mensor pressure related commands

    def getPressureReading(self):
        """
        Get the actual pressure reading from the sensor
        + other configurable output values
        """
        # Send the read command and receive the answer
        answer = self._send_and_receive("PRESS?")
        # TODO: Error message response handling
        # TODO: Make this work also with other configurable output values

        # Convert the output to floating point data
        pressure = float(answer)
        return pressure


    def getRange(self):
        """
        Get the sensor range in currently specified units
        """
        answer = self._send_and_receive("RANGE_MIN?")
        range_min = float(answer)
        answer = self._send_and_receive("RANGE_MAX?")
        range_max = float(answer)
        return [range_min, range_max]


    def getUnit(self):
        """
        Get the unit and the corresponding unit index
        """
        answer_unit = self._send_and_receive("UNIT?")
        answer_idx = self._send_and_receive("UNIT_INDEX?")
        return [answer_unit, int(answer_idx)]


    def setUnitIndex(self, unit_idx):
        """
        Set the unit index according to the sensor's manual
        Parameters:
            unit_idx =
                1 ... psi
                14 ... bar
                15 ... mbar
                23 ... Pa
        """
        answer = self._send_and_receive(f"UNIT_INDEX {unit_idx}")
        print(answer)


    def getCustomUnitFactor(self):
        """
        Get the custom unit factor.
        This is used to implement any custom unit.
        The conversion formula is: (xx/psi)
        """
        answer = self._send_and_receive("CUST_UNIT?")
        factor = float(answer)
        return factor


    def setCustomUnitFactor(self, factor):
        """
        Set a unit factor to multiply the pressure output with.
        This is used to implement any custom unit.
        The conversion formula is: (xx/psi)
        """
        # Build a correctly formatted command
        cmd = "CUST_UNIT {:+.7E}".format(factor)
        answer = self._send_and_receive(cmd)
        if "Ready" in answer:
            print(f"Successfully set the custom unit factor to {factor}")
        else:
            print(f"Error while setting the custom unit factor, sensor answer is: {answer}")

    # -------------------------------------------------------------------------
    # Mensor calibration related commands

    def getCalibrationDate(self):
        """
        Get the date of the last calibration
        """
        answer = self._send_and_receive("CAL_DATE?")
        answer = answer.replace(',','-')
        return answer


    def getSpanMultiplier(self):
        """
        Get the span multiplier (gain factor) of the sensor calibration
        """
        answer = self._send_and_receive("SPAN?")
        return float(answer)


    def getZeroOffset(self):
        """
        Get the zero offset of the sensor calibration
        """
        answer = self._send_and_receive("ZERO?")
        return float(answer)


    def setTare(self, on_off):
        """
        Turn the tare function on or off
        """
        if on_off:
            answer = self._send_and_receive("TARE 1")
        else:
            answer = self._send_and_receive("TARE 0")
        if "Ready" in answer:
            print(f"Successfully set tare function to {on_off}")
        else:
            print(f"Error while setting tare function, sensor answer is: {answer}")

    # Commands not implemented: CAL_DATE, CAL_SPAN, CAL_ZERO

    # -------------------------------------------------------------------------
    # Mensor other commands

    def getTemperature(self):
        """
        Get the sensor temperature in degrees Celsius
        """
        answer = self._send_and_receive("TEMP?")
        return float(answer)


    def getOutputMask(self):
        """
        Get the output mask configuration of the sensor
        """
        answer = self._send_and_receive("OUTPUT_MASK?")
        # TODO: Implement decoding of the output mask
        return answer

    # Commands not implemented: OUTPUT_MASK

    # -------------------------------------------------------------------------
    # Private methods

    def _send_and_receive(self, command, broadcast = False):
        """
        Internal method to format commands correctly
        Send the command, receive and cut the answer propperly
        In broadcast mode all received answers are packed into a list
        """
        # Build the write command
        if broadcast:
            cmd = f"#*{command}\r\n"
        else:
            cmd = f"#{self.address}{command}\r\n"
        # Write it to the serial line
        self.ser.write(cmd.encode('ascii'))

        # Receive the answer, we can use readline as it is always terminated
        if broadcast:
            # In broadcast mode we try to read as many answers as possible
            # BUG: Actually the broadcast mode is not working properly
            # because all sensors responde equally fast jamming the line
            answer = []
            raw_data = self.ser.readlines()
            for dt in raw_data:
                answer.append(str(dt)[2:-5])
        else:
            # Just read one line
            data_str = str(self.ser.readline())
            answer = data_str[2:-5] # Cut unnecessary characters

        return answer
