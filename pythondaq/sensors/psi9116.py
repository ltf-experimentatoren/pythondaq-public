#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (C) 2022 Daniel Jaeger

This file is part of PythonDAQ.

PythonDAQ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PythonDAQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PythonDAQ.  If not, see <http://www.gnu.org/licenses/>.

Module to operate the 9016 and 9116 ethernet pressure scanners by
Measurement Specialties, Inc.

@author: Daniel Jaeger
@date: 2020
"""

import time

from .psi9000series import Psi9000Series
from utils import unitconversion


class Psi9116(Psi9000Series):
    """
    Class which sets up all commands to use the Psi 9116 pressure scanner
    """
    # Class variables
    # Public variables
    num_channels = 16
    units = ["Pa" for i in range(num_channels)]

    # Private variables
    _read_command = b'rffff0'


    def __init__(self, ip_address, name = "Psi9116", init_comm = True):
        """
        Method to initializ the class
        """
        # Initialize the parent class
        super().__init__(ip_address, name, init_comm)

        # TODO: Set the other necessary parameters
        # - Channel names
        self.channel_names = [f"Channel {i+1}" for i in range(self.num_channels)]
        # - unit conversion
        # - Individual module configuration


    def initCommunication(self):
        """
        Initialize network communication with the sensor
        """
        # Open the network connection
        super().initCommunication()

        # Get the range information
        self.sensor_range = self.getSensorRange()
        self.value_range = self.getSensorRange()


    def _convert_to_units(self, data_list):
        """
        The Psi 9116 returns the data in psi.
        This function converts them to pascals.
        """
        return unitconversion.psi_to_pascal(data_list)


    def purge(self,purge_time = 2):
        """
        Sets internal valves to the purge position for the specified amount of time.
        """
        # Set the manifold to manual operation
        self.setCalvalveMode(False)

        # Set the calibration valve to the purge position
        ok = self.setCalvalvePosition("purge")
        if not ok:
            return False

        # Wait for the specified amount of time
        time.sleep(purge_time)

        # Set the manifold back to run position
        ok = self.setCalvalvePosition("run")
        if not ok:
            return False

        # Set the manifold back to automatic operation
        self.setCalvalveMode(True)


    def calibrateOffset(self):
        """
        Triggers the re-zero-calibration of the sensor.

        Usage:
            1. Ensure that the calibration manifold switches automatically
            2. Present zero pressure between CAL and CAL REF pressure ports
            3. Run this function

        Attention:
            The values are stored in volatile memory.
            Use the saveCalibrationOffset command to save calibration values
            to the persistent memory.
        """
        # Send the re-zero calibration command for all channels
        self.sock.sendall(b"hffff")
        # Receive the answer (offset values) and convert it to a string
        data_str = str(self.sock.recv(self.PUFFER_SIZE))

        # Convert response to list of float and sort per channel number
        data_str = data_str[:-1]
        data_list = data_str.split()
        data_list = data_list[1:]
        offset_values = [float(i) for i in data_list]
        offset_values.reverse()

        return offset_values


    def calibrateGain(self,ref_value = 0,manual_entry = False):
        """
        Triggers the span (full range gain) calibration of the sensor.
        - Normally full range pressure has to be applied, otherwise manually specify
          the applied calibration pressure (ref_value in Pa).
        - Set manual entry to true if you want to specify the reference value at runtime.

        Usage:
            1. Perform Offset calibration first
            2. Ensure that zero pressure is detected correctly after re-zero
            3. Ensure that the calibration manifold switches automatically
            4. Present full range pressure between CAL and CAL REF pressure ports
            5. Run this function

        Attention:
            The values are stored in volatile memory.
            Use the saveCalibrationGain command to save calibration values
            to the persistent memory.
        """
        # Set the calibration valve to the cal position
        self.setCalvalvePosition("calibrate")

        # Wait some time so stabilize the pressure
        time.sleep(30)

        # Runtime reference value
        if manual_entry:
            ref_value = float(input("Please enter actual pressure in Pa..."))
            [timestamp, values] = self.getValue()
            print("Acutual pressure values are:"+str(values))

        # Send the gain calibration command for all channels
        if ref_value == 0:
            self.sock.sendall(b"Zffff")
        else:
            ref_value = unitconversion.pascal_to_psi(ref_value)
            print("Calibration pressure is " + str(ref_value) + " Pa")
            command = "Zffff {:7.4f}".format(ref_value)
            self.sock.sendall(command.encode('ascii'))
        # Receive the answer (gain values) and convert it to a string
        data_str = str(self.sock.recv(self.PUFFER_SIZE))

        # Wait some time before switching the calibration valve back
        time.sleep(2)
        self.setCalvalvePosition("run")

        # Convert response to list of float and sort per channel number
        data_str = data_str[:-1]
        data_list = data_str.split()
        data_list = data_list[1:]
        gain_values = [float(i) for i in data_list]
        gain_values.reverse()

        return gain_values


    def setCalvalveMode(self,automatic = True):
        """
        Set the switching mode of the sensor's calibration valve.
        True for automatic switching, False for manual switching
        """
        if automatic:
            self._send_set_command(b"w0b00",True)
        else:
            self._send_set_command(b"w0b01",True)


    def setCalvalvePosition(self,position = "run"):
        """
        Sets the position of the calibration valve.
        Possible positions are:
        - "run"
        - "calibrate"
        - "purge"
        - "leakcheck"
        """
        # TODO: Propper error handling
        if position == "run":
            self._send_set_command(b"w1200")
            self._send_set_command(b"w0C00")
            ok = True
        elif position == "calibrate":
            self._send_set_command(b"w1200")
            self._send_set_command(b"w0C01")
            ok = True
        elif position == "purge":
            self._send_set_command(b"w0C01")
            self._send_set_command(b"w1201")
            ok = True
        elif position == "leakcheck":
            self._send_set_command(b"w0C00")
            self._send_set_command(b"w1201")
            ok = True
        else:
            print("ERROR: Unknown position")
            ok = False

        return ok


    def saveCalibrationOffset(self):
        """
        Stores the calibrated offset values to the non-volatile memory.
        """
        # Send the command
        ok = self._send_set_command(b"w08", True)

        # Print some user output
        if ok:
            print("Offset values successfully stored")
        else:
            print("ERROR: Offset values could not be stored")
        return ok


    def saveCalibrationGain(self):
        """
        Stores the calibrated gain values to the non-volatile memory.
        """
        # Send the command
        ok = self._send_set_command(b"w09", True)

        # Print some user output
        if ok:
            print("Gain values successfully stored")
        else:
            print("ERROR: Gain values could not be stored")
        return ok


    def getSensorRange(self, decode = True):
        """
        Get the range of the individual channel transceivers
        Parameter decode:
            True: The range is returned in pascal.
            False: The range is returned as a range code. See manual for details.
        """
        range_output = []

        # Get the range code for each channel
        for i in range(self.num_channels):
            # Send the command
            command = f"u5{(i+1):02x}0A"
            self.sock.sendall(command.encode('ascii'))
            # Receive and decode the answer
            data = self.sock.recv(self.PUFFER_SIZE)
            # TODO: Perform error handling before decoding
            code = int(data[2:])
            if decode:
                if code == 1:
                    range_chan = [-2488.4, 2488.4]
                elif code == 2:
                    range_chan = [-4976.8, 4976.8]
                elif code == 3:
                    range_chan = [-6894.8, 6894.8]
                elif code == 4:
                    range_chan = [-17236.9, 17236.9]
                elif code == 5:
                    range_chan = [-34473.8, 34473.8]
                else:
                    print(f"Unknown range code at channel {i+1}")
                    range_chan = [-1, 1]
                range_output.append(range_chan)
            else:
                # No decoding
                range_output.append(code)
        return range_output


    def getUnitConversionFactor(self):
        """
        Get the factor stored to convert engineering units
        If the factor is set to 1.0, raw data units are in psi
        The command sent to the module is "u01101"
        """
        # Send the command
        self.sock.sendall(b"u01101")
        # Receive and decode the answer
        data = self.sock.recv(self.PUFFER_SIZE)
        return float(data[2:])


    def setUnitConversionFactor(self, factor):
        """
        Set the engineering unit conversion factor
        The command sent to the module is "v01101"
        Save the values using the method "saveOperatingOptions()"
        """
        # Send the command
        command = f"v01101 {factor:.6f}"
        ok = self._send_set_command(command.encode('ascii'), True)

        # Print some user output
        if ok:
            print("Values successfully set")
            print("Do not forget to save the values")
        else:
            print("ERROR: Values could not be set")
        return ok



def CalibrationTool():
    """
    Simple program to perform the calibration of a sensor.
    """
    print("Calibration program for the Psi 9116 pressure scanner")

    # Get the ip and initialize communication
    ip = input("Please enter the sensor's IP address...")
    sensor = Psi9116(ip,"Psi9116Scanner")
    sensor.setCalvalveMode(False)

    # Decide the calibration prodcedure
    mode = input("Enter the calibration mode: o for offset, g for gain, a for all, q to quit the program...")
    if mode == "o":
        do_offset = True
        do_gain = False
    elif mode == "g":
        do_offset = False
        do_gain = True
    elif mode == "a":
        do_offset = True
        do_gain = True
    else:
        # Go directly to the closing procedure
        do_offset = False
        do_gain = False

    # Perform offset calibration if desired
    if do_offset:
        print("Perform offset calibration.")
        # Switch calibration manifold to cal position and do the calibration
        sensor.setCalvalvePosition("calibrate")
        print("Apply zero pressure between CAL and CAL REF pressure ports")
        input("Press enter to continue...")
        values = sensor.calibrateOffset()

        # Shift the valve back to run position
        time.sleep(2)
        sensor.setCalvalvePosition("run")

        # Store the values to non-volatile memory if desired
        print("The measured offset values are: "+ str(values))
        store = input("Do you want to store the values to non-volatile memory? y for yes, n for no")
        if store == "y":
            sensor.saveCalibrationOffset()

    # Perform gain calibration if desired
    if do_gain:
        print("Perform gain calibration.")
        print("Apply full range pressure between CAL and CAL REF pressure ports")
        input("Press enter to continue...")
        values = sensor.calibrateGain(manual_entry = True)

        # Store the values to non-volatile memory if desired
        print("The measured gain values are: "+ str(values))
        store = input("Do you want to store the values to non-volatile memory? y for yes, n for no")
        if store == "y":
            sensor.saveCalibrationGain()

    sensor.setCalvalveMode(True)
    sensor.close()
    print("Calibration finished. Good bye!")

