#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (C) 2022 Daniel Jaeger

This file is part of PythonDAQ.

PythonDAQ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PythonDAQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PythonDAQ.  If not, see <http://www.gnu.org/licenses/>.

Module to operate the 9000 series netscanner ethernet measurement devices by
Measurement Specialties, Inc.

@author: Daniel Jaeger
@date: 2020
"""

import socket
import datetime

from .sensor import Sensor


class Psi9000Series(Sensor):
    """
    Class which contains all common commands for the PSI 9000 Netscanner series.
    Classes for the individual devices should inheritate this class.

    To create an inheritated class configure the following attributes
    - Default name
    - Channel names
    - Engineering units
    - Sensor and value range
    - Unit conversion if necessary
    - Individual module configuration
    - Read and set commands
    """
    # Constants
    UDP_SEND_PORT = 7000
    UDP_RCV_PORT = 7001
    TCP_PORT = 9000
    PUFFER_SIZE = 1024

    # Private class variables
    _read_command = b'rffff0'

    def __init__(self, ip_address, name = "Psi9000Series", init_comm = True):
        """
        Initialize the class
        """
        # Initialize the parent class
        super().__init__()
        # Set some attributes
        self.ip = ip_address
        self.name = name

        # Init network communication if desired
        if init_comm:
            self.initCommunication()
        # TODO: Module setup or reset


    def setIp(self,ip_address):
        self.ip = ip_address


    def initCommunication(self):
        """
        Initialize network communication with the sensor
        """
        self.sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        self.sock.connect((self.ip,self.TCP_PORT))


    def get_raw_value(self):
        """
        Get the acutal high precision values from the sensor
        """
        # Send the request command
        self.sock.sendall(self._read_command)
        # Receive the answer and convert it to a string
        data_str = str(self.sock.recv(self.PUFFER_SIZE))
        # TODO: Error message response handling

        # Get the actual timestamp
        timestamp = datetime.datetime.now()

        # Convert response to list of float and sort per channel number
        data_str = data_str[:-1]
        data_list = data_str.split()
        data_list = data_list[1:]
        data_list = [float(i) for i in data_list]
        data_list.reverse()

        # Transform data to the desired units
        data_list = self._convert_to_units(data_list)
        # A custom claibration is done by the parent's class getValue() method

        return [timestamp,data_list]


    def _convert_to_units(self, data_list):
        """
        Function to convert read values to the correct units
        This function has to be reimplemented by the interitated class.
        """
        return data_list


    def queryModule(self, broadcast = False):
        """
        Send a command to query all modules which are connected to the network
        This function uses the command 'psi9000'
        """

        if broadcast:
            ip = "255.255.255.255"
        else:
            ip = self.ip
        # Send
        # TODO; Send command as broadcast to query all modules
        MESSAGE = b"psi9000"
        with socket.socket(socket.AF_INET,socket.SOCK_DGRAM) as sock:
            #INET = Internet, DGRAM = UDP
            if broadcast:
                sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, True)
            sock.sendto(MESSAGE, (ip, self.UDP_SEND_PORT))

        # Receive
        # TODO: This has to be reworked to detect multiple modules
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
            sock.settimeout(5)
            sock.bind(('', self.UDP_RCV_PORT))

            data, addr = sock.recvfrom(self.PUFFER_SIZE)
            print("received message: %s" % data)

        return data


    def getOnline(self):
        """
        Get information if the sensor is online
        """
        ok = self._send_set_command(b"A", True)
        return ok


    def reset(self):
        """
        Resets the module to default settings, i.e.:
        - Re-zero correction (offset) terms are set to the last values stored in transducer memory.
        - Span correction (gain) terms are set to the last values stored in transducer memory.
        - Calibration Valve is set to the RUN Position
        - Number of Samples for Data Averaging is set to last value stored in non-volatile memory (factory default = 8).
        - Any autonomous host data delivery streams defined by ‘c’ sub-commands are reset (undefined).
        - The Multi-Point Calibration function defined by ‘C’ sub-commands is reset (undefined) if in progress.
        """
        ok = self._send_set_command(b"B",True)
        return ok


    def close(self):
        """
        Close the network connection
        """
        self.sock.close()


    # ------------------------------------------------------------------------
    # Common w-Commands

    def saveOperatingOptions(self):
        """
        Save operating options to flash memory.
        Use command w07.
        """
        ok = self._send_set_command(b"w07", True)
        return ok


    def setIpResolutionMode(self, mode):
        """
        Set the mode of IP address resolution:
            mode = "static": Use static IP address
            mode = "dynamic": Use dynamic IP address via RARP/BOOTP
        Use command "w13ii".
        """
        # Define the command according to the selected mode
        if mode == "static":
            cmd = b"w1300"
        elif mode == "dynamic":
            cmd = b"w1301"
        else:
            print("Error: Unknown mode!")
            return False

        # Send the command and receive the answer
        ok = self._send_set_command(cmd, True)
        return ok


    # TODO: Error handling
    # TODO: Destructor method to close communication


    def _send_set_command(self, command, print_answer = False):
        """
        Sends a simple set commmand whick only returns acknowledgement.
        This communication function handles some errors
        The command already has to be formatted correctly (bytes)
        """
        # Send the command
        self.sock.sendall(command)
        # Receive the answer
        data_str = str(self.sock.recv(self.PUFFER_SIZE))

        # Print the answer if desired
        if print_answer:
            print(data_str)

        # TODO: Parse the answer correctly
        if "A" in data_str:
            ok = True
        else:
            print("ERROR: System answer was: " + data_str)
            ok = False
        # TODO: Error handling
        return ok