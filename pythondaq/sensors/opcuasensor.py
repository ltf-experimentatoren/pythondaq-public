#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (C) 2022 Daniel Jaeger

This file is part of PythonDAQ.

PythonDAQ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PythonDAQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PythonDAQ.  If not, see <http://www.gnu.org/licenses/>.

Module to connect to an OPC UA server to acquire data.

@author: Daniel Jaeger
@date: 2022
"""

import datetime
from opcua import Client

from .sensor import Sensor


class OpcUaSensor(Sensor):
    """
    Class for data acquisition from an OPC UA server
    """
    def __init__(self, ip_address, variable_names, name = "OpcUa_Sensor", init_comm = True):
        """
        Initialization method.
        The variable names is a dictionary of qualified opcua names,
        in the structure e.g. {'1:MyObject':['1:MyVariable']}
        """
        # Call the parent's initialization method
        super().__init__()

        # Store some instance variables
        self.url = "opc.tcp://" + ip_address + ":4840"
        self.variable_names = variable_names
        self.name = name
        self.num_channels = len(variable_names)
        # TODO: Remove dummy units
        self.units = ['-' for i in range(self.num_channels)]
        self.sensor_range = [[0.0,1.0] for i in range(self.num_channels)]
        self.value_range = [[0.0,1.0] for i in range(self.num_channels)]

        # OPC UA client for communication
        self.opc_client = Client(self.url)
        self.opc_nodes = []

        # Init network communication if desired
        if init_comm:
            self.initCommunication()


    def initCommunication(self):
        """
        Connect to the OPC UA server
        """
        self.opc_client.connect()
        # Search for the OPC nodes
        opc_objects = self.opc_client.get_objects_node()
        for object_name in self.variable_names:
            obj_node = opc_objects.get_child(object_name)
            for var_name in self.variable_names[object_name]:
                self.opc_nodes.append(obj_node.get_child(var_name))

        # Assign proper channel names
        self.channel_names = [node.get_display_name().Text for node in self.opc_nodes]


    def get_raw_value(self):
        """
        Get the acutal readingss from the sensors
        """
        # Get the values from the OPC server
        values = [node.get_value() for node in self.opc_nodes]
        # Get the actual timestamp
        timestamp = datetime.datetime.now()
        return [timestamp, values]


    def close(self):
        """
        Close the network connection
        """
        self.opc_client.disconnect()
        self.opc_client.close_session()
