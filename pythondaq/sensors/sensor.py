#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (C) 2022 Daniel Jaeger

This file is part of PythonDAQ.

PythonDAQ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PythonDAQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PythonDAQ.  If not, see <http://www.gnu.org/licenses/>.

Basic abstract sensor class. All further sensor classes
should be derived from this clas.

@author: Daniel Jaeger
@date: 2020
"""


class Sensor():
    """
    Abstract class for the implementation of a sensor.
    It includes a basic linear claibration
    """
    # Class variables
    num_channels = 1
    units = ["-"]

    def __init__(self):
        """
        Initialize this class
        """
        self.name = "Unnamed_Sensor"

        self.gain = [1.0 for i in range(self.num_channels)]
        self.offset = [0.0 for i in range(self.num_channels)]
        self.channel_names = [f"Channel {i+1}" for i in range(self.num_channels)]
        # The range, the sensor is capable of
        self.sensor_range = [[0.0, 1.0] for i in range(self.num_channels)]
        # The expected range of the signal
        self.value_range = [[0.0, 1.0] for i in range(self.num_channels)]

        # Some meta-information which will be used by computations
        self.height = 0
        self.rec = {}
        self.channels = {}
        self.circ_pos = {}
        self.meas_heights = {}
        self.rad_pos = {}


    def get_raw_value(self):
        """
        Get the raw data from the sensor.
        Reimplement this method for the actual used sensor
        """
        values = 0
        timestamp = 0
        return [timestamp, values]


    def getValue(self):
        """
        Get the data from the sensor and do a basic linear calibration.
        This method should be called by the user.
        A reimplementation of this method should not be necessary.
        """
         # Get the rawq data from the sensor
        [timestamp, raw_values] = self.get_raw_value()
        # Apply linear scaling
        values = [self.gain[i] * raw_values[i] + self.offset[i] \
                  for i in range(self.num_channels)]
        # Bundle the data inside a dictionary
        channel_dict = dict(zip(self.channel_names, values))

        return [timestamp, channel_dict]


    def close(self):
        """
        Method called when shutting down the sensor
        Reimplement this class to close networ connections etc.
        """
        pass


    def setName(self,name):
        self.name = name


    def setUnits(self, units):
        self.units = units


    def setSensorRange(self, new_range):
        self.sensor_range = new_range


    def setValueRange (self, new_range):
        self.value_range = new_range


    def setChannelNames(self,channel_names):
        """
        Set the names for all channels
        """
        for i in range(self.num_channels):
            self.channels[channel_names[i]] = 0.0
            self.meas_heights[self.name + ":" + channel_names[i]] = 0.0
            self.rad_pos[self.name + ":" + channel_names[i]] = 0.0
            self.circ_pos[self.name + ":" + channel_names[i]] = 0.0
        self.channel_names = channel_names


    def setGain(self,gain):
        """
        Set the gain for all channels
        """
        # Check for the correct type
        if type(gain) != list:
            raise TypeError("The gain argument has to be of type list")
        # Check for the correct number of channels
        if len(gain) != self.num_channels:
            raise ValueError("Wrong length of the list")
        # Now we are good to set the gain
        self.gain = gain


    def setChannelGain(self, ch_number, gain):
        """
        Set the gain of a single channel.
        The channel number is zero based
        """
        # Check if the channel number is correct
        if (ch_number < 0) or (ch_number >= self.num_channels):
            print("Error: Wrong channel number")
            return
        # Set the gain
        self.gain[ch_number] = gain


    def setOffset(self,offset):
        """
        Set the offset for all channels
        """
        # Check for the correct type
        if type(offset) != list:
            raise TypeError("The offset argument has to be of type list")
        # Check for the correct number of channels
        if len(offset) != self.num_channels:
            raise ValueError("Wrong length of the list")
        # Now we are good to set the offset
        self.offset = offset


    def setChannelOffset(self, ch_number, offset):
        """
        Set the offset of a single channel.
        The channel number is zero based
        """
        # Check if the channel number is correct
        if (ch_number < 0) or (ch_number >= self.num_channels):
            print("Error: Wrong channel number")
            return
        # Set the offset
        self.offset[ch_number] = offset


    def setHeight(self,height):
        self.height = height

    def setRecoveryFactor(self,rec):
        for i in rec:
            self.rec[self.name + ":" + i] = rec[i]

    def setCircPos(self, circ_pos):
        for k in circ_pos:
            self.circ_pos[self.name + ":" + k] = circ_pos[k]

    def setMeasHeights(self, meas_heights):

        for k in meas_heights:
            self.meas_heights[self.name + ":" + k] = meas_heights[k]

    def setRadPos(self, rad_pos):

        for k in rad_pos:
            self.rad_pos[self.name+ ":" + k] = rad_pos[k]
