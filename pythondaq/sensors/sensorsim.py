#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (C) 2022 Daniel Jaeger

This file is part of PythonDAQ.

PythonDAQ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PythonDAQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PythonDAQ.  If not, see <http://www.gnu.org/licenses/>.

This file contains a class simulating the behavior of a sensor.

@author: Daniel Jaeger
@date: 2020
"""

import random
import datetime

from .sensor import Sensor

class SensorSim(Sensor):
    """
    Class for a sensor simulation
    """
    def __init__(self,name = "Sensor Simulation"):
        """
        Initialize the class
        """
        # Call the initialization method of the parent class
        super().__init__()
        
        # Add the name information
        self.name = name


    def get_raw_value(self):
        """
        Get the raw data from the sensor
        For the sensor simulation these values are created by using random variables
        """
        values = [random.random()]
        timestamp = datetime.datetime.now()
        return [timestamp, values]
