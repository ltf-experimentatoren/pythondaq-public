#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (C) 2022 Daniel Jaeger

This file is part of PythonDAQ.

PythonDAQ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PythonDAQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PythonDAQ.  If not, see <http://www.gnu.org/licenses/>.

Module to operate the 9032 barometer by Measurement Specialties, Inc.

@author: Daniel Jaeger
@date: 2020
"""

from.psi9000series import Psi9000Series
from utils import unitconversion

class Psi9032(Psi9000Series):
    """
    Class which sets up all commands to use the Psi 9032 absolute pressure sensor
    """
    # Public class varibles
    num_channels = 2
    units = ["Pa","°C"]

    # Private class variables
    _read_command = b'r80010'


    def __init__(self, ip_address, name = "Psi9032", init_comm = True):
        """
        Method to initialize the class
        """
        # Initialize the parent class
        super().__init__(ip_address, name, init_comm)

        # Set public variables
        self.channel_names = ["Pressure","Module Temp"]
        self.sensor_range = [[75842.3, 103421.0], [-10.0,60.0]] # Exact value (11-15 psi)
        self.value_range = [[76000.0, 103000.0], [-10.0,60.0]] # Rounded to useful range

        # TODO: Individual module configuration


    def _convert_to_units(self, data_list):
        """
        The Psi 9116 returns the data in psi.
        This function converts them to pascals.
        """
        # Convert the pressure value from psi to pascals
        data_list[0] = unitconversion.psi_to_pascal(data_list[0])
        # The module temperature already comes in degrees celsius
        return data_list